//
//  TonicKeyTableViewCell.swift
//  ear training
//
//  Created by Marcus Choi on 10/29/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class TonicKeyTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var tonicKeyText: UITextView!

    @IBOutlet var tonicKeyOutlet: UISlider!
    
    @IBAction func tonicKeyValueChanged(sender: AnyObject)
    {
        var tonicKeyInt = Int(tonicKeyOutlet.value)
        var tonicKey = String()
        
        if tonicKeyInt == 0
        {
            tonicKey = "C"
        }
        else if tonicKeyInt == 1
        {
            tonicKey = "Db"
        }
        else if tonicKeyInt == 2
        {
            tonicKey = "D"
        }
        else if tonicKeyInt == 3
        {
            tonicKey = "Eb"
        }
        else if tonicKeyInt == 4
        {
            tonicKey = "E"
        }
        else if tonicKeyInt == 5
        {
            tonicKey = "F"
        }
        else if tonicKeyInt == 6
        {
            tonicKey = "Gb"
        }
        else if tonicKeyInt == 7
        {
            tonicKey = "G"
        }
        else if tonicKeyInt == 8
        {
            tonicKey = "Ab"
        }
        else if tonicKeyInt == 9
        {
            tonicKey = "A"
        }
        else if tonicKeyInt == 10
        {
            tonicKey = "Bb"
        }
        else if tonicKeyInt == 11
        {
            tonicKey = "B"
        }
        else //if tonicKeyInt == 12
        {
            tonicKey = "Random"
        }
        
        tonicKeyText.text = "Tonic Key: \(tonicKey)"
    
    }
    
}
