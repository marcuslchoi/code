//
//  TempoTableViewCell.swift
//  ear training
//
//  Created by Marcus Choi on 10/29/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class TempoTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var tempoText: UITextView!

    @IBOutlet var tempoSlider: UISlider!

    @IBAction func tempoSliderValueChanged(sender: AnyObject)
    {
        var tempo = Int(tempoSlider.value)
        tempoText.text = "Tempo: \(tempo) BPM"
    }
    
    
}
