//
//  FixToCTableViewCell.swift
//  ear training
//
//  Created by Marcus Choi on 10/31/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class FixToCTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet var fixToCOutlet: UISwitch!
    @IBAction func fixToCValueChanged(sender: AnyObject)
    {
        var fixToC = Bool()
        
        if fixToCOutlet.on == true
        {
            fixToC = true
        }
        else
        {
            fixToC = false
        }
    }
}
