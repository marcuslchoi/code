//
//  TutorialViewController.swift
//  ear training
//
//  Created by Marcus Choi on 9/8/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import AVFoundation
import Parse

class TutorialViewController: UIViewController, UITextFieldDelegate {
    
    var tutorialMode = true
    
    var error: NSError? = nil
    @IBOutlet var solfegeImage: UIImageView!
    @IBOutlet var solfegeText: UITextView!
    
    var userAnswer = ""
    var userAnswerArray = [String]()
    var userGuesses = 0
    //@IBOutlet var userGuessesText: UITextView!
    
    //number of notes that user must guess for each problem
    var numberOfNotesPlayed = 1
    //total correct user guessed notes, updates after each correct answer
    var totalNumberOfNotesPlayed = 0
    
    //time that passes between notes played in seconds
    var timeBetweenNotes: NSTimeInterval = 1
    
    //plays chord then melody with delay of timeBetweenNotes
    var delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
        Int64(1 * Double(NSEC_PER_SEC)))
    
    @IBOutlet var scoreLabel: UILabel!
    var points = 0
    var problems = 0
    var finalScore = 0.00
    
    //timer stuff
    var timer = NSTimer()
    //hundredths of a second
    var hseconds = 0.00
    var minutes = 0
    @IBOutlet var timeLabel: UILabel!
    
    
    //set the play/pause button toggle
    @IBOutlet var navbar: UINavigationItem!
    
    //initially game is activated so playing and paused are both false
    var isPlaying = false
    var isPaused = false
    
    //tells how many times play/pause, marks game entry
    var counter = 0
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "backSegue"
        {
            if isPlaying == true
            {
                playPauseToggle(self)
            }
        }
    }
    
    @IBOutlet var playPauseOutlet: UIBarButtonItem!
    @IBAction func playPauseToggle(sender: AnyObject)
    {
        tutorialMode = false
        examplesOutlet.hidden = true
/*
        var domifaArray = ["do1","mi1","mi2","do2","fa2","fa1"]
        
        let sylChosenArraySet = Set(sylChosenArray)
        let domifaArraySet = Set(domifaArray)
        
        //compares sets to ensure do mi fa are in sylChosenArray
        let allElementsPresent = domifaArraySet.isStrictSubsetOf(sylChosenArraySet) //isSubsetOf(sylChosenArraySet)
*/
        //do mi fa game
        if tutorialSection == "do mi fa game" && scaleChosen.text!.lowercaseString != "do mi fa" && scaleChosen.text != "1 3 4"
        {

            var alertWrongScaleTones = UIAlertController(title: "Wrong Scale Tones", message: "Input \"do mi fa\" or \"1 3 4\" into the text field.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertWrongScaleTones.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                
                self.scaleChosen.text = ""
                
            }))
            
            self.presentViewController(alertWrongScaleTones, animated: true, completion: nil)
        
        }
        else
        {
            counter++
            
            var toggleButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Play, target: self, action: "playPauseToggle:")
            
            //pausing the game
            if isPlaying
            {
                timer.invalidate()
                isPlaying = false
                isPaused = true
                
                generateNewMelodyOutlet.enabled = false
                
            }
            else    //un-pausing creates new melody. If first play, reset points and probs
            {
                //game entry. counter is reset at end of game
                if counter == 1
                {
                    resetGameProperties()
                }
                
                gameTime(self)
                
                toggleButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Pause, target: self, action: "playPauseToggle:")
                isPlaying = true
                isPaused = false
            }
            navbar.rightBarButtonItem = toggleButton
            
            print(counter)
        }
    }
    
    var dotiGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("dotiGamePassed") as? Bool 
    var dotisolGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("dotisolGamePassed") as? Bool
    var domifaGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("domifaGamePassed") as? Bool
    var dosollaGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("dosollaGamePassed") as? Bool
    var doremiGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("doremiGamePassed") as? Bool
    var domesolGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("domesolGamePassed") as? Bool
    var dosolleGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("dosolleGamePassed") as? Bool
    var domesolteGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("domesolteGamePassed") as? Bool
    var dofafisolGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("dofafisolGamePassed") as? Bool
    var doratiGamePassed = NSUserDefaults.standardUserDefaults().valueForKey("doratiGamePassed") as? Bool
    
    var gameLength = 1
    var passingScore = 1500 as Double//2000 as Double
    @IBAction func gameTime(sender: AnyObject)
    {
        //PLAY MAJOR IF BEFORE MINOR SECTION, MINOR IF MINOR SECTION
        
        if tutorialCounter <= tutorialSections.indexOf("finished major") || tutorialCounter > tutorialSections.indexOf("finished minor")
        {
            playMinorChord = false
        
        }
        else
        {
            playMinorChord = true
        }
        
        previousOutlet.enabled = false
        nextOutlet.enabled = false
        
        playMelodyOutlet.enabled = true
        playChordOutlet.enabled = true
        
        scaleChosen.enabled = false
        
        generateNewMelody(self)
        generateNewMelodyOutlet.enabled = true
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("result"), userInfo: nil, repeats: true)
        
    }
    func result()
    {
        hseconds++
        
        //hseconds/100 is 1 second
        if hseconds/100 == 60.00
        {
            minutes++
            hseconds = 0.00
        }
            //proper display of hundredths of a second
        else if hseconds/100 < 10.00
        {
            timeLabel.text = "\(minutes):0\(hseconds/100)"
        }
        else
        {
            timeLabel.text = "\(minutes):\(hseconds/100)"
        }
        
        if minutes == gameLength //hseconds/100 == 5
        {
            
            timer.invalidate()
            
            timeLabel.text = "\(minutes):00.00"
            
            message.font = UIFont(name: message.font!.fontName, size: 22)
            message.textColor = UIColor.redColor()
            message.text = "Game Over"
            
            //if user did not finish a problem
            if (problems - 1) == 0
            {
                finalScore = 0.0
            }
            else
            {
                
                //total number of notes accumulated for each problem * accuracy * 100
                finalScore = Double(totalNumberOfNotesPlayed)*Double(abs(points))/Double(problems - 1) //problems-1 because last problem in game unfinished
                
                finalScore = round(100*finalScore)
            }
            
            //show the final score and ask to play again
            var alert = UIAlertController(title: "Final Score: \(finalScore)", message: "Remember to \"feel\" each scale tone for immediate recognition. Try not to sing/hear other tones in the scale to find your tone. Try Again?", preferredStyle: UIAlertControllerStyle.Alert)
            
            if finalScore < passingScore //100
            {
                alert.addAction(UIAlertAction(title: "Quit", style: .Default, handler: { (action) -> Void in
                    
                    self.problems = 1
                    
                    self.resetGameProperties()
                    self.playPauseToggle(self)
                    self.isPaused = false
                    
                    self.nextOutlet.enabled = true
                    self.previousOutlet.enabled = true
                    
                }))
            }
            else if finalScore >= passingScore //100
            {
                alert = UIAlertController(title: "Final Score: \(finalScore) You did it!", message: "Continue or Try Again?", preferredStyle: UIAlertControllerStyle.Alert)
            
                alert.addAction(UIAlertAction(title: "Continue", style: .Default, handler: { (action) -> Void in
                    
                    for solfegeOutlet in self.solfegeCollection
                    {
                        solfegeOutlet.enabled = false
                    }
                    
                    //["do ti game","do ti sol game","do mi fa game","do sol la game","do re mi game","do me sol game","do sol le game","do me sol te game","do fa fi sol game","do ra ti game"]
                    
                    //var tutorialSection = self.tutorialSections[self.tutorialCounter!]

                    if self.tutorialSection == "do ti game"
                    {
                        self.dotiGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.dotiGamePassed, forKey: "dotiGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do ti sol game"
                    {
                        self.dotisolGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.dotisolGamePassed, forKey: "dotisolGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do mi fa game"
                    {
                        self.domifaGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.domifaGamePassed, forKey: "domifaGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do sol la game"
                    {
                        self.dosollaGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.dosollaGamePassed, forKey: "dosollaGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do re mi game"
                    {
                        self.doremiGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.doremiGamePassed, forKey: "doremiGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do me sol game"
                    {
                        self.domesolGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.domesolGamePassed, forKey: "domesolGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do sol le game"
                    {
                        self.dosolleGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.dosolleGamePassed, forKey: "dosolleGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do me sol te game"
                    {
                        self.domesolteGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.domesolteGamePassed, forKey: "domesolteGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do fa fi sol game"
                    {
                        self.dofafisolGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.dofafisolGamePassed, forKey: "dofafisolGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    else if self.tutorialSection == "do ra ti game"
                    {
                        self.doratiGamePassed = true
                        NSUserDefaults.standardUserDefaults().setObject(self.doratiGamePassed, forKey: "doratiGamePassed")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                    
                    //enable the next button to continue in tutorial
                    self.nextOutlet.enabled = true
                    self.previousOutlet.enabled = true
                    
                    self.message.font = UIFont(name: self.message.font!.fontName, size: 17)
                    self.message.textColor = UIColor.blackColor()
                    self.message.text = "Press the Next button to continue"
                    
                    
                    self.playMelodyOutlet.enabled = false
                    //self.playChordOutlet.enabled = false
                    self.showAnswerOutlet.enabled = false
                    
                    self.resetGameProperties()
                    
                    //the last unanswered problem from the game
                    self.problems = 1
                    
                    self.playPauseToggle(self)
                    
                    self.navbar.rightBarButtonItem?.enabled = false
                    
                    self.isPaused = false    //because play pause toggle is called, make sure this is false
                    self.counter = 0
                    
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Try Again", style: .Default, handler: { (action) -> Void in
                
                self.resetGameProperties()
                self.gameTime(self)
                
            }))
                
            self.presentViewController(alert, animated: true, completion: nil)
            
            //only save score to database if it is above 0
            if finalScore > 0.0
            {
                var gameScore = PFObject(className: "GameScore")
                gameScore["score"] = finalScore
                gameScore["scale"] = scaleChosen.text
                gameScore["username"] = PFUser.currentUser()!.username
                gameScore["gameLength"] = gameLength
                
                //save the score
                gameScore.saveInBackgroundWithBlock
                    {(success: Bool, error: NSError?) -> Void in
                        if (success)
                        {print("\(gameScore.objectId)")}
                        else
                        {print("failed")}
                }
                
            }
            
            points = 0
            problems = 0
            minutes = 0
        }
    }
    func resetGameProperties()
    {
        //reset minutes and seconds
        minutes = 0
        hseconds = 0.0
        
        scoreLabel.text = "0/0"
        points = 0
        problems = 0
        totalNumberOfNotesPlayed = 0
        
        userGuesses = 0
        userAnswer = ""
        userAnswerArray.removeAll(keepCapacity: false)
    }
    
    //    @IBOutlet var resetGuessesOutlet: UIButton!
    @IBAction func resetGuesses(sender: AnyObject)
    {
        if userGuesses != 0
        {
            problems++
            
            userGuesses = 0
            userAnswer = ""
            userAnswerArray.removeAll(keepCapacity: false)
            
            //userGuessesText.text = "Guesses: \(userGuesses)"
            scoreLabel.text = "\(points)/\(problems)"
        }
        
    }
    
    
    var previousUserNote = String?()
    
    //very first guess of entire instance of using the app
    var firstGuessHasHappened = false
    
    var buttonPressDelayTime: dispatch_time_t!
    var playerSolfege: AVAudioPlayer = AVAudioPlayer()
    @IBAction func solfegeButtons(sender: AnyObject)
    {
        buttonPressDelayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        
        sender.layer.shadowOffset = CGSizeMake(0, 0)
        sender.layer.shadowOpacity = 0.5
        
        dispatch_after(buttonPressDelayTime, dispatch_get_main_queue()){
            
            sender.layer.shadowColor = UIColor.blackColor().CGColor
            sender.layer.shadowOffset = CGSizeMake(2, 2)
            sender.layer.shadowOpacity = 1
            
        }
        
        if tutorialMode == false
        {
            var restID = sender.restorationIdentifier!!
            var scaleDegree = String()
            
            //identify image based on restoration identifier property of button pressed
            solfegeImage.image = UIImage(named: restID + ".jpg")
            
            if restID == "do"
            {
                scaleDegree = "1"
            }
            else if restID == "ra"
            {
                scaleDegree = "b2"
            }
            else if restID == "re"
            {
                scaleDegree = "2"
            }
            else if restID == "me"
            {
                scaleDegree = "b3"
            }
            else if restID == "mi"
            {
                scaleDegree = "3"
            }
            else if restID == "fa"
            {
                scaleDegree = "4"
            }
            else if restID == "fi"
            {
                scaleDegree = "#4"
            }
            else if restID == "sol"
            {
                scaleDegree = "5"
            }
            else if restID == "le"
            {
                scaleDegree = "b6"
            }
            else if restID == "la"
            {
                scaleDegree = "6"
            }
            else if restID == "te"
            {
                scaleDegree = "b7"
            }
            else //if restID == "ti"
            {
                scaleDegree = "7"
            }
            
            solfegeText.text = "\(restID)(\(scaleDegree))"
            
            //file location of first (possibly wrong) guess
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(sender.restorationIdentifier!!)1"]!, ofType: "mp3")
            
            if answerIsShown == false //&& tutorialMode == false
            {
                message.text = ""
                userAnswer += sender.restorationIdentifier!! + " "
                
                //has syllables without ending number
                userAnswerArray.append(sender.restorationIdentifier!!)
                
                //if value from useranswerarray == value from answer (minus the last character), play the exact syllable as answer
                if userAnswerArray[userGuesses] == solfegeArray[solfegeIndices[userGuesses]].substringToIndex(solfegeArray[solfegeIndices[userGuesses]].endIndex.predecessor())
                {
                    fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary[solfegeArray[solfegeIndices[userGuesses]]], ofType: "mp3")
                }
                else //if user answer is wrong, play nearest note for sender's syllable
                {
                    if firstGuessHasHappened
                    {
                        //get index of previously played note, subtract index of syllable with end numbers, play the one that is smaller difference
                        if pianoWavArray.indexOf(previousUserNote!) != nil
                        {
                            //CHANGED FOR SWIFT2
                            if abs(pianoWavArray.indexOf(previousUserNote!)! - pianoWavArray.indexOf(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"]!)!) > abs(pianoWavArray.indexOf(previousUserNote!)! - pianoWavArray.indexOf(solfegeWavDictionary["\(userAnswerArray[userGuesses])2"]!)!)
                            {
                                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])2"], ofType: "mp3")
                                print("higher octave")
                            }
                            else
                            {
                                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"], ofType: "mp3")
                            }
                        }
                        else
                        {
                            fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"], ofType: "mp3")
                        }
                    }
                }
                
                //after this is true, can start ensuring notes of guesses are close together
                firstGuessHasHappened = true
                
                //remove extension from filename (last 4 characters) gives just the note
                //previousUserNote = fileLocationSolfege?.lastPathComponent.stringByDeletingPathExtension
                previousUserNote = String(NSURL(fileURLWithPath: fileLocationSolfege!).URLByDeletingPathExtension)
                
                
                userGuesses += 1
            }
            
            //playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!), error: &error)
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
                
            playerSolfege.play()
            
            //userGuessesText.text = "Guesses: \(userGuesses)"
            if userGuesses == numberOfNotesPlayed
            {
                userGuesses = 0
                
                delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
                    Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
                
                if userAnswer == answer
                {
                    message.font = UIFont(name: message.font!.fontName, size: 22)
                    let darkGreen = UIColor(hue: 0.3, saturation: 1.0, brightness: 0.5, alpha: 1.0)
                    message.textColor = darkGreen
                    message.text = "Correct!"
                    
                    //gives points if game is not paused and in non-game mode
                    if isPaused == false
                    {
                        points += 1
                        
                        //reset answer so user can't repeatedly press and get pts
                        answer = ""
                        
                        totalNumberOfNotesPlayed += numberOfNotesPlayed
                        
                        //generates new melody with delay
                        dispatch_after(delayTime1, dispatch_get_main_queue()){
                            self.generateNewMelody(self)
                            //self.userGuessesText.text = "Guesses: \(self.userGuesses)"
                        }
                    }
                    
                }
                else
                {
                    message.font = UIFont(name: message.font!.fontName, size: 22)
                    message.textColor = UIColor.redColor()
                    
                    message.text = "Try Again"
                    dispatch_after(delayTime1, dispatch_get_main_queue()){
                        
                        self.message.text = ""
                        //self.userGuessesText.text = "Guesses: \(self.userGuesses)"
                    }
                    
                    problems++
                }
                scoreLabel.text = "\(points)/\(problems)"
                
                userAnswer = ""
                userAnswerArray.removeAll(keepCapacity: false)
            }
        }
    }
    
    var answerIsShown = false
    @IBOutlet var showAnswerOutlet: UIButton!
    @IBAction func showAnswer(sender: AnyObject)
    {
        //make syllables the same octave if answer is played?
        
        playMelody(self)
        
        message.font = UIFont(name: message.font!.fontName, size: 22)
        message.textColor = UIColor.blueColor()
        
        //NUMBER SYSTEM
        if numberSystem == true
        {
            //turn answer into numbers
            var solfegeNumberDictionary = ["do":"1","ra":"b2","re":"2","me":"b3","mi":"3","fa":"4","fi":"#4","sol":"5","le":"b6","la":"6","te":"b7","ti":"7"]
            
            let answerNumSysArray = answer.characters.split{$0 == " "}.map(String.init)
            
            var answerNumSys = ""
            
            for answerNum in answerNumSysArray
            {
                answerNumSys += solfegeNumberDictionary[answerNum]! + " "
            }
            
            message.text = answerNumSys + "\n(Press New Question to continue)"
        }
        else
        {
            message.text = answer + "\n(Press New Question to continue)"
        }
        
        answerIsShown = true
        
    }
    
    var player = [AVAudioPlayer]()
    
    var chord1: AVAudioPlayer = AVAudioPlayer()
    var chordm3: AVAudioPlayer = AVAudioPlayer()
    var chord3: AVAudioPlayer = AVAudioPlayer()
    var chord5: AVAudioPlayer = AVAudioPlayer()
    
    var pianoWavArray = ["C3", "Db3", "D3", "Eb3", "E3", "F3", "Gb3", "G3", "Ab3", "A3", "Bb3", "B3", "C4", "Db4", "D4", "Eb4", "E4", "F4", "Gb4", "G4", "Ab4", "A4", "Bb4", "B4"]
    
    var solfegeArray = ["do1","ra1","re1","me1","mi1","fa1","fi1","sol1","le1","la1","te1","ti1","do2","ra2","re2","me2","mi2","fa2","fi2","sol2","le2","la2","te2","ti2"]
    
    var rootIndex = 0//Int(arc4random_uniform(UInt32(12)))
    
    //dictionary do=Ab.wav, ra=A.wav etc
    var solfegeWavDictionary = [String: String]()
    
    var solfegeIndices = [Int]()
    
    //each notesPlayed index contains a wav filename, ie "Bb", for the melody
    var notesPlayed = [String]()
    
    var answer: String = ""
    
    //duplicated in generateNewMelody, created for tutorial mode
    func populateSolfegeWavDictionary(sender: AnyObject)
    {
        var i:Int
        for i = rootIndex; i > 0; i--
        {
            //ie, do = "Bb"
            solfegeWavDictionary[solfegeArray[12-i]] = pianoWavArray[rootIndex-i];
        }
        
        //12 is index of beginning of 2nd octave
        for i = rootIndex; i < 12; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
        for i = 12; i < 12 + rootIndex; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex + 12]] = pianoWavArray[i];
        }
        
        for i = 12 + rootIndex; i < solfegeArray.count; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
    
    }
    
    var sylChosenArray = [String]()
    @IBOutlet var generateNewMelodyOutlet: UIButton!
    @IBAction func generateNewMelody(sender: AnyObject)
    {
        buttonPressDelayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        
        generateNewMelodyOutlet.layer.shadowOffset = CGSizeMake(0, 0)
        generateNewMelodyOutlet.layer.shadowOpacity = 0.5
        
        dispatch_after(buttonPressDelayTime, dispatch_get_main_queue()){
            
            self.generateNewMelodyOutlet.layer.shadowColor = UIColor.blackColor().CGColor
            self.generateNewMelodyOutlet.layer.shadowOffset = CGSizeMake(2, 2)
            self.generateNewMelodyOutlet.layer.shadowOpacity = 1
            
        }
        
        //the scale chosen variable
        var scale = scaleChosen.text!.lowercaseString
        
        //var tutorialSection = tutorialSections[tutorialCounter!]
        
        resetGuesses(self)
        showAnswerOutlet.enabled = true

        for solfegeOutlet in solfegeCollection
        {
            solfegeOutlet.enabled = false   //used to say true
        }

        doOutlet.enabled = true
        
        if tutorialSection == "do ti game"
        {
            tiOutlet.enabled = true
        }
        else if tutorialSection == "do ti sol game"
        {
            tiOutlet.enabled = true
            solOutlet.enabled = true
        }
        else if tutorialSection == "do mi fa game"
        {
            miOutlet.enabled = true
            faOutlet.enabled = true
        }
        else if tutorialSection == "do sol la game"
        {
            solOutlet.enabled = true
            laOutlet.enabled = true
        }
        else if tutorialSection == "do re mi game"
        {
            reOutlet.enabled = true
            miOutlet.enabled = true
        }
        else if tutorialSection == "do me sol game"
        {
            meOutlet.enabled = true
            solOutlet.enabled = true
        }
        else if tutorialSection == "do sol le game"
        {
            leOutlet.enabled = true
            solOutlet.enabled = true
        }
        else if tutorialSection == "do me sol te game"
        {
            meOutlet.enabled = true
            solOutlet.enabled = true
            teOutlet.enabled = true
        }
        else if tutorialSection == "do fa fi sol game"
        {
            faOutlet.enabled = true
            solOutlet.enabled = true
            fiOutlet.enabled = true
        }
        else if tutorialSection == "do ra ti game"
        {
            raOutlet.enabled = true
            tiOutlet.enabled = true
                
        }
        
        playMelodyOutlet.enabled = true
        playChordOutlet.enabled = true
        
        answerIsShown = false
        
        //don't increase number of problems if game is paused and new melody generated
        if isPaused == false
        {
            problems += 1
        }
        answer = ""
        
        solfegeIndices = [Int](count: numberOfNotesPlayed, repeatedValue: 0)
        notesPlayed = [String](count: numberOfNotesPlayed, repeatedValue: "")
        
        //matching solfege syllables to piano notes in dictionary based on root index
        var i:Int
        for i = rootIndex; i > 0; i--
        {
            //ie, do = "Bb"
            solfegeWavDictionary[solfegeArray[12-i]] = pianoWavArray[rootIndex-i];
        }
        
        //12 is index of beginning of 2nd octave
        for i = rootIndex; i < 12; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
        for i = 12; i < 12 + rootIndex; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex + 12]] = pianoWavArray[i];
        }
        
        for i = 12 + rootIndex; i < solfegeArray.count; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
        var limitToSyllables = false
        
        //Check if user input syllables. Check for space and check if text does not match anything in scaleData array
        if scale.rangeOfString(" ") != nil
        {
            //CHANGED FOR SWIFT2
            sylChosenArray = (scaleChosen.text?.characters.split{$0 == " "}.map(String.init))!
            
            //keeps array count from updating after every loop
            let sylChosenArrayCount = sylChosenArray.count
            
            //make the array of syllables in all octaves
            for var i = 0; i < sylChosenArrayCount; i++
            {
                //change syllable to match solfegeArray syllables if user inputs numbers or other syllable names
                if sylChosenArray[i] == "1"
                {
                    sylChosenArray[i] = "do"
                }
                else if sylChosenArray[i] == "#1" || sylChosenArray[i] == "b2" || sylChosenArray[i] == "di"
                {
                    sylChosenArray[i] = "ra"
                }
                else if sylChosenArray[i] == "2"
                {
                    sylChosenArray[i] = "re"
                }
                else if sylChosenArray[i] == "#2" || sylChosenArray[i] == "b3" || sylChosenArray[i] == "ri"
                {
                    sylChosenArray[i] = "me"
                }
                else if sylChosenArray[i] == "3"
                {
                    sylChosenArray[i] = "mi"
                }
                else if sylChosenArray[i] == "4"
                {
                    sylChosenArray[i] = "fa"
                }
                else if sylChosenArray[i] == "#4" || sylChosenArray[i] == "b5" || sylChosenArray[i] == "se"
                {
                    sylChosenArray[i] = "fi"
                }
                else if sylChosenArray[i] == "5" || sylChosenArray[i] == "so"
                {
                    sylChosenArray[i] = "sol"
                }
                else if sylChosenArray[i] == "#5" || sylChosenArray[i] == "b6" || sylChosenArray[i] == "si"
                {
                    sylChosenArray[i] = "le"
                }
                else if sylChosenArray[i] == "6"
                {
                    sylChosenArray[i] = "la"
                }
                else if sylChosenArray[i] == "#6" || sylChosenArray[i] == "b7" || sylChosenArray[i] == "li"
                {
                    sylChosenArray[i] = "te"
                }
                else if sylChosenArray[i] == "7"
                {
                    sylChosenArray[i] = "ti"
                }
                
                sylChosenArray.append(sylChosenArray[i] + "2")
                sylChosenArray[i] += "1"
            }
            
            for sylChosen in sylChosenArray
            {
                //if solfegeArray contains chosen syllable(s) (if what user typed are syllables), melody can be limited to syllables
                if solfegeArray.contains(sylChosen)
                {
                    limitToSyllables = true
                    
                }
            }
        }
        
        //creating notesPlayed array
        for var j = 0; j < numberOfNotesPlayed; j++
        {
            
            //solfegeIndices is an array of solfegeArray indices from 0-11
            solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
            
            //if user inputs syllables, limit melody to those
            if limitToSyllables == true
            {
                
                while !sylChosenArray.contains(solfegeArray[solfegeIndices[j]])
                {
                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                }
            }
                
                //Major scale
            else if scale == "major"
            {
                
                while solfegeArray[solfegeIndices[j]].rangeOfString("ra") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("me") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("fi") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("le") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("te") != nil
                {
                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                }
                
                
            }
                //minor scale
            else if scale == "minor(relative)"
            {
                
                while solfegeArray[solfegeIndices[j]].rangeOfString("ra") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("mi") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("fi") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("la") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("ti") != nil
                {
                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                }
                
            }
            
            //contains the notes ie ["Db3", "A4", "C3"] of the melody
            notesPlayed[j] = solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!
            
            
            answer += solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()) + " "
            
        }
        
        print(answer)
        
        //plays chord then melody with delay of timeBetweenNotes
        delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
            Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
 
        
        
        //if learnMode == false
        //{
        
        //if first problem, play chord
        if problems == 1
        {
            self.playChord(self)
            dispatch_after(delayTime1, dispatch_get_main_queue())
            {
                self.playMelody(self)
            }
        }
        else
        {
            self.playMelody(self)
            message.text = ""
        }
        //}
        scoreLabel.text = "\(points)/\(problems)"
        
    }//CLOSE GENERATE NEW MELODY
    
    
    //@IBOutlet var scaleChosen: UITextView!
    @IBOutlet var scaleChosen: UITextField!
    
    var playMinorChord = false
    @IBOutlet var playChordOutlet: UIButton!
    @IBAction func playChord(sender: AnyObject)
    {
        var rootIndexm3rd = rootIndex + 3
        var rootIndex3rd = rootIndex + 4
        var rootIndex5th = rootIndex + 7
        
        var fileLocationRoot = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndex], ofType: "mp3")
        var fileLocationRootm3rd = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndexm3rd], ofType: "mp3")
        var fileLocationRoot3rd = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndex3rd], ofType: "mp3")
        var fileLocationRoot5th = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndex5th], ofType: "mp3")
        //var error: NSError? = nil
        
        do
        {
            try chord1 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRoot!))
        
        }catch{
            
        }
        
        do{
            try chordm3 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRootm3rd!))
        }catch{}
        do{
            try chord3 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRoot3rd!))
        }catch{}
        do{
            try chord5 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRoot5th!))
        }catch{}
        
        chord1.play()
        chord5.play()
        
        //if scale chosen has "minor" in string, play minor chord
        if scaleChosen.text!.rangeOfString("minor") != nil || playMinorChord == true
        {
            
            chordm3.play()
            playMinorChord = true
            
        }
        else
        {
            chord3.play()
            
        }
        
        //message.font = UIFont(name: message.font.fontName, size: 22)
        //message.textColor = UIColor.blueColor()
        //message.text = "Tonic: \(pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor()))"
    }
    
    @IBOutlet var playMelodyOutlet: UIButton!
    @IBAction func playMelody(sender: AnyObject)
    {
        if tutorialMode == true
        {
            if whichTune != 0
            {
                //play same example again
                whichTune--
            }
            examplesButton(self)
        }
        else
        {
            player.removeAll(keepCapacity: true)
            
            var error: NSError? = nil
            
            var fileLocation = [NSObject]()
            
            var shortStartDelay: NSTimeInterval = 0
            
            for var i = 0; i < numberOfNotesPlayed; i++
            {
                
                //file name of notesPlayed array sound file is appended to fileLocation array
                fileLocation.append(NSBundle.mainBundle().pathForResource(notesPlayed[i], ofType: "mp3" as String)!)
                
                //file location of random sound file is appended to audio player array
                do
                {
                
                    try player.append(AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocation[i] as! String)))
                
                }catch{
                    
                }
                //current time associated with output device "player[0], the first random sound file played"
                let now: NSTimeInterval = player[0].deviceCurrentTime;
                
                player[i].playAtTime(now + shortStartDelay)
                
                shortStartDelay += timeBetweenNotes;
            }
        }
    }
    @IBOutlet var message: UITextView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        examplesOutlet.layer.shadowColor = UIColor.blackColor().CGColor
        examplesOutlet.layer.shadowOffset = CGSizeMake(2, 2)
        examplesOutlet.layer.shadowOpacity = 1
        
        generateNewMelodyOutlet.layer.shadowColor = UIColor.blackColor().CGColor
        generateNewMelodyOutlet.layer.shadowOffset = CGSizeMake(2, 2)
        generateNewMelodyOutlet.layer.shadowOpacity = 1
        
        solfegeText.text = "do(1)"
        
        if tutorialCounter == nil
        {
            tutorialCounter = -1
            NSUserDefaults.standardUserDefaults().setObject(tutorialCounter, forKey: "tutorialCounter")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        if dotiGamePassed == nil
        {
            dotiGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(dotiGamePassed, forKey: "dotiGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if dotisolGamePassed == nil
        {
            dotisolGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(dotisolGamePassed, forKey: "dotisolGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        //["do ti game","do ti sol game","do mi fa game","do sol la game","do re mi game","do me sol game","do sol le game","do me sol te game","do fa fi sol game","do ra ti game"]

        if domifaGamePassed == nil
        {
            domifaGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(domifaGamePassed, forKey: "domifaGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if dosollaGamePassed == nil
        {
            dosollaGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(dosollaGamePassed, forKey: "dosollaGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if doremiGamePassed == nil
        {
            doremiGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(doremiGamePassed, forKey: "doremiGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if domesolGamePassed == nil
        {
            domesolGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(domesolGamePassed, forKey: "domesolGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if dosolleGamePassed == nil
        {
            dosolleGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(dosolleGamePassed, forKey: "dosolleGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if domesolteGamePassed == nil
        {
            domesolteGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(domesolteGamePassed, forKey: "domesolteGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if dofafisolGamePassed == nil
        {
            dofafisolGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(dofafisolGamePassed, forKey: "dofafisolGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        if doratiGamePassed == nil
        {
            doratiGamePassed = false
            NSUserDefaults.standardUserDefaults().setObject(doratiGamePassed, forKey: "doratiGamePassed")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        view.backgroundColor = UIColor(patternImage: UIImage(named: "bluebgBig.jpg")!)
        
        for solfegeOutlet in solfegeCollection
        {
            solfegeOutlet.enabled = false
            
            //making the shadows
            solfegeOutlet.layer.shadowColor = UIColor.blackColor().CGColor
            solfegeOutlet.layer.shadowOffset = CGSizeMake(2, 2)
            solfegeOutlet.layer.shadowOpacity = 1
        }
        
        playChordOutlet.enabled = false
        playMelodyOutlet.enabled = false
        examplesOutlet.hidden = true
        generateNewMelodyOutlet.enabled = false
        navbar.rightBarButtonItem?.enabled = false
        previousOutlet.enabled = false
        
        //resetGuessesOutlet.enabled = false
        
        showAnswerOutlet.enabled = false
        
        message.font = UIFont(name: message.font!.fontName, size: 17)
        message.textColor = UIColor.blackColor()
        message.text = "Welcome, \(PFUser.currentUser()!.username!)! Press Next to begin or continue."
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //@IBOutlet var BPM: UITextView!
    
    @IBOutlet var doOutlet: UIButton!
    @IBOutlet var reOutlet: UIButton!
    @IBOutlet var miOutlet: UIButton!
    @IBOutlet var faOutlet: UIButton!
    @IBOutlet var solOutlet: UIButton!
    @IBOutlet var laOutlet: UIButton!
    @IBOutlet var tiOutlet: UIButton!
    @IBOutlet var raOutlet: UIButton!
    @IBOutlet var meOutlet: UIButton!
    @IBOutlet var fiOutlet: UIButton!
    @IBOutlet var leOutlet: UIButton!
    @IBOutlet var teOutlet: UIButton!
    
    //solfege button collection in array
    @IBOutlet var solfegeCollection: [UIButton]!
    
    //these must be defined outside the func so that the var exists between func calls and is updated each time func is called
    var verticalConstraints = [NSLayoutConstraint]()
    var horizontalConstraints = [NSLayoutConstraint]()
    
    //re-draw the constraints when view changes orientation
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        //calls new key config when transitioning between orientations
        coordinator.animateAlongsideTransition({context in
            self.newKeyConfigurationC()
            
        }, completion: nil)
    }
    
    var fixKeyConfigToC = true //false
    
    func newKeyConfigurationC()
    {
        //initial distance (do) is at left side of screen
        var horizontalDist:CGFloat = 0
        
        var black = UIColor.blackColor()
        var white = UIColor.whiteColor()
        var blue = UIColor.blueColor()
        
        var verticalDist:CGFloat = 0
        
        var bgColor:UIColor
        var tintColor = blue
        
        var i:Int
        
        var indexOfDb:Int = 1
        var indexOfEb:Int = 3
        var indexOfGb:Int = 6
        var indexOfAb:Int = 8
        var indexOfBb:Int = 10
        var indexOfC:Int = -1
        var indexOfF:Int = 5
        
        var halfKeyDisplacement = self.view.frame.size.width/14
        
        self.view.addConstraint(NSLayoutConstraint(item: doOutlet, attribute: .Width, relatedBy: .Equal, toItem: self.view, attribute: .Width, multiplier: 2/16, constant: 0))
        
        for i = 0; i < solfegeCollection.count; i++
        {
            
            //black keys vertical displacement
            if i == indexOfDb || i == indexOfEb || i == indexOfGb || i == indexOfAb || i == indexOfBb
            {
                bgColor = black
                tintColor = white
                verticalDist = -doOutlet.frame.size.height - 2 //-42
            }
                //white keys
            else
            {
                bgColor = white
                tintColor = blue
                verticalDist = 0
            }
            
            //index of 2nd white key in a row (F and/or C) is twice distance away from previous key than if black key were between them
            if i == indexOfC || i == indexOfF
            {
                horizontalDist += halfKeyDisplacement
            }
            
            //put constraints into array
            verticalConstraints.append(NSLayoutConstraint(item: solfegeCollection[i], attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: verticalDist))
            
            //makes sure the constant of each constraint is updated everytime function called
            verticalConstraints[i].constant = verticalDist
            
            //applying vertical position of keys
            self.view.addConstraint(verticalConstraints[i])
            
            solfegeCollection[i].backgroundColor = bgColor
            solfegeCollection[i].tintColor = tintColor
            solfegeCollection[i].layer.borderWidth = 1.5
            solfegeCollection[i].layer.borderColor = UIColor.greenColor().CGColor
            
            //horizontal positions of keys
            horizontalConstraints.append(NSLayoutConstraint(item: solfegeCollection[i], attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: horizontalDist))
            
            horizontalConstraints[i].constant = horizontalDist
            self.view.addConstraint(horizontalConstraints[i])
            
            horizontalDist += halfKeyDisplacement
            
        }
        
    }
    
    //use only in key of C
    @IBAction func doButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "do.jpg")
            
            solfegeText.text = "do(1)"
            
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["do1"]!, ofType: "mp3")
            
            if playHigherDo == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["do2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
            
            }catch
            {
            
            }
            playerSolfege.play()
        }
    
    }
    @IBAction func tiButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "ti.jpg")
            
            solfegeText.text = "ti(7)"
    
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["ti1"]!, ofType: "mp3")
            
            if playHigherTi == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["ti2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
            
            }catch{
                
            }
            
            playerSolfege.play()
        }
    
    }
    @IBAction func solButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "sol.jpg")
        
            solfegeText.text = "sol(5)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["sol1"]!, ofType: "mp3")
            
            if playHigherSol == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["sol2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
    
    }
    @IBAction func miButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "mi.jpg")
            
            solfegeText.text = "mi(3)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["mi1"]!, ofType: "mp3")
            
            if playHigherMi == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["mi2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
    
    }
    @IBAction func laButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "la.jpg")
            
            solfegeText.text = "la(6)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["la1"]!, ofType: "mp3")
            
            if playHigherLa == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["la2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
    
    }
    
    @IBAction func faButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "fa.jpg")
            
            solfegeText.text = "fa(4)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["fa1"]!, ofType: "mp3")
            
            if playHigherFa == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["fa2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
    
    }
    
    @IBAction func reButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "re.jpg")
            
            solfegeText.text = "re(2)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["re1"]!, ofType: "mp3")
            
            if playHigherRe == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["re2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
    
    }
    
    @IBAction func raButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "ra.jpg")
            
            solfegeText.text = "ra(b2)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["ra1"]!, ofType: "mp3")
            
            if playHigherRa == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["ra2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
        
    }
    @IBAction func meButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "me.jpg")
            
            solfegeText.text = "me(b3)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["me1"]!, ofType: "mp3")
            
            if playHigherMe == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["me2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
        
    }
    @IBAction func fiButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "fi.jpg")
            
            solfegeText.text = "fi(#4)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["fi1"]!, ofType: "mp3")
            
            if playHigherFi == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["fi2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
        
    }
    @IBAction func leButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "le.jpg")
            
            solfegeText.text = "le(b6)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["le1"]!, ofType: "mp3")
            
            if playHigherLe == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["le2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
        
    }
    @IBAction func teButton(sender: AnyObject)
    {
        if tutorialMode == true
        {
            solfegeImage.image = UIImage(named: "te.jpg")
            
            solfegeText.text = "te(b7)"
            
            //file location
            var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["te1"]!, ofType: "mp3")
            
            if playHigherTe == true
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["te2"]!, ofType: "mp3")
            }
            
            do
            {
                try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
                
            }catch{
                
            }
            playerSolfege.play()
        }
        
    }
    
    
    var playHigherDo = false
    var playHigherRe = false
    var playHigherMi = false
    var playHigherFa = false
    var playHigherSol = false
    var playHigherLa = false
    var playHigherTi = false
    var playHigherRa = false
    var playHigherMe = false
    var playHigherFi = false
    var playHigherLe = false
    var playHigherTe = false
    
    var tutorialCounter = NSUserDefaults.standardUserDefaults().valueForKey("tutorialCounter") as? Int //-1
    
    var tuneIndex = 0
    var tune = String()
    
    //called for each tone of a tune. Iterate through tune with tuneIndex
    func pressSolfegeButton()
    {
        
        if tuneIndex == 0
        {
            self.message.text = tune
        }
        
        if tune == "Major Scale"
        {
            if tuneIndex == 0
            {
                playChord(self)
            }
            else if tuneIndex == 1
            {
                self.message.text = "The Major scale tones are do (1), re (2), mi (3), fa (4), sol (5), la (6), ti (7), and do (1) an octave higher. 'Do' is also the root (bottom) of the tonic chord. Each scale tone has an image associated with it that describes its feel."
                doButton(self)
            }
            else if tuneIndex == 2
            {
                
                reButton(self)
            }
            else if tuneIndex == 3
            {
                
                miButton(self)
            }
            else if tuneIndex == 4
            {
                
                faButton(self)
            }
            else if tuneIndex == 5
            {
                
                solButton(self)
            }
            else if tuneIndex == 6
            {
                
                laButton(self)
            }
            else if tuneIndex == 7
            {
                
                tiButton(self)
            }
            else if tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
        }
        else if tune == "Somewhere, Over the Rainbow"
        {
            if tuneIndex == 0
            {
                playHigherDo = false
                doButton(self)
            }
            else if tuneIndex == 1
            {
                playHigherDo = true
                doButton(self)
            }
            else if tuneIndex == 2
            {
                tiButton(self)
            }
            else if tuneIndex == 3
            {
                solButton(self)
            }
            else if tuneIndex == 4
            {
                laButton(self)
            }
            else if tuneIndex == 5
            {
                tiButton(self)
            }
            else if tuneIndex == 6
            {
                doButton(self)
            }
        }
        else if tune == "Ave Maria"
        {
            if tuneIndex == 0
            {
                doButton(self)
            }
            else if tuneIndex == 1
            {
                
                tiButton(self)
            }
            else if tuneIndex == 2
            {
                doButton(self)
            }
            else if tuneIndex == 3
            {
                playHigherMi = true
                miButton(self)
                
                playHigherMi = false
            }
            else if tuneIndex == 4
            {
                playHigherRe = true
                reButton(self)
                
                playHigherRe = false
            }
            
        }
        else if tune == "Time to Say Goodbye"
        {
            if tuneIndex == 0
            {
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 4
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 2
            {
                tiButton(self)
            }
            else if tuneIndex == 3
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            
        }
        else if tune == "William Tell Overture"
        {
            
            if tuneIndex <= 6
            {
                solButton(self)
            }
            else if tuneIndex == 7
            {
                miButton(self)
            }
            else if tuneIndex == 8
            {
                doButton(self)
            }
            else if tuneIndex == 9
            {
                miButton(self)
                
            }
            else if tuneIndex == 10
            {
                
                solButton(self)
                
            }
            else if tuneIndex == 11
            {
                
                miButton(self)
                
            }
            else if tuneIndex == 12
            {
                
                solButton(self)
                
            }
            else if tuneIndex == 13
            {
                playHigherDo = true
                doButton(self)
                
                playHigherDo = false
                
            }
            else if tuneIndex == 14
            {
                
                solButton(self)
                
            }
        }
        else if tune == "Superman Theme Song"
        {
            if tuneIndex < 2
            {
                doButton(self)
            }
            else if tuneIndex == 2 || tuneIndex == 3 || tuneIndex == 5 || tuneIndex == 7
            {
                solButton(self)
            }
            else if tuneIndex == 4
            {
                laButton(self)
            }
            else if tuneIndex == 6
            {
                faButton(self)
            }
        }
        else if tune == "Mighty Mouse Theme Song"
        {
            if tuneIndex == 0 || tuneIndex == 6
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 1 || tuneIndex == 5
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            else if tuneIndex == 2 || tuneIndex == 4
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 3
            {
                solButton(self)
            }
        }
        else if tune == "Here Comes the Bride"
        {
            if tuneIndex == 0 || tuneIndex == 4
            {
                
                solButton(self)
                
            }
            else if tuneIndex == 1 || tuneIndex == 2 || tuneIndex == 3 || tuneIndex == 7
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 5
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 6
            {
                tiButton(self)
            }
        }
        else if tune == "William Tell Overture (Theme)"
        {
            if tuneIndex < 8 || tuneIndex >= 11 && tuneIndex < 16 || tuneIndex == 20
            {
                solButton(self)
            }
            else if tuneIndex == 8 || tuneIndex == 16
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 9 || tuneIndex == 18
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 10 || tuneIndex == 17
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            if tuneIndex == 19
            {
                tiButton(self)
            }
        }
        else if tune == "Happy Birthday"
        {
            if tuneIndex == 0 || tuneIndex == 1 || tuneIndex == 3 || tuneIndex == 6 || tuneIndex == 7 || tuneIndex == 9 || tuneIndex == 12 || tuneIndex == 13
            {
                
                solButton(self)
                
            }
            else if tuneIndex == 2 || tuneIndex == 8 || tuneIndex == 18
            {
                
                laButton(self)
                
            }
            else if tuneIndex == 4 || tuneIndex == 11 || tuneIndex == 16 // || tuneIndex == 7
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 5 || tuneIndex == 17
            {
                tiButton(self)
            }
            else if tuneIndex == 10
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 14
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 15
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            
        }
        else if tune == "At Last"
        {
            if tuneIndex == 0 || tuneIndex == 11
            {
                playMinorChord = false
                solButton(self)
                
            }
            else if tuneIndex == 1 || tuneIndex == 3 || tuneIndex == 6 || tuneIndex == 8 || tuneIndex == 9
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 2 || tuneIndex == 5
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 4
            {
                playHigherMe = true
                meButton(self)
                playHigherMe = false
            }
            else if tuneIndex == 7 || tuneIndex == 10
            {
                laButton(self)
            }
        }
        else if tune == "Chopin Waltz Op. 64 No 2"
        {
            if tuneIndex == 0 || tuneIndex == 3 || tuneIndex == 6 || tuneIndex == 8 || tuneIndex == 11
            {
                playMinorChord = true
                solButton(self)
            }
            else if tuneIndex == 1
            {
                playHigherMe = true
                meButton(self)
                playHigherMe = false
            }
            else if tuneIndex == 2 || tuneIndex == 7
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 4 || tuneIndex == 9
            {
                
                teButton(self)
                
            }
            else if tuneIndex == 5 || tuneIndex == 10
            {
                
                leButton(self)
                
            }
            else if tuneIndex == 12
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
        }
        else if tune == "Chopin Nocturne Op. 9 No 2"
        {
            if tuneIndex == 0 || tuneIndex == 6
            {
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 3 || tuneIndex == 7 || tuneIndex == 12
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            else if tuneIndex == 2 || tuneIndex == 4
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 5
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 8 || tuneIndex == 10
            {
                laButton(self)
            }
            else if tuneIndex == 9
            {
                leButton(self)
            }
            else if tuneIndex == 11
            {
                playHigherLa = true
                laButton(self)
                playHigherLa = false
            }
            else if tuneIndex == 5 || tuneIndex == 13
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 5 || tuneIndex == 14
            {
                playHigherFa = true
                faButton(self)
                playHigherFa = false
            }
        }
        else if tune == "Someday My Prince Will Come"
        {
            if tuneIndex == 0
            {
                solButton(self)
            }
            else if tuneIndex == 1
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 2
            {
                leButton(self)
            }
            else if tuneIndex == 3
            {
                tiButton(self)
            }
            else if tuneIndex == 4 || tuneIndex == 5
            {
                laButton(self)
            }
        }
        
        else if tune == "Mary Had a Little Lamb"
        {
            if tuneIndex == 0 || tuneIndex > 3
            {
                playMinorChord = false
                miButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 3
            {
                reButton(self)
            }
            else if tuneIndex == 2
            {
                doButton(self)
            }
        }
        else if tune == "Raindrops Keep Falling on My Head"
        {
            if tuneIndex < 3
            {
                miButton(self)
            }
            else if tuneIndex == 3
            {
                faButton(self)
            }
            else if tuneIndex == 4
            {
                miButton(self)
            }
            else if tuneIndex == 5
            {
                
                reButton(self)
                
            }
            else if tuneIndex == 6
            {
                
                doButton(self)
                
            }
            else if tuneIndex == 7
            {
                
                miButton(self)
                
            }
            
        }
        else if tune == "Brahms' Lullaby"
        {
            if tuneIndex < 2 || tuneIndex == 3 || tuneIndex == 4 || tuneIndex == 6
            {
                miButton(self)
            }
            else if tuneIndex == 2 || tuneIndex == 5 || tuneIndex == 7
            {
                solButton(self)
            }
            else if tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                
                playHigherDo = false
            }
            else if tuneIndex == 9
            {
                
                tiButton(self)
                
            }
            else if tuneIndex == 10 || tuneIndex == 11
            {
                //2x
                laButton(self)
                
            }
            else if tuneIndex == 12
            {
                
                solButton(self)
                
            }
            
        }
        else if tune == "A Whole New World"
        {
            if tuneIndex == 0 || tuneIndex == 3
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            else if tuneIndex == 1
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 2
            {
                playHigherFa = true
                faButton(self)
                playHigherFa = false
            }
            else if tuneIndex == 4
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
                
            }
            else if tuneIndex == 5
            {
                
                solButton(self)
                
            }
            
        }
        else if tune == "Don't Stop Believin'"
        {
            if tuneIndex == 0 || tuneIndex == 3
            {
                playMinorChord = false
                faButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 4
            {
                
                miButton(self)
            }
            else if tuneIndex == 2
            {
                
                reButton(self)
                
            }
        }
        else if tune == "When I Fall in Love"
        {
            if tuneIndex == 0
            {
                
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 4
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
                
            }
            else if tuneIndex == 2
            {
                playHigherFa = true
                faButton(self)
                playHigherFa = false
            }
            else if tuneIndex == 3
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
                
            }
        }
        else if tune == "Happy Birthday (Ending)"
        {
            if tuneIndex == 0 || tuneIndex == 1
            {
                
                faButton(self)
            }
            else if tuneIndex == 2
            {
                
                miButton(self)
            }
            else if tuneIndex == 3 || tuneIndex == 5
            {
                
                doButton(self)
                
            }
            else if tuneIndex == 4
            {
                reButton(self)
            }
        }
        else if tune == "The Hills Are Alive"
        {
            if tuneIndex == 0 || tuneIndex == 2
            {
                playMinorChord = false
                solButton(self)
            }
            else if tuneIndex == 1
            {
                
                laButton(self)
            }
            else if tuneIndex == 3
            {
                
                faButton(self)
                
            }
            else if tuneIndex == 4
            {
                miButton(self)
            }
        }
        else if tune == "What A Wonderful World"
        {
            if tuneIndex == 0
            {
                solButton(self)
            }
            else if tuneIndex == 1
            {
                
                laButton(self)
            }
            else if tuneIndex == 2 || tuneIndex == 3
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
                
            }
            else if tuneIndex == 4 || tuneIndex == 8
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 5 || tuneIndex == 6 || tuneIndex == 7
            {
                playHigherLa = true
                laButton(self)
                playHigherLa = false
            }
            
        }
        else if tune == "Kumbaya"
        {
            if tuneIndex == 0 //|| tuneIndex == 3 || tuneIndex == 6
            {
                doButton(self)
            }
            else if tuneIndex == 1 //|| tuneIndex == 2 || tuneIndex == 4 || tuneIndex == 5 || tuneIndex == 8
            {
                
                miButton(self)
            }
            else if tuneIndex == 2 || tuneIndex == 3 || tuneIndex == 4 || tuneIndex == 7
            {
                
                solButton(self)
            }
            else if tuneIndex == 5 || tuneIndex == 6
            {
                
                laButton(self)
            }
        }
        else if tune == "Singin' in the Rain"
        {
            if tuneIndex == 0 || tuneIndex == 6
            {
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 11
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 2 || tuneIndex == 9
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            else if tuneIndex == 3 || tuneIndex == 8
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 10
            {
                playHigherFa = true
                faButton(self)
                playHigherFa = false
            }
            else if tuneIndex == 7 //|| tuneIndex == 2 || tuneIndex == 4 || tuneIndex == 5 || tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 4 //|| tuneIndex == 2 || tuneIndex == 4 || tuneIndex == 5 || tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 5 //|| tuneIndex == 2 || tuneIndex == 4 || tuneIndex == 5 || tuneIndex == 8
            {
                laButton(self)
            }
        }
        else if tune == "Someone to Watch Over Me"
        {
            if tuneIndex == 0
            {
                playMinorChord = false
                doButton(self)
            }
            else if tuneIndex == 1
            {
                
                reButton(self)
            }
            else if tuneIndex == 2
            {
                
                miButton(self)
                
            }
            else if tuneIndex == 3
            {
                solButton(self)
            }
            else if tuneIndex == 4
            {
                laButton(self)
            }
            else if tuneIndex == 5 || tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 6 || tuneIndex == 7
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 9
            {
                tiButton(self)
                
            }
        }
        else if tune == "Have Yourself A Merry Little Christmas"
        {
            if tuneIndex == 0 || tuneIndex == 8
            {
                doButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 6
            {
                
                miButton(self)
            }
            else if tuneIndex == 2 || tuneIndex == 4
            {
                
                solButton(self)
                
            }
            else if tuneIndex == 3
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 5
            {
                faButton(self)
            }
            else if tuneIndex == 7 || tuneIndex == 9
            {
                reButton(self)
            }
            
        }
        else if tune == "I Loves You Porgy"
        {
            if tuneIndex == 0 //|| tuneIndex == 8
            {
                doButton(self)
            }
            else if tuneIndex == 1 //|| tuneIndex == 6
            {
                
                miButton(self)
            }
            else if tuneIndex == 2 //|| tuneIndex == 4
            {
                
                solButton(self)
                
            }
            else if tuneIndex == 3
            {
                tiButton(self)
            }
            else if tuneIndex == 4
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
        }
        else if tune == "All I Ask of You"
        {
            if tuneIndex == 0 || tuneIndex == 1 || tuneIndex == 2
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 3 //|| tuneIndex == 6
            {
                
                //playHigherDo = true
                doButton(self)
                //playHigherDo = false
            }
            else if tuneIndex == 4 //|| tuneIndex == 4
            {
                
                //playHigherMi = true
                miButton(self)
                //playHigherMi = false
                
            }
            else if tuneIndex == 5
            {
                solButton(self)
            }
            else if tuneIndex == 6 || tuneIndex == 7 || tuneIndex == 8
            {
                laButton(self)
            }
            else if tuneIndex == 9
            {
                solfegeText.text = "ti(7)"
                solfegeImage.image = UIImage(named: "ti.jpg")
                
                var fileLocationB2 = NSBundle.mainBundle().pathForResource("B2", ofType: "mp3")
                
                do{
                    try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationB2!))
                
                }catch{}
                
                playerSolfege.play()
            }
            
/*
            if tuneIndex == 0 || tuneIndex == 2
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 1 //|| tuneIndex == 6
            {
                
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 3 || tuneIndex == 4
            {
                
                playHigherMi = true
                miButton(self)
                playHigherMi = false
                
            }
            else if tuneIndex == 5
            {
                solButton(self)
            }
*/
        }

        else if tune == "Minor Scale"
        {
            if tuneIndex == 0
            {
                playChord(self)
            }
            else if tuneIndex == 1
            {
                self.message.text = "The relative minor scale tones are do (1), re (2), me (b3), fa (4), sol (5), le (b6), te (b7), and back to do."
                doButton(self)
            }
            else if tuneIndex == 2
            {
                
                reButton(self)
            }
            else if tuneIndex == 3
            {
                
                meButton(self)
            }
            else if tuneIndex == 4
            {
                
                faButton(self)
            }
            else if tuneIndex == 5
            {
                
                solButton(self)
            }
            else if tuneIndex == 6
            {
                
                leButton(self)
            }
            else if tuneIndex == 7
            {
                
                teButton(self)
            }
            else if tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
        }
        else if tune == "Alone Together"
        {
            if tuneIndex == 0 || tuneIndex == 2
            {
                doButton(self)
            }
            else if tuneIndex == 1
            {
                
                meButton(self)
            }
            else if tuneIndex == 3 || tuneIndex == 4
            {
                
                reButton(self)
            }
        }
        else if tune == "Beethoven's 5th Symphony"
        {
            if tuneIndex < 3
            {
                solButton(self)
            }
            else if tuneIndex == 3 // || tuneIndex == 6
            {
                
                meButton(self)
            }
            else if tuneIndex == 4 || tuneIndex == 5 || tuneIndex == 6
            {
                
                faButton(self)
            }
            else if tuneIndex == 7 // || tuneIndex == 6
            {
                
                reButton(self)
            }
        }
        else if tune == "Round Midnight"
        {
            if tuneIndex == 0
            {
                solButton(self)
            }
            else if tuneIndex == 1 // || tuneIndex == 6
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 2 // || tuneIndex == 6
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 3 // || tuneIndex == 6
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 4 // || tuneIndex == 6
            {
                playHigherMe = true
                meButton(self)
                playHigherMe = false
            }
        }
        else if tune == "Darth Vader Theme"
        {
            if tuneIndex < 3
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 3 // || tuneIndex == 6
            {
                playHigherLe = true
                leButton(self)
                playHigherLe = false
            }
            else if tuneIndex == 4 || tuneIndex == 7
            {
                playHigherMe = true
                meButton(self)
                playHigherMe = false
            }
            else if tuneIndex == 5
            {
                tiButton(self)
            }
            else if tuneIndex == 6
            {
                leButton(self)
            }
            else if tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            
        }
        else if tune == "Batman Theme"
        {
            if tuneIndex == 0
            {
                doButton(self)
            }
            else if tuneIndex == 1 // || tuneIndex == 6
            {
                
                reButton(self)
            }
            else if tuneIndex == 2 // || tuneIndex == 6
            {
                
                meButton(self)
            }
            else if tuneIndex == 3 // || tuneIndex == 6
            {
                
                leButton(self)
            }
            else if tuneIndex == 4 // || tuneIndex == 6
            {
                
                solButton(self)
            }
        }
        else if tune == "Sleepwalk (Major)"
        {
            
            if tuneIndex == 0
            {
                playMinorChord = false
                playChord(self)
            }
            else if tuneIndex == 1
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 2 || tuneIndex == 4 || tuneIndex == 6
            {
                
                solButton(self)
            }
            else if tuneIndex == 3 || tuneIndex == 7
            {
                
                faButton(self)
            }
            else if tuneIndex == 5
            {
                
                leButton(self)
            }
            
        }
        else if tune == "Revolutionary Etude"
        {
            
            if tuneIndex == 0
            {
                playMinorChord = true
                
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            else if tuneIndex == 1 //|| tuneIndex == 3
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 2 || tuneIndex == 3
            {
                playHigherMe = true
                meButton(self)
                playHigherMe = false
            }
            else if tuneIndex == 4 || tuneIndex == 5 || tuneIndex == 6
            {
                solButton(self)
                
            }
            else if tuneIndex == 7 //|| tuneIndex == 3
            {
                leButton(self)
            }
        }
        else if tune == "Peter Gunn Theme"
        {
            
            if tuneIndex == 0 || tuneIndex == 2
            {
                playMinorChord = true
                teButton(self)
            }
            else if tuneIndex == 1 //|| tuneIndex == 3
            {
                solButton(self)
            }
            else if tuneIndex == 3 //|| tuneIndex == 3
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            else if tuneIndex == 4 //|| tuneIndex == 3
            {
                playHigherRa = true
                raButton(self)
                playHigherRa = false
            }
        }
        else if tune == "Watermelon Man"
        {
            
            if tuneIndex == 0
            {
                playMinorChord = false
                teButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 2 || tuneIndex == 5
            {
                doButton(self)
            }
            else if tuneIndex == 3
            {
                solButton(self)
            }
            else if tuneIndex == 4
            {
                laButton(self)
            }
        }
        else if tune == "Luck Be a Lady"
        {
            
            if tuneIndex == 0 || tuneIndex == 4 || tuneIndex == 6
            {
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 3 //|| tuneIndex == 5
            {
                faButton(self)
            }
            else if tuneIndex == 2 //|| tuneIndex == 2 || tuneIndex == 5
            {
                miButton(self)
            }
            else if tuneIndex == 5
            {
                teButton(self)
            }
        }
        else if tune == "Rockin' Robin"
        {
            
            if tuneIndex == 0 || tuneIndex == 5
            {
                teButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 2 || tuneIndex == 4 || tuneIndex == 6 || tuneIndex == 8
            {
                laButton(self)
            }
            else if tuneIndex == 3 || tuneIndex == 7
            {
                solButton(self)
            }
        }
        else if tune == "Young at Heart"
        {
            
            if tuneIndex == 0  || tuneIndex == 3 || tuneIndex == 6
            {
                fiButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 4 || tuneIndex == 7 //|| tuneIndex == 8
            {
                solButton(self)
            }
            else if tuneIndex == 2 //|| tuneIndex == 7
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            else if tuneIndex == 5 //|| tuneIndex == 7
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            else if tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            if tuneIndex == 9
            {
                meButton(self)
            }
            if tuneIndex == 10
            {
                miButton(self)
            }
            if tuneIndex == 11
            {
                tiButton(self)
            }
            
        }
        else if tune == "Birk's Works"
        {
            
            if tuneIndex == 0 || tuneIndex == 8
            {
                playMinorChord = true
                doButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 7 || tuneIndex == 9 || tuneIndex == 12
            {
                meButton(self)
            }
            if tuneIndex == 2 || tuneIndex == 6 || tuneIndex == 10 || tuneIndex == 11
            {
                
                faButton(self)
            }
            if tuneIndex == 3 || tuneIndex == 5
            {
                
                fiButton(self)
            }
            if tuneIndex == 4 || tuneIndex == 13
            {
                
                solButton(self)
            }
        }
        else if tune == "Back to the Future Theme"
        {
            
            if tuneIndex == 0 || tuneIndex == 3 || tuneIndex == 5
            {
                playMinorChord = false
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 7 //|| tuneIndex == 9 || tuneIndex == 12
            {
                doButton(self)
            }
            if tuneIndex == 2 || tuneIndex == 8
            {
                
                fiButton(self)
            }
            if tuneIndex == 4
            {
                
                laButton(self)
            }
            if tuneIndex == 6
            {
                
                miButton(self)
            }
        }
        else if tune == "The Simpsons Theme"
        {
            
            if tuneIndex == 0 || tuneIndex == 3 //|| tuneIndex == 5
            {
                doButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 5 //|| tuneIndex == 9 || tuneIndex == 12
            {
                fiButton(self)
            }
            if tuneIndex == 2 || tuneIndex == 7
            {
                
                solButton(self)
            }
            if tuneIndex == 4
            {
                
                miButton(self)
            }
            if tuneIndex == 6
            {
                laButton(self)
            }
        }
        else if tune == "Star-Spangled Banner"
        {
            
            if tuneIndex == 0 || tuneIndex == 4 || tuneIndex == 11
            {
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 3 || tuneIndex == 9
            {
                miButton(self)
            }
            if tuneIndex == 2 //|| tuneIndex == 7
            {
                
                doButton(self)
            }
            if tuneIndex == 5 || tuneIndex == 8
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false

            }
            if tuneIndex == 6
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            if tuneIndex == 7
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            if tuneIndex == 10
            {
                fiButton(self)
            }
        }
        else if tune == "Pink Panther Theme"
        {
            
            if tuneIndex == 0 || tuneIndex == 5 //|| tuneIndex == 5
            {
                playMinorChord = true
                doButton(self)
            }
            else if tuneIndex == 1 //|| tuneIndex == 5 //|| tuneIndex == 9 || tuneIndex == 12
            {
                reButton(self)
            }
            if tuneIndex == 2 || tuneIndex == 6
            {
                
                meButton(self)
            }
            if tuneIndex == 3
            {
                
                leButton(self)
            }
            if tuneIndex == 4 || tuneIndex == 7
            {
                solButton(self)
            }
            if tuneIndex == 8
            {
                fiButton(self)
            }
        }
        else if tune == "Cry Me a River"    //ra
        {
            
            if tuneIndex == 0 || tuneIndex == 2 //|| tuneIndex == 5
            {
                playMinorChord = true
                solButton(self)
            }
            else if tuneIndex == 1
            {
                fiButton(self)
            }
            if tuneIndex == 3 || tuneIndex == 5 || tuneIndex == 7
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            if tuneIndex == 4 || tuneIndex == 6
            {
                playHigherRa = true
                raButton(self)
                playHigherRa = false
            }
        }
        else if tune == "Nardis"
        {
            
            if tuneIndex == 0 //|| tuneIndex == 2 //|| tuneIndex == 5
            {
                //playMinorChord = true
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 4 || tuneIndex == 6
            {
                faButton(self)
            }
            if tuneIndex == 2 //|| tuneIndex == 5 || tuneIndex == 7
            {
                meButton(self)
            }
            if tuneIndex == 3 || tuneIndex == 9
            {
                doButton(self)
            }
            if tuneIndex == 5 || tuneIndex == 7
            {
                miButton(self)
            }
            if tuneIndex == 8
            {
                raButton(self)
            }
            
        }
        else if tune == "Han Solo and the Princess"
        {
            
            if tuneIndex == 0 //|| tuneIndex == 6
            {
                playMinorChord = false
                solButton(self)
            }
            else if tuneIndex == 1 || tuneIndex == 3
            {
                playHigherMi = true
                miButton(self)
                playHigherMi = false
            }
            if tuneIndex == 2 || tuneIndex == 5 //|| tuneIndex == 7
            {
                playHigherRe = true
                reButton(self)
                playHigherRe = false
            }
            if tuneIndex == 4 //|| tuneIndex == 9
            {
                playHigherSol = true
                solButton(self)
                playHigherSol = false
            }
            if tuneIndex == 6
            {
                faButton(self)
            }
            if tuneIndex == 7 || tuneIndex == 9
            {
                playHigherRa = true
                raButton(self)
                playHigherRa = false
            }
            if tuneIndex == 10
            {
                playHigherFa = true
                faButton(self)
                playHigherFa = false
            }
            if tuneIndex == 8 || tuneIndex == 12
            {
                playHigherDo = true
                doButton(self)
                playHigherDo = false
            }
            if tuneIndex == 11
            {
                tiButton(self)
            }
            
        }

        
        tuneIndex++
    }
    
    @IBOutlet var examplesOutlet: UIButton!
    
    var whichTune = 0
    @IBAction func examplesButton(sender: AnyObject)
    {
        buttonPressDelayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        
        examplesOutlet.layer.shadowOffset = CGSizeMake(0, 0)
        examplesOutlet.layer.shadowOpacity = 0.5
        
        dispatch_after(buttonPressDelayTime, dispatch_get_main_queue()){
            
            self.examplesOutlet.layer.shadowColor = UIColor.blackColor().CGColor
            self.examplesOutlet.layer.shadowOffset = CGSizeMake(2, 2)
            self.examplesOutlet.layer.shadowOpacity = 1
            
        }
        
        var tunesLimited = [String]()
        
        //var tutorialSection = tutorialSections[tutorialCounter!]
        
        if tutorialSection == "play major scale"
        {
            tunesLimited = ["Major Scale"]
            
        }
        else if tutorialSection == "do ti examples"
        {
            tunesLimited = ["Somewhere, Over the Rainbow", "Ave Maria", "Time to Say Goodbye"]
            
        }
        else if tutorialSection == "sol"
        {
            tunesLimited = ["William Tell Overture", "Superman Theme Song", "Mighty Mouse Theme Song"]
        }
        else if tutorialSection == "sol from below"
        {
            tunesLimited = ["William Tell Overture (Theme)","Happy Birthday","Here Comes the Bride","Chopin Nocturne Op. 9 No 2","Someday My Prince Will Come","Chopin Waltz Op. 64 No 2","At Last","Singin' in the Rain","Han Solo and the Princess"] // chopin nocturne, waltz, fantaisie, marions theme, someday, another you, once in life,
        }
        else if tutorialSection == "mi"    //mi
        {
            tunesLimited = ["Mary Had a Little Lamb", "Raindrops Keep Falling on My Head", "Brahms' Lullaby", "A Whole New World"]
        }
        else if tutorialSection == "fa"    //fa
        {
            tunesLimited = ["Don't Stop Believin'", "When I Fall in Love", "Happy Birthday (Ending)"]
        }
        else if tutorialSection == "la"    //la
        {
            tunesLimited = ["The Hills Are Alive","What A Wonderful World","Kumbaya","Singin' in the Rain"]
            
        }
        else if tutorialSection == "re"   //re
        {
            tunesLimited = ["Someone to Watch Over Me", "Have Yourself A Merry Little Christmas","I Loves You Porgy","All I Ask of You"]
            
            print(tunesLimited)
        }
        else if tutorialSection == "play minor scale"
        {
            tunesLimited = ["Minor Scale"]
        }
        else if tutorialSection == "me"
        {
            tunesLimited = ["Alone Together","Beethoven's 5th Symphony","Round Midnight"]
        }
        else if tutorialSection == "le"
        {
            tunesLimited = ["Darth Vader Theme","Batman Theme","Sleepwalk (Major)","Revolutionary Etude"]//,"Autumn Leaves"]
        }
        else if tutorialSection == "te"
        {
            tunesLimited = ["Peter Gunn Theme","Watermelon Man","Luck Be a Lady","Rockin' Robin"] //mission imp
        }
        else if tutorialSection == "fi"
        {
            tunesLimited = ["Young at Heart","Birk's Works","Back to the Future Theme","The Simpsons Theme","Star-Spangled Banner","Pink Panther Theme"]//maria,baby elephant,agua de beber,be my love', young at heart!!
        }
        else if tutorialSection == "ra"
        {
            tunesLimited = ["Cry Me a River","Nardis","Han Solo and the Princess"]//white rabbit,tool
        }
        
        //reset whichTune to start at first example song
        if whichTune == tunesLimited.count
        {
            whichTune = 0
        }
        
        tune = tunesLimited[whichTune]
        print(tune)
        
        whichTune++
        
        //array of start delays of each tone played in melody
        var shortStartDelayArray = [NSTimeInterval]()
        
        //tune = "Have Yourself A Merry Little Christmas"
        
        if tune == "Major Scale"
        {
            shortStartDelayArray = [0,1,2,3,4,5,6,7,8] //[0,0.5,1,1.5,2,2.5,3,3.5,4]
        }
        else if tune == "Somewhere, Over the Rainbow"
        {
            //over the rainbow
            shortStartDelayArray = [0,1,2,2.5,2.75,3,3.5]
        }
        else if tune == "Ave Maria"
        {
            shortStartDelayArray = [0,1,1.5,2,3]
        }
        else if tune == "Time to Say Goodbye"
        {
            shortStartDelayArray = [0,1,3,3.5,4]
        }
        else if tune == "William Tell Overture"
        {
            shortStartDelayArray = [0,0.75,0.875,1,1.75,1.875,2,2.25,2.5,2.75,3,3.25,3.5,3.75,4]
            //[0,1.5,1.75,2,3.5,3.75,4,4.5,5,5.5,6,6.5,7,7.5,8]
        }
        else if tune == "Superman Theme Song"
        {
            shortStartDelayArray = [0,0.33,0.5,1.25,1.5,1.75,1.875,2]
        }
        else if tune == "Mighty Mouse Theme Song"
        {
            shortStartDelayArray = [0,0.5,0.75,1.25,1.5,1.75,2]
        }
        else if tune == "Here Comes the Bride"
        {
            shortStartDelayArray = [0,0.5,0.83,1,2,2.5,2.83,3]
        }
        else if tune == "William Tell Overture (Theme)"
        {
            shortStartDelayArray = [0,0.125,0.25,0.5,0.625,0.75,1,1.125,1.25,1.5,1.75,2,2.125,2.25,2.5,2.625,2.75,3,3.25,3.5,3.75]//[0,0.5,1,2,2.5,3,4,4.5,5,6,7,8,8.5,9,10,10.5,11,12,13,14,15]
        }
        else if tune == "Happy Birthday"
        {
            shortStartDelayArray = [0,0.25,0.5,1,1.5,2,3,3.25,3.5,4,4.5,5,6,6.25,6.5,7,7.5,8,8.5]
        }
        else if tune == "At Last"
        {
            shortStartDelayArray = [0,1,3.5,3.75,4,4.25,4.5,5.5,6,6.25,6.5,6.75]
        }
            
        else if tune == "Chopin Waltz Op. 64 No 2"
        {
            shortStartDelayArray = [0,0.5,2,3.75,3.875,4,4.375,4.5,5.25,5.375,5.5,5.875,6]//[0,1,4,7.5,7.75,8,8.75,9,10.5,10.75,11,11.75,12]//
        }
        else if tune == "Chopin Nocturne Op. 9 No 2"
        {
            shortStartDelayArray = [0,0.5,2.5,3,3.5,5,6,6.5,7.5,7.66,7.83,8,9,9.5,11]//[0,1,5,6,7,10,12,13,15,15.33,15.66,16,18,19,22
        }
        else if tune == "Someday My Prince Will Come"
        {
            shortStartDelayArray = [0,1.5,2.5,3,4,4.5]//[0,3,5,6,8,9]//
        }
        else if tune == "Mary Had a Little Lamb"
        {
            shortStartDelayArray = [0,0.5,1,1.5,2,2.5,3]
        }
        else if tune == "Raindrops Keep Falling on My Head"
        {
            shortStartDelayArray = [0,1,1.5,2,2.5,3,3.5,4]
        }
        else if tune == "Brahms' Lullaby"
        {
            shortStartDelayArray = [0,0.5,1,3,3.5,4,6,6.5,7,8,9,10,11]
        }
        else if tune == "A Whole New World"
        {
            shortStartDelayArray = [0,0.5,0.875,1.25,1.625,2]
        }
        else if tune == "Don't Stop Believin'"
        {
            shortStartDelayArray = [0,0.75,1.75,2,2.75]
        }
        else if tune == "When I Fall in Love"
        {
            shortStartDelayArray = [0,1,2,3.5,4]
        }
        else if tune == "Happy Birthday (Ending)"
        {
            shortStartDelayArray = [0,0.5,1,2,3,4]
        }
        else if tune == "The Hills Are Alive"
        {
            shortStartDelayArray = [0,0.66,1.33,1.66,2]//,6,6.5,7,8,9,11]
        }
        else if tune == "What A Wonderful World"
        {
            shortStartDelayArray = [0,0.5,1,2.5,3,5.5,6,6.5,7]
        }
        else if tune == "Kumbaya"
        {
            shortStartDelayArray = [0,0.5,1,1.75,2,3,3.5,4]
        }
        else if tune == "Singin' in the Rain"
        {
            shortStartDelayArray = [0,0.5,1.75,2,2.25,2.5,4,4.5,5.75,6,6.25,6.5]//[0,1,3.5,4,4.5,5,8,9,11.5,12,12.5,13]
        }
        else if tune == "Someone to Watch Over Me"
        {
            shortStartDelayArray = [0,0.5,1,1.5,2,2.5,3,3.5,4.5,5]
        }
        else if tune == "Have Yourself A Merry Little Christmas"
        {
            shortStartDelayArray = [0,1,2,3,4,4.5,5,5.5,6,7]
        }
        else if tune == "I Loves You Porgy"
        {
            shortStartDelayArray = [0,0.5,1,1.5,2]
        }
        else if tune == "All I Ask of You"
        {
            shortStartDelayArray = [0,1,2,2.5,3,3.5,4,5,6,6.5]//[0,0.5,1,1.75,2,2.5]
        }
        else if tune == "Minor Scale"   //minor scale with minor chord
        {
            shortStartDelayArray = [0,1,2,3,4,5,6,7,8]
        }
        else if tune == "Alone Together"
        {
            shortStartDelayArray = [0,0.5,2,2.5,3]
        }
        else if tune == "Beethoven's 5th Symphony"
        {
            shortStartDelayArray = [0,0.33,0.66,1,4,4.33,4.66,5]
        }
        else if tune == "Round Midnight"
        {
            shortStartDelayArray = [0,0.5,1,1.5,2]
        }
        else if tune == "Darth Vader Theme"
        {
            shortStartDelayArray = [0,0.5,1,1.5,1.83,2,2.5,2.83,3]
        }
        else if tune == "Batman Theme"
        {
            shortStartDelayArray = [0,1,1.5,2,3]
        }
        else if tune == "Sleepwalk (Major)"
        {
            shortStartDelayArray = [0,1,2,3.5,3.75,4,4.33,4.66] //[0,1,3,6.33,6.66,7,7.66,8.33]
        }
        else if tune == "Revolutionary Etude"
        {
            shortStartDelayArray = [0,0.5,1.16,1.5,4,4.5,5.16,5.5]
        }
        else if tune == "Peter Gunn Theme"
        {
            shortStartDelayArray = [0,2,4,6,6.25]
        }
        else if tune == "Watermelon Man"
        {
            shortStartDelayArray = [0,2.5,2.75,3,3.25,3.75]
            
        }
        else if tune == "Luck Be a Lady"
        {
            shortStartDelayArray = [0,1,1.5,2,2.5,3,4]
           
        }
        else if tune == "Rockin' Robin"
        {
            shortStartDelayArray = [0,0.5,0.75,1,1.25,1.75,2.5,3,3.25]
        }
        else if tune == "Young at Heart"
        {
            shortStartDelayArray = [0,0.5,1,2.33,2.66,3,4.33,4.66,5,5.33,5.66,6]
        }
        else if tune == "Birk's Works"
        {
            shortStartDelayArray = [0,0.45,1,1.45,2,2.45,3,3.45,4,4.45,5,5.45,6.45,7]
        }
        else if tune == "Back to the Future Theme"
        {
            shortStartDelayArray = [0,0.5,1,1.66,1.83,2,2.33,2.66,3]
        }
        else if tune == "The Simpsons Theme"
        {
            shortStartDelayArray = [0,0.5,1.5,3,3.75,4.25,4.75,5]
        }
        else if tune == "Star-Spangled Banner"
        {
            shortStartDelayArray = [0,0.66,1,2,3,4,6,6.66,7,8,9,10]
        }
        else if tune == "Pink Panther Theme"
        {
            shortStartDelayArray = [0,0.66,1,1.66,2,2.66,3,3.66,4]
        }
        else if tune == "Cry Me a River"
        {
            shortStartDelayArray = [0,0.5,1,1.5,3,3.25,3.5,3.75]
        }
        else if tune == "Nardis"
        {
            shortStartDelayArray = [0,0.5,1,1.5,2.5,3.5,3.66,3.83,4,5]//[0,1,2,3,5,7,7.33,7.66,8,10]
        }
        else if tune == "Han Solo and the Princess"
        {
            shortStartDelayArray = [0,0.5,1.75,2,2.25,2.5,4,4.5,5.75,6,6.25,6.5,7.5]//[0,1,3.5,4,4.5,5,8,9,11.5,12,12.5,13,14]
        }
        //call pressSolfegeButton function after each short start delay
        for var i = 0; i < shortStartDelayArray.count; i++
        {
            NSTimer.scheduledTimerWithTimeInterval(shortStartDelayArray[i], target: self, selector: "pressSolfegeButton", userInfo: nil, repeats: false)
        }
        
        //disable user interaction until tune is done
        self.examplesOutlet.userInteractionEnabled = false
        self.playMelodyOutlet.userInteractionEnabled = false
        self.nextOutlet.userInteractionEnabled = false
        self.previousOutlet.userInteractionEnabled = false
        
        var disableTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(shortStartDelayArray[shortStartDelayArray.count - 1] * Double(NSEC_PER_SEC)))
        
        dispatch_after(disableTime, dispatch_get_main_queue()){
            
            self.examplesOutlet.userInteractionEnabled = true
            self.playMelodyOutlet.userInteractionEnabled = true
            self.nextOutlet.userInteractionEnabled = true
            self.previousOutlet.userInteractionEnabled = true
        }
        
        tuneIndex = 0
    }
    
    
    
    var tutorialSections = ["play major scale","do","higher do","ti","do ti examples","do ti game","sol","sol from below","do ti sol game","mi","fa","do mi fa game","la","do sol la game","re","do re mi game","finished major","play minor scale","me","do me sol game","le","do sol le game","te","do me sol te game","finished minor","fi","do fa fi sol game","ra","do ra ti game","finished"]
    
    var tutorialSection = String()
    
    //var gameMode = false
    
    @IBOutlet var previousOutlet: UIButton!
    @IBAction func previousButton(sender: AnyObject)
    {
        tutorialCounter = tutorialCounter! - 2
        nextButton(self)
    
    }
    
    
    @IBOutlet var nextOutlet: UIButton!
    @IBAction func nextButton(sender: AnyObject)
    {
        //WHATS WRONG WITH THE MESSAGE SOMETIMES? NOT ABLE TO SCROLL AND OUT OF VIEW
        
        message.font = UIFont(name: message.font!.fontName, size: 17)
        message.textColor = UIColor.blackColor()
        
        NSUserDefaults.standardUserDefaults().setObject(tutorialCounter, forKey: "tutorialCounter")
        NSUserDefaults.standardUserDefaults().synchronize()
        
        tutorialCounter!++
        
        tutorialSection = tutorialSections[tutorialCounter!]
        
        if tutorialSection.rangeOfString("game") != nil
        {
            //gameMode = true
            
            scoreLabel.hidden = false
            timeLabel.hidden = false
            generateNewMelodyOutlet.hidden = false
            showAnswerOutlet.hidden = false
            showAnswerOutlet.enabled = false
        }
        else
        {
            scoreLabel.hidden = true
            timeLabel.hidden = true
            generateNewMelodyOutlet.hidden = true
            showAnswerOutlet.hidden = true
        }
        
        if tutorialCounter <= tutorialSections.indexOf("finished major")
        {
            print(tutorialSection)
            playMinorChord = false
        }
        
        //print(tutorialSections[tutorialCounter])
        print(tutorialCounter)
        
        previousOutlet.enabled = true
        nextOutlet.enabled = true
        tutorialMode = true
        doOutlet.enabled = true
        
        scaleChosen.enabled = false
        
        //need this?
        playChordOutlet.enabled = true
        
        //var tutorialSection = tutorialSections[tutorialCounter!]
        
        if tutorialSection == "play major scale"
        {
            
            previousOutlet.enabled = false
            
            examplesOutlet.hidden = true
            
            generateNewMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            scaleChosen.enabled = false
            
            scaleChosen.text = "Major"
            
            //examplesOutlet.hidden = false
            playChordOutlet.enabled = true
            playMelodyOutlet.enabled = true
            
            tutorialMode = true
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = true
            }
            
            meOutlet.enabled = false
            raOutlet.enabled = false
            fiOutlet.enabled = false
            leOutlet.enabled = false
            teOutlet.enabled = false
            
            var alertMajor = UIAlertController(title: "First: Major Scale", message: "First, you will hear the major tonic chord followed by the major scale.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertMajor.addAction(UIAlertAction(title: "Hear Major Scale", style: .Default, handler: { (action) -> Void in
                
                self.examplesButton(self)
                
                
            }))
            
            self.presentViewController(alertMajor, animated: true, completion: nil)
        }
        //do
        else if tutorialSection == "do"
        {
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            previousOutlet.enabled = true
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            //playMelodyOutlet.enabled = false
            
            var alertDo = UIAlertController(title: "Next: 'Do'", message: "Now you will hear 'do', the first scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertDo.addAction(UIAlertAction(title: "Hear 'Do'", style: .Default, handler: { (action) -> Void in
                
                self.playHigherDo = false
                self.doButton(self)
                self.message.text = "Notice the image of a house. This is because 'do' feels like home. It is the root (bottom tone) of the tonic chord, and it defines the key you are in (key center)."
                self.doOutlet.enabled = true

            }))
            
            self.presentViewController(alertDo, animated: true, completion: nil)
            
        }
        //do higher
        else if tutorialSection == "higher do"
        {
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            var alertDo2 = UIAlertController(title: "Next: Higher 'Do'", message: "Now you will hear 'do' an octave higher, but still the first scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertDo2.addAction(UIAlertAction(title: "Hear 'Do'", style: .Default, handler: { (action) -> Void in
                
                self.playHigherDo = true
                self.doButton(self)
                
                self.message.text = "This is 'do' an octave higher. When playing the game, listen for the scale tones in different octaves. They still have the same feel, no matter which octave."
                
                
            }))
            
            self.presentViewController(alertDo2, animated: true, completion: nil)
            
        }
        
        //ti
        else if tutorialSection == "ti"
        {
            tiOutlet.enabled = true
            
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            var alertTi = UIAlertController(title: "Next: 'Ti'", message: "Now you will hear 'ti', the seventh major scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertTi.addAction(UIAlertAction(title: "Hear 'Ti'", style: .Default, handler: { (action) -> Void in
                    
                self.tiButton(self)
                self.message.text = "Notice the image of a magnet in position to attract material upward. This is because 'ti' feels like resolving upward to 'do'. Feel the tension it creates as an unstable tone, wanting to resolve up a half step back to 'do'."
                
            }))
            
            self.presentViewController(alertTi, animated: true, completion: nil)

        }
        else if tutorialSection == "do ti examples"
        {
            tiOutlet.enabled = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            //var alertExamples = UIAlertController(title: "Play Some Examples", message: "Press 'Examples' to hear some familiar tunes using these scale tones.", preferredStyle: UIAlertControllerStyle.Alert)
            
            //alertExamples.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                
            message.text = "Press the 'Examples' button to hear some familiar tunes using these scale tones."
                
            //}))
            
            //self.presentViewController(alertTi, animated: true, completion: nil)
            
            //reset this every time examples can be played
            whichTune = 0
            
        }
        //game mode do and ti
        else if tutorialSection == "do ti game"
        {
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            examplesOutlet.hidden = true
            
            scaleChosen.text = "do ti"
            
            navbar.rightBarButtonItem?.enabled = true
            
            if dotiGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "Press the Play button to play the game. You will hear either 'do' or 'ti' after the tonic chord; press the button that matches it. If correct, a new tone to be identified will sound. Try to score \(passingScore) (correct answers * %accuracy) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        
        }
        //sol. Get used to hearing tones not in order and recognizing automatically using feel
        else if tutorialSection == "sol"
        {
            //dotiGamePassed = true
            
            //turn on tutorial mode after every game
            tutorialMode = true
            solOutlet.enabled = true
            doOutlet.enabled = true
            tiOutlet.enabled = true
            
            playHigherDo = false
            
            //reset this every time examples can be played
            whichTune = 0

            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            var alertSol = UIAlertController(title: "Next: 'Sol'", message: "Now you will hear 'sol', the fifth major scale tone, and the top note in the tonic chord.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertSol.addAction(UIAlertAction(title: "Hear 'Sol'", style: .Default, handler: { (action) -> Void in
                
                self.solButton(self)
                self.message.text = "Notice the image of a sun. This is because 'sol' has energy! Try to feel the energy 'sol' has compared to 'do', which feels at rest. Listen to some examples."
                
            }))
            
            self.presentViewController(alertSol, animated: true, completion: nil)
        }
        else if tutorialSection == "sol from below"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            solOutlet.enabled = true
            
            tiOutlet.enabled = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            message.text = "When 'sol' begins a melody from below, it acts like a springboard out of which the rest of the melody arises; it energizes what follows. Listen to some more examples."

        }

        //do ti sol game
        else if tutorialSection == "do ti sol game"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            scaleChosen.text = "do ti sol"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if dotisolGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            //playChordOutlet.enabled = true
            
            message.text = "Let's add 'sol' to the game. Press the Play button. Aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        
        }
        //mi
        else if tutorialSection == "mi"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            tutorialMode = true
            miOutlet.enabled = true
            solOutlet.enabled = true
            doOutlet.enabled = true
            tiOutlet.enabled = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            var alertMi = UIAlertController(title: "Next: 'Mi'", message: "Now you will hear 'mi', the third major scale tone, and the middle note in the major tonic chord. It defines the chord/scale as major.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertMi.addAction(UIAlertAction(title: "Hear 'Mi'", style: .Default, handler: { (action) -> Void in
                
                self.miButton(self)
                self.message.text = "Notice the image of candy. This is because 'mi' feels sweet. Many ballads and sweet songs start with or feature 'mi'. Play some examples."
                
            }))
            
            self.presentViewController(alertMi, animated: true, completion: nil)
        
        }
        //fa
        else if tutorialSection == "fa"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            faOutlet.enabled = true
            
            var alertFa = UIAlertController(title: "Next: 'Fa'", message: "Now you will hear 'fa', the fourth major scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertFa.addAction(UIAlertAction(title: "Hear 'Fa'", style: .Default, handler: { (action) -> Void in
                
                self.faButton(self)
                self.message.text = "Notice the image of a man falling. This is because 'fa' feels like resolving down to 'mi'. It is the opposite of 'ti', which resolves upward. Feel the tension it creates as an unstable tone, wanting to resolve down a half step to 'mi', a stable tone. Play some examples."
                
            }))
            
            self.presentViewController(alertFa, animated: true, completion: nil)
            
        }
        //do mi fa game
        else if tutorialSection == "do mi fa game"
        {
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            scaleChosen.text = "do mi fa"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if domifaGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "Let's play a game using 'do', 'mi' and 'fa'. Press Play and aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        
        }
        //la
        else if tutorialSection == "la"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            tutorialMode = true
            laOutlet.enabled = true
            solOutlet.enabled = true
            tiOutlet.enabled = true
            miOutlet.enabled = true
            doOutlet.enabled = true
            faOutlet.enabled = true
            
            var alertLa = UIAlertController(title: "Next: 'La'", message: "Now you will hear 'la', the sixth major scale tone. It is also the tonic of the relative minor.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertLa.addAction(UIAlertAction(title: "Hear 'La'", style: .Default, handler: { (action) -> Void in
                
                self.laButton(self)
                self.message.text = "Notice the image of a rainbow over the sun ('sol'). This is because 'la' hovers above 'sol'. Listen to some examples."
                
            }))
            
            self.presentViewController(alertLa, animated: true, completion: nil)
        }
        //do sol la game
        else if tutorialSection == "do sol la game"
        {
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "la sol do"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if dosollaGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "Let's play a game using 'do', 'sol' and 'la'. Press Play and aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        //re
        else if tutorialSection == "re"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            tutorialMode = true
            reOutlet.enabled = true
            laOutlet.enabled = true
            solOutlet.enabled = true
            tiOutlet.enabled = true
            miOutlet.enabled = true
            doOutlet.enabled = true
            faOutlet.enabled = true
            
            var alertRe = UIAlertController(title: "Next: 'Re'", message: "Now you will hear 're', the second major scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertRe.addAction(UIAlertAction(title: "Hear 'Re'", style: .Default, handler: { (action) -> Void in
                
                self.reButton(self)
                self.message.text = "Notice the image of a helicopter hovering above ground. This is because, much like 'la' over 'sol', 're' hovers above 'do'. Listen to some examples."
                
            }))
            
            self.presentViewController(alertRe, animated: true, completion: nil)
        }
        //do re mi
        else if tutorialSection == "do re mi game"
        {
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "do re mi"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if doremiGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "Now for our last game for the Major scale! The tones are 'do', 're' and 'mi'. Remember, aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        else if tutorialSection == "finished major"   //finished major
        {
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            message.text = "Great job, \(PFUser.currentUser()!.username!)! Now you are ready to play the game using the Major scale. Press Back to play the game or Next to learn the minor scale."
        }
        else if tutorialSection == "play minor scale"   //finished major
        {
            //reset this every time examples can be played
            whichTune = 0
            
            scaleChosen.text = "minor(relative)"
            
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            tutorialMode = true
            
            doOutlet.enabled = true
            reOutlet.enabled = true
            meOutlet.enabled = true
            faOutlet.enabled = true
            solOutlet.enabled = true
            leOutlet.enabled = true
            teOutlet.enabled = true
            
            var alertMinor = UIAlertController(title: "Next: Minor Scale", message: "Now you will hear the relative minor scale, also called the aeolean scale.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertMinor.addAction(UIAlertAction(title: "Hear Minor Scale", style: .Default, handler: { (action) -> Void in
                
                self.examplesButton(self)
                
                
            }))
            
            self.presentViewController(alertMinor, animated: true, completion: nil)
        }
        else if tutorialSection == "me"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            leOutlet.enabled = false
            teOutlet.enabled = false
            
            var alertMe = UIAlertController(title: "Next: 'Me'", message: "Now you will hear 'me' (b3), the third minor scale tone, and the middle tone of the minor chord.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertMe.addAction(UIAlertAction(title: "Hear 'Me'", style: .Default, handler: { (action) -> Void in
                
                self.meButton(self)
                self.message.text = "Notice the image of a sad face. This is because 'me' replaces 'mi' to define the scale/chord as minor, or sad-sounding. Listen to some examples."
                
            }))
            
            self.presentViewController(alertMe, animated: true, completion: nil)
        }
        else if tutorialSection == "do me sol game"
        {
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "do me sol"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if domesolGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "'Do', 'me' and 'sol'. Remember to aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        else if tutorialSection == "le"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            tutorialMode = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            doOutlet.enabled = true
            reOutlet.enabled = true
            meOutlet.enabled = true
            faOutlet.enabled = true
            solOutlet.enabled = true
            leOutlet.enabled = true
            //teOutlet.enabled = true
            
            var alertLe = UIAlertController(title: "Next: 'Le'", message: "Now you will hear 'le' (b6), the sixth minor scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertLe.addAction(UIAlertAction(title: "Hear 'Le'", style: .Default, handler: { (action) -> Void in
                
                self.leButton(self)
                self.message.text = "Notice the image of thunderstorm. This is because 'le' is dramatic, and is often used in film scores to create tension. Listen to some examples."
                
            }))
            
            self.presentViewController(alertLe, animated: true, completion: nil)
        }
        else if tutorialSection == "do sol le game"
        {
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "do sol le"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if dosolleGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "'Do', 'sol' and 'le'. Remember to aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        else if tutorialSection == "te"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            tutorialMode = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            doOutlet.enabled = true
            reOutlet.enabled = true
            meOutlet.enabled = true
            faOutlet.enabled = true
            solOutlet.enabled = true
            leOutlet.enabled = true
            teOutlet.enabled = true
            
            var alertTe = UIAlertController(title: "Next: 'Te'", message: "Now you will hear 'te' (b7), the seventh minor scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertTe.addAction(UIAlertAction(title: "Hear 'Te'", style: .Default, handler: { (action) -> Void in
                
                self.teButton(self)
                self.message.text = "Notice the image of a cool guy in a suit, because 'te' sounds 'cool'. Listen to some examples."
                
            }))
            
            self.presentViewController(alertTe, animated: true, completion: nil)
        }
        else if tutorialSection == "do me sol te game"
        {
            playMinorChord = true
            
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "do me sol te"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if domesolteGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "'Do', 'me', 'sol' and 'te'. Remember to aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        else if tutorialSection == "finished minor"
        {
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            message.text = "You did it! Now you are ready to play the game using the relative minor scale. Press Back to play the game or Next to learn the remaining scale tones."
        }
        else if tutorialSection == "fi"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            tutorialMode = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            //WHY IS THIS SHOWING?
            showAnswerOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = true
            }
            raOutlet.enabled = false
            
            var alertFi = UIAlertController(title: "Next: 'Fi'", message: "Now you will hear 'fi' (#4), and the middle tone of the chromatic scale.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertFi.addAction(UIAlertAction(title: "Hear 'Fi'", style: .Default, handler: { (action) -> Void in
                
                self.playMinorChord = false
                self.fiButton(self)
                self.message.text = "Notice the image of a balance scale. This is because 'fi' (#4) can resolve either up to 'sol' or down to 'fa' in a minor or blues scale. It is also exactly a tri-tone (3 whole steps) away from 'do' in either direction. It is commonly used in place of 'fa' in many major melodies. Listen to some examples."
                
            }))
            
            self.presentViewController(alertFi, animated: true, completion: nil)
        }
        else if tutorialSection == "do fa fi sol game"
        {
            playMinorChord = false
            
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "do fa fi sol"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if dofafisolGamePassed == false
            {
                //nextOutlet.enabled = false
            }
            
            message.text = "'Do', 'fa', 'fi' and 'sol'. Remember to aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        else if tutorialSection == "ra"
        {
            //reset this every time examples can be played
            whichTune = 0
            
            tutorialMode = true
            
            examplesOutlet.hidden = false
            playMelodyOutlet.enabled = true
            navbar.rightBarButtonItem?.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = true
            }
            
            var alertRa = UIAlertController(title: "Next: 'Ra'", message: "Now you will hear 'ra' (b2), the second chromatic scale tone.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertRa.addAction(UIAlertAction(title: "Hear 'Ra'", style: .Default, handler: { (action) -> Void in
                
                self.raButton(self)
                self.message.text = "Notice the image of a cobra snake. This is because 'ra' (b2) feels exotic. Feel the tension it creates with 'do' in the examples."
                
            }))
            
            self.presentViewController(alertRa, animated: true, completion: nil)
        }
        else if tutorialSection == "do ra ti game"
        {
            playMinorChord = false
            
            examplesOutlet.hidden = true
            //playChordOutlet.enabled = false
            playMelodyOutlet.enabled = false
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = false
            }
            
            scaleChosen.text = "do ra ti"
            //scaleChosen.enabled = true
            
            navbar.rightBarButtonItem?.enabled = true
            
            if doratiGamePassed == false
            {
                //nextOutlet.enabled = false
            }
//            else
//            {
//                nextOutlet.enabled = true
//            }
            
            message.text = "'Do', 'ra' and 'ti'. Remember to aim for immediate recognition by feel. Try to score \(passingScore) points in 1 minute before continuing. Good luck, \(PFUser.currentUser()!.username!)!"
        }
        else if tutorialSection == "finished"
        {
            examplesOutlet.hidden = true
            playMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            nextOutlet.enabled = false
            message.text = "You have learned all 12 scale tones! Now you are ready to play the game using any scale! Press the Back button to play the game. Remember, we learn and create music to affect our emotions. Ear training should utilize how the building blocks of music make us feel. Great job, \(PFUser.currentUser()!.username!)!"
        }
    }

    var numberSystem = false
    override func viewDidAppear(animated: Bool)
    {
        //INTRO TO SCALE TONE METHOD, GIVES YOU ABSOLUTE RELATIVE PITCH. NOT INTERVALLIC TRAINING (IMPRACTICAL) NO TIME, DOESN'T UTILIZE FEEL/FUNCTION OF EACH TONE
        
        scoreLabel.hidden = true
        timeLabel.hidden = true
        generateNewMelodyOutlet.hidden = true
        scaleChosen.hidden = true
        showAnswerOutlet.hidden = true
        examplesOutlet.hidden = true
        
        //EXAMPLE TURNS TO NEXT EXAMPLE, ENABLE REPLAY MELODY AND REPLAY CHORD (USE MESSAGE FOR INSTRUCTIONS)
        print(tutorialCounter)
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        if tutorialCounter == -1
        {
            examplesOutlet.hidden = true
            previousOutlet.enabled = false
            
            generateNewMelodyOutlet.enabled = false
            navbar.rightBarButtonItem?.enabled = false
            
            scaleChosen.enabled = false
        }
        
        //populates solfegeWavDictionary
        populateSolfegeWavDictionary(self)
        
        newKeyConfigurationC()
        
/*
        if numberSystem == true
        {
            doOutlet.setTitle("1", forState: .Normal)
            reOutlet.setTitle("2", forState: .Normal)
            miOutlet.setTitle("3", forState: .Normal)
            faOutlet.setTitle("4", forState: .Normal)
            solOutlet.setTitle("5", forState: .Normal)
            laOutlet.setTitle("6", forState: .Normal)
            tiOutlet.setTitle("7", forState: .Normal)
            raOutlet.setTitle("#1/b2", forState: .Normal)
            meOutlet.setTitle("#2/b3", forState: .Normal)
            fiOutlet.setTitle("#4/b5", forState: .Normal)
            leOutlet.setTitle("#5/b6", forState: .Normal)
            teOutlet.setTitle("#6/b7", forState: .Normal)
        }
        else if numberSystem == false
        {
            doOutlet.setTitle("do", forState: .Normal)
            reOutlet.setTitle("re", forState: .Normal)
            miOutlet.setTitle("mi", forState: .Normal)
            faOutlet.setTitle("fa", forState: .Normal)
            solOutlet.setTitle("sol", forState: .Normal)
            laOutlet.setTitle("la", forState: .Normal)
            tiOutlet.setTitle("ti", forState: .Normal)
            raOutlet.setTitle("di/ra", forState: .Normal)
            meOutlet.setTitle("ri/me", forState: .Normal)
            fiOutlet.setTitle("fi/se", forState: .Normal)
            leOutlet.setTitle("si/le", forState: .Normal)
            teOutlet.setTitle("li/te", forState: .Normal)
        }
*/
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        //end editing when screen tapped
        self.view.endEditing(true)
    }
}