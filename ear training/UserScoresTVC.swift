//
//  UserScoresTVC.swift
//  ear training
//
//  Created by Marcus Choi on 6/29/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class UserScoresTVC: UITableViewController {

    //ADD RANKINGS BASED ON GAMELENGTH. INCLUDE SCORES
    
    var userScores = [String]()
    var toPassUsername: String!
    
    func retrieveScores()
    {
        var query = PFQuery(className: "GameScore")
        
        query.whereKey("username", equalTo: toPassUsername)
        
        query.orderByDescending("createdAt")
        
        query.findObjectsInBackgroundWithBlock({
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                
                // Do something with the found objects
                if let objects = objects as? [PFObject]
                {
                    for object in objects
                    {
                        
                        var score: Int! = object.valueForKey("score") as! Int//as? PFObject
                        print(score)
                        
                        var scale = object.valueForKey("scale") as! String
                        
                        var gameLength:Int! = object.valueForKey("gameLength") as! Int
                        
                        var dataFormatter:NSDateFormatter = NSDateFormatter()
                        dataFormatter.dateFormat = "MM-dd-YY"
                        var date = dataFormatter.stringFromDate(object.createdAt!)
                        
                        //ADD MOVING AND FIXED DO
                        self.userScores.append("\(gameLength)min: \(score), \(scale), \(date)")
                    }
                }
                
                //ok to reload here if not too many users
                self.tableView.reloadData()
                
            }
            else
            {
                // Log details of the failure
                //print("Error: \(error!) \(error!.userInfo!)")
            }
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        retrieveScores()
        
        title = "\(toPassUsername)'s Scores"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        //ARRANGE BY DATE
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return userScores.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //changed for Swift2: used to say as? UITableViewCell
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellID") as UITableViewCell!
        if(cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellID")
        }
        
        cell!.textLabel!.text = userScores[indexPath.row]
        return cell!
        
    }
    
}
