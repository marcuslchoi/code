//
//  PlayOrTutorialViewController.swift
//  ear training
//
//  Created by Marcus Choi on 10/9/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class PlayOrTutorialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(patternImage: UIImage(named: "bluebgBig.jpg")!)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
