//
//  OptionsTVC.swift
//  ear training
//
//  Created by Marcus Choi on 10/28/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class OptionsTVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Return the number of rows in the section.
        return 11
        //celltext.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDSave") as? SaveTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDSave")
            }
            return cell!
        }
        else if indexPath.row == 1
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDGamelength") as? GamelengthTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDGamelength")
            }
            return cell!
        }
        else if indexPath.row == 2
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellID") as? TempoTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellID")
                
            }
            return cell!
        }
        
        else if indexPath.row == 3
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDTonickey") as? TonicKeyTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDTonickey")
            }
            return cell!
        }
        else if indexPath.row == 4
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDNamingSystem") as? NamingSystemTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDNamingSystem")
            }
            return cell!
        }
        else if indexPath.row == 5
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDFixToC") as? FixToCTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDFixToC")
            }
            return cell!
        }
        else if indexPath.row == 6
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDCadence") as? CadenceTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDCadence")
            }
            return cell!
        }
        else if indexPath.row == 7
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDHarmonic") as? HarmonicTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDHarmonic")
            }
            return cell!
        }
        else if indexPath.row == 8
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDAddUser") as? AddUserTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDAddUser")
            }
            return cell!
        }
        else if indexPath.row == 9
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDViewScores") as? ViewScoresTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDViewScores")
            }
            return cell!
        }
        else //if indexPath.row == 10
        {
            var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellIDRankings") as? RankingsTableViewCell
            if(cell == nil)
            {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellIDRankings")
            }
            return cell!
        }
                
    }

}
