//
//  HighScores.swift
//  ear training
//
//  Created by Marcus Choi on 8/1/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class HighScores: NSObject
{
    //string array containing the names of the sections.
    var sections:[String] = []
    
    //an array of string arrays, ie [["user1:300","a:250","c:23"],["user1:400","c:250","a:234"],["a:3003","user1:2500","c:2305"]] one array for each section (gameLength) in descending score order
    var scoresSections:[[String]] = []
    
    
    //We add a section of the menu table by the method addSection, which adds the section and the item array to the existing property
    func addSection(section: String, scoresSection:[String])
    {
        sections = sections + [section]
        //scoresSection is an array of scores for 1 section
        scoresSections = scoresSections + [scoresSection]
    }
    
}

//I subclass MenuItems with PizzaMenuItems.  I override the initializer adding the three sections of pizza. Now I make an instance of PizzaMenuItems whenever I need this list.

/*
class HighScoresInit: HighScores
{
    var gameLengthArray = [1,3,5]
    
    var userScoreArray = [String]()
    
    func retrieveScores()
    {
        var query = PFQuery(className: "HighScore")
        query.whereKey("gameLength", equalTo: gameLengthArray[0])
        query.orderByDescending("score1")
        
        query.findObjectsInBackgroundWithBlock(
        {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                //println("Successfully retrieved \(objects!.count) scores.")
                
                if let objects = objects as? [PFObject]
                {
                    var i = 1
                    
                    for object in objects
                    {
                        
                        var username = object.valueForKey("username") as String
                        
                        var score:Int! = object.valueForKey("score1") as Int
                        
                        var scale = object.valueForKey("scale") as String
                        
                        self.userScoreArray.append("\(i). \(username): \(score) \(scale)")
                        
                        i++
                        
                    }
                }
                
                //
                
                //ok to reload here if not too many users
                //self.tableView.reloadData()
                
            }
            else
            {
                println("Error: \(error!) \(error!.userInfo!)")
            }
        })
        
    }

    
    override init()
    {
        super.init()
        
        retrieveScores()
        
        println(self.userScoreArray)
        
        addSection("1-Minute Game", scoresSection: userScoreArray)
        addSection("3-Minute Game", scoresSection: ["Sausage","Meat Lover's","Veggie Lover's","BBQ Chicken","Mushroom","Special"])
        addSection("5-Minute Game", scoresSection: ["Sausage","Chicken Pesto","Prawns and Mushrooms","Primavera", "Meatball"])
    }
}
*/