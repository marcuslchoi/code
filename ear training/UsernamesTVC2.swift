//
//  UsernamesTVC2.swift
//  ear training
//
//  Created by Marcus Choi on 8/22/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class UsernamesTVC2: UITableViewController {
    
    var usernames = NSUserDefaults.standardUserDefaults().objectForKey("usernames") as! [String]
    
    let userScoresSegueIdentifier = "showUserScoresSegue"
    
    //segue to scores when click on a user
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == userScoresSegueIdentifier
        {
            if let destination = segue.destinationViewController as? UserScoresTVC2
            {
                if let userIndex = tableView.indexPathForSelectedRow?.row
                {
                    destination.usernameSelected = self.usernames[userIndex]
                }
                
            }
            
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        title = "Users"
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    //deleting a user from list
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete
        {
            self.usernames.removeAtIndex(indexPath.row)
            NSUserDefaults.standardUserDefaults().setObject(self.usernames, forKey: "usernames")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return usernames.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellID") as UITableViewCell!
        if(cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellID")
        }
        
        cell!.textLabel!.text = usernames[indexPath.row]
        
        return cell!
        
    }
}
