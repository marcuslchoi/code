//
//  GamelengthTableViewCell.swift
//  ear training
//
//  Created by Marcus Choi on 10/29/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class GamelengthTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var gameLengthOutlet: UISegmentedControl!
    @IBAction func gameLengthValueChanged(sender: AnyObject)
    {
        var gameLength = Int()
        if gameLengthOutlet.selectedSegmentIndex == 0
        {
            gameLength = 1
        }
        else if gameLengthOutlet.selectedSegmentIndex == 1
        {
            gameLength = 3
        }
        else if gameLengthOutlet.selectedSegmentIndex == 2
        {
            gameLength = 5
        }
        gameLengthText.text = "Game Length: \(gameLength) min"
    
    }

    @IBOutlet var gameLengthText: UITextView!
}
