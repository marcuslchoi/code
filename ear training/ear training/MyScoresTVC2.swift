//
//  MyScoresTVC2.swift
//  ear training
//
//  Created by Marcus Choi on 8/4/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

import Parse

class MyScoresTVC2: UITableViewController
{

    let myScores = MyScores()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var array = ["123: 1min. Bebop","2343: 3min. Major"]
        
        //myScores.addSection("todays date", scoresSection: array)//userScoreArrays[2])
        
        tableView.reloadData()
    
        
        var query = PFQuery(className: "GameScore")
        query.whereKey("username", equalTo: PFUser.currentUser().username)
        query.orderByDescending("createdAt")
 
        query.findObjectsInBackgroundWithBlock(
        {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                println("Successfully retrieved \(objects!.count) scores.")
                
                if let objects = objects as? [PFObject]
                {
                    var lastDate:String?
                    var currentDate:String
                    
                    var uniqueDatesArray = [String]()
                    
                    
                    
                    for object in objects
                    {
                        var dateFormatter:NSDateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "MM-dd-YY"
                        currentDate = dateFormatter.stringFromDate(object.createdAt)
                        
                        println(currentDate)
                        
                        if !contains(uniqueDatesArray, currentDate)
                        {
                            uniqueDatesArray.append(currentDate)
                        }
                    
                    }
                    
                    var myScoreArrays = [[String]](count: uniqueDatesArray.count, repeatedValue: [""])
                    
                    println(uniqueDatesArray)
                    
                    for object in objects
                    {

                        var dataFormatter:NSDateFormatter = NSDateFormatter()
                        dataFormatter.dateFormat = "MM-dd-YY"
                        currentDate = dataFormatter.stringFromDate(object.createdAt)
 
/*
                        if !contains(uniqueDatesArray, currentDate)
                        {
                            uniqueDatesArray.append(currentDate)
                        }
                        
*/
                        var gameLength:Int = object.valueForKey("gameLength") as Int
                        
                        var score:Int! = object.valueForKey("score") as Int
                        
                        var scale = object.valueForKey("scale") as String
                        
                        var i = 0
                        
                        //if current date is a new date, go to next index of myScoreArrays and add scores corresponding to next date
                        if currentDate != lastDate
                        {
                            i++
                        }
                        
                        myScoreArrays[i].append("\(score) \(scale)")
                        
                        
                        //keep this at the bottom. Makes this current date into last date for next object
                        lastDate = currentDate
                        
                    }
                    println(myScoreArrays)
                    
                    for var i = 0; i < uniqueDatesArray.count; i++
                    {
                        self.myScores.addSection(uniqueDatesArray[i], scoresSection: myScoreArrays[i])
                    }
                }
                
                self.tableView.reloadData()
                
            }
            else
            {
                println("Error: \(error!) \(error!.userInfo!)")
            }
        })


    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()

    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        // Return the number of sections.
        return myScores.sections.count
    }
    
    //We count the array of items for the current section
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        // Return the number of rows in the section.
        return myScores.scoresSections[section].count
    }
    
    override func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        //instantiate a cell
        let cell = tableView.dequeueReusableCellWithIdentifier("cell",
            forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel?.text = myScores.scoresSections[indexPath.section][indexPath.row]
        return cell
    }
    
    //returns a string which is a title for a header row
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return myScores.sections[section]
    }
    

}
