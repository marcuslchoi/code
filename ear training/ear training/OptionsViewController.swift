//
//  OptionsViewController.swift
//  ear training
//
//  Created by Marcus Choi on 6/7/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

protocol OptionsViewControllerDelegate
{
    func myVCDidFinish(controller:OptionsViewController, bpmPassed:Int, gameLengthIndexPassed:Int, numbSolfPassed:Int, playCadencePassed:Bool, rootIndexPassed:Int, pitchRangePassed:Int, movingTonicPassed:Bool, fixKeyConfigPassed:Bool, harmonicQuestionPassed: Bool, harmonicTypePassed:Int)

}


class OptionsViewController: UIViewController, UITextFieldDelegate {
    
    
    
    @IBAction func infoAction(sender: AnyObject) {
    
        let alertInfo = UIAlertController(title: "More Info", message: "Go to marcuschoipiano.com for more information", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertInfo.addAction(UIAlertAction(title: "Go", style: .Default, handler: { (action) -> Void in
            
            //send to app store
            UIApplication.sharedApplication().openURL(NSURL(string: "http://marcuschoipiano.com/")!)
            
        }))
        
        alertInfo.addAction(UIAlertAction(title: "Close", style: .Default, handler: { (action) -> Void in
            
        }))
        
        self.presentViewController(alertInfo, animated: true, completion: nil)
    }
    
    @IBOutlet var pitchRangeLabel: UILabel!
    @IBOutlet var movingTonicLabel: UILabel!
    @IBOutlet var harmonicQuestionLabel: UILabel!
    
    @IBOutlet var buyProVersionOutlet: UIButton!
    
    @IBAction func buyProVersionAction(sender: AnyObject)
    {
        PFPurchase.buyProduct("proVersion") {
            (error: NSError?) -> Void in
            if error == nil
            {
                print("purchased")
                
                var alertPurchased = UIAlertController(title: "Congratulations!", message: "You have purchased the Pro Version", preferredStyle: UIAlertControllerStyle.Alert)
                alertPurchased.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                    
                }))
                
                self.presentViewController(alertPurchased, animated: true, completion: nil)
                
                self.buyProVersionOutlet.hidden = true
                self.restorePurchaseOutlet.hidden = true
                
                self.tempoOutlet.enabled = true
                self.movingTonicOutlet.enabled = true
                self.harmonicQuestionOutlet.enabled = true
                self.pitchRangeOutlet.enabled = true
                
                self.tempoText.layer.opacity = 1
                self.harmonicQuestionLabel.layer.opacity = 1
                self.pitchRangeLabel.layer.opacity = 1
                self.movingTonicLabel.layer.opacity = 1
            }
        }
    
    }
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var tutorialOutlet: UIBarButtonItem!
    
    var toPassBPM: Int!
    var toPassGameLength: Int!
    var toPassNumbSolf: Bool!
    var toPassPlayCadence: Bool!
    var toPassRootIndex: Int!
    var toPassPitchRange: Int!
    var toPassMovingTonic: Bool!
    var toPassFixKeyConfig: Bool!
    var toPassHarmonicQuestion: Bool!
    var toPassHarmonicType: Int!
    
    var newRootIndex: Int!
    
    var storedUsernames = [String]() //= NSUserDefaults.standardUserDefaults().valueForKey("usernames") as! [String]
    
    @IBOutlet var tempoText: UILabel!
    
    @IBOutlet var tempoOutlet: UISlider!
    @IBAction func tempoValueChanged(sender: AnyObject)
    {
        var tempo = Int(tempoOutlet.value)
        tempoText.text = "Tempo: \(tempo) BPM"
    
    }
    
    var tonicKey = String()
    var tonicKeyInt: Int!
    @IBOutlet weak var tonicKeyText: UILabel!
    @IBOutlet weak var tonicKeyOutlet: UISlider!    
    @IBAction func tonicKeyValueChanged(sender: AnyObject)
    {
        tonicKeyInt = Int(tonicKeyOutlet.value)
        
        if tonicKeyInt == 0
        {
            tonicKey = "C"
        }
        else if tonicKeyInt == 1
        {
            tonicKey = "Db"
        }
        else if tonicKeyInt == 2
        {
            tonicKey = "D"
        }
        else if tonicKeyInt == 3
        {
            tonicKey = "Eb"
        }
        else if tonicKeyInt == 4
        {
            tonicKey = "E"
        }
        else if tonicKeyInt == 5
        {
            tonicKey = "F"
        }
        else if tonicKeyInt == 6
        {
            tonicKey = "Gb"
        }
        else if tonicKeyInt == 7
        {
            tonicKey = "G"
        }
        else if tonicKeyInt == 8
        {
            tonicKey = "Ab"
        }
        else if tonicKeyInt == 9
        {
            tonicKey = "A"
        }
        else if tonicKeyInt == 10
        {
            tonicKey = "Bb"
        }
        else if tonicKeyInt == 11
        {
            tonicKey = "B"
        }
        else //if tonicKeyInt == 12
        {
            tonicKey = "Random"
        }
        
        tonicKeyText.text = "Tonic Key: \(tonicKey)"
    
    }
    
    var pitchRangeDescription = String()
    var pitchRangeDescriptionArray = ["Low","Low-Middle","Middle","Middle-High","High","Low to High"]
    @IBOutlet weak var pitchRangeText: UILabel!
    @IBOutlet weak var pitchRangeOutlet: UISlider!
    @IBAction func pitchRangeValueChanged(sender: AnyObject)
    {
        var pitchRangeInt = Int(pitchRangeOutlet.value)
        
        pitchRangeDescription = pitchRangeDescriptionArray[pitchRangeInt - 1]
        
        pitchRangeText.text = "Pitch Range: \(pitchRangeDescription)"
    }
    
    
    @IBOutlet weak var movingTonicOutlet: UISwitch!
    
    @IBOutlet weak var playCadenceOutlet: UISegmentedControl!
    
    var pianoWavArrayMiddle = ["C3", "Db3", "D3", "Eb3", "E3", "F3", "Gb3", "G3", "Ab3", "A3", "Bb3", "B3", "C4", "Db4", "D4", "Eb4", "E4", "F4", "Gb4", "G4", "Ab4", "A4", "Bb4", "B4"]
    
    var delegate:OptionsViewControllerDelegate? = nil
    
        @IBOutlet var fixKeyConfigOutlet: UISwitch!
    
    @IBOutlet var gameLengthOutlet: UISegmentedControl!
    
    @IBOutlet var numbSolfOutlet: UISegmentedControl!
    
    @IBOutlet weak var harmonicTypeOutlet: UISegmentedControl!
    
    
    @IBOutlet weak var harmonicQuestionOutlet: UISwitch!
    @IBAction func harmonicQuestionValueChanged(sender: AnyObject)
    {
        if harmonicQuestionOutlet.on == true
        {
            harmonicTypeOutlet.enabled = true
        }
        else
        {
            harmonicTypeOutlet.enabled = false
        }
    
    }
    
    @IBOutlet weak var diatonicRootLabel: UILabel!
    @IBOutlet weak var diatonicAllPositionsLabel: UILabel!
    @IBOutlet weak var randomHarmonicLabel: UILabel!
    
    @IBAction func saveButton(sender: AnyObject)
    {
        if delegate != nil
        {
            if tonicKey != "Random"
            {
                //find gives index of chosen key in pianoWavArrayMiddle
                //newRootIndex = find(pianoWavArrayMiddle, "\(tonicKey)3")
                
                //chaned to indexOf for Swift2
                newRootIndex = pianoWavArrayMiddle.indexOf("\(tonicKey)3")
            }
            else //if tonicKey == "Random"
            {
                //root index is a random number from 0 to tonic key's max value - 1
                newRootIndex = Int(arc4random_uniform(UInt32(tonicKeyOutlet.maximumValue - 1)))
            }
            
            var playCadenceBool = Bool()
            if playCadenceOutlet.selectedSegmentIndex == 0
            {
                playCadenceBool = false
            }
            else
            {
                playCadenceBool = true
            }
            
            delegate!.myVCDidFinish(self, bpmPassed: Int(tempoOutlet.value), gameLengthIndexPassed: gameLengthOutlet!.selectedSegmentIndex, numbSolfPassed: numbSolfOutlet!.selectedSegmentIndex, playCadencePassed: playCadenceBool, rootIndexPassed: newRootIndex, pitchRangePassed: Int(pitchRangeOutlet.value), movingTonicPassed: movingTonicOutlet.on, fixKeyConfigPassed: fixKeyConfigOutlet.on, harmonicQuestionPassed: harmonicQuestionOutlet.on, harmonicTypePassed: harmonicTypeOutlet!.selectedSegmentIndex)
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //don't want this functionality any more
        logOutOutlet.hidden = true
        
        if isPro != true
        {
            buyProVersionOutlet.hidden = false
            
            tempoOutlet.enabled = false
            movingTonicOutlet.enabled = false
            harmonicQuestionOutlet.enabled = false
            pitchRangeOutlet.enabled = false
            
            tempoText.layer.opacity = 0.4
            movingTonicLabel.layer.opacity = 0.4
            pitchRangeLabel.layer.opacity = 0.4
            harmonicQuestionLabel.layer.opacity = 0.4
            
        }
        else
        {
            buyProVersionOutlet.removeFromSuperview()
        }
        
        scrollView.contentSize.height = 800
        
        self.navigationItem.rightBarButtonItem = tutorialOutlet
    
        //making images out of labels to display on harmonic type in order to have multi-line text
        var diatonicRootImage = diatonicRootLabel.toImage()
        var diatonicAllPositionsImage = diatonicAllPositionsLabel.toImage()
        var randomHarmonicImage = randomHarmonicLabel.toImage()
        harmonicTypeOutlet.setImage(diatonicRootImage, forSegmentAtIndex: 0)
        harmonicTypeOutlet.setImage(diatonicAllPositionsImage, forSegmentAtIndex: 1)
        harmonicTypeOutlet.setImage(randomHarmonicImage, forSegmentAtIndex: 2)
        
        //hide the original labels because their associated images are seen on harmonicType outlet
        diatonicRootLabel.hidden = true
        diatonicAllPositionsLabel.hidden = true
        randomHarmonicLabel.hidden = true
        
        //add current username to nsuserdefaults if it's not there
        var currentUsername = PFUser.currentUser()?.username as String!
        
        //create usernames in nsuserdefaults if first login
        if NSUserDefaults.standardUserDefaults().objectForKey("usernames") == nil
        {
            storedUsernames.append(currentUsername)
            NSUserDefaults.standardUserDefaults().setValue(storedUsernames, forKey: "usernames")
        }
        else
        {
            storedUsernames = NSUserDefaults.standardUserDefaults().valueForKey("usernames") as! [String]
        }
        
        //if the current player erased own name
        if !storedUsernames.contains(currentUsername)
        {
            storedUsernames.append(currentUsername)
            NSUserDefaults.standardUserDefaults().setObject(storedUsernames, forKey: "usernames")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        title = "Options"

        if toPassGameLength == 1
        {
            gameLengthOutlet.selectedSegmentIndex = 0
        }
        else if toPassGameLength == 3
        {
            gameLengthOutlet.selectedSegmentIndex = 1
        }
        else if toPassGameLength == 5
        {
            gameLengthOutlet.selectedSegmentIndex = 2
        }
        
        tempoOutlet.value = Float(toPassBPM)
        tempoText.text = "Tempo: \(toPassBPM) BPM"
        
        tonicKeyOutlet.value = Float(toPassRootIndex)
        tonicKey = pianoWavArrayMiddle[toPassRootIndex].substringToIndex(pianoWavArrayMiddle[toPassRootIndex].endIndex.predecessor())
        tonicKeyText.text = "Tonic Key: \(tonicKey)"
        
        pitchRangeOutlet.value = Float(toPassPitchRange)
        pitchRangeDescription = pitchRangeDescriptionArray[toPassPitchRange - 1]
        pitchRangeText.text = "Pitch Range: \(pitchRangeDescription)"
        
        movingTonicOutlet.on = toPassMovingTonic
        
        if toPassNumbSolf == true
        {
            numbSolfOutlet.selectedSegmentIndex = 1
        }
        else if toPassNumbSolf == false
        {
            numbSolfOutlet.selectedSegmentIndex = 0
        }
        
        if toPassPlayCadence == true
        {
            playCadenceOutlet.selectedSegmentIndex = 1
        }
        else
        {
            playCadenceOutlet.selectedSegmentIndex = 0
        }
            
        if toPassFixKeyConfig == true
        {
            fixKeyConfigOutlet.on = true
        }
        else
        {
            fixKeyConfigOutlet.on = false
        }
        
        if toPassHarmonicQuestion == true
        {
            harmonicQuestionOutlet.on = true
            harmonicTypeOutlet.enabled = true
        }
        else
        {
            harmonicQuestionOutlet.on = false
            harmonicTypeOutlet.enabled = false
        }
        
        harmonicTypeOutlet.selectedSegmentIndex = toPassHarmonicType
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == addUsernameTextField
        {
           //self.view.frame.origin.y -= 170
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == addUsernameTextField
        {
            //self.view.frame.origin.y = 0
        }
    }

    //moving frame doesnt work without these
    func keyboardWillShow(sender: NSNotification) {
    }
    func keyboardWillHide(sender: NSNotification) {
    }

    @IBOutlet var addUsernameTextField: UITextField!
    
    @IBAction func addUsernameButton(sender: AnyObject)
    {
        
        var usernameQuery = PFUser.query()
        usernameQuery!.whereKey("username", equalTo: addUsernameTextField.text!.lowercaseString)
        
        usernameQuery?.getFirstObjectInBackgroundWithBlock({ (object: AnyObject?, error: NSError?) -> Void in
            
            //username exists, may or may not have been already added
            if error == nil
            {
                //user exists but previously added
                if self.storedUsernames.contains(self.addUsernameTextField.text!.lowercaseString)
                {
                    let alert = UIAlertView()
                    alert.title = "User Already Added"
                    alert.message = "Enter Another Username"
                    alert.addButtonWithTitle("OK")
                    alert.show()
                }
                else    //new username added
                {
                    let alert = UIAlertView()
                    alert.title = "Username Added!"
                    //alert.message = "Enter Another Username"
                    alert.addButtonWithTitle("OK")
                    alert.show()
                                        
                    self.storedUsernames.append(self.addUsernameTextField.text!.lowercaseString)
                    
                    NSUserDefaults.standardUserDefaults().setObject(self.storedUsernames, forKey: "usernames")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    print(NSUserDefaults.standardUserDefaults().objectForKey("usernames")!)
                }
            }
            else
            {
                let alert = UIAlertView()
                alert.title = "User Does Not Exist or No Network Connection"
                alert.message = "Enter Valid Username or Try Again Later"
                alert.addButtonWithTitle("OK")
                alert.show()
            }
            
        })
        
    }
    @IBOutlet weak var restorePurchaseOutlet: UIButton!
    
    @IBAction func restorePurchases(sender: AnyObject)
    {
        PFPurchase.restore()
        //restorecompletedtransactions
    
    }
    
    
    
    //not using log out function
    @IBOutlet weak var logOutOutlet: UIButton!
    @IBAction func logOut(sender: AnyObject)
    {
        PFUser.logOutInBackgroundWithBlock { (error) -> Void in
            if error != nil
            {
                print("logout fail")
            }
            else
            {
                print("logged out")
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //end editing when screen tapped
        self.view.endEditing(true)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
