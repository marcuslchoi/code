//
//  RankingsViewController.swift
//  ear training
//
//  Created by Marcus Choi on 6/17/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class RankingsViewController: UIViewController {

    @IBOutlet var scoreSummary: UILabel!
    
    var score = ""
    
    var myScores = [AnyObject]()

    func retrieveScores()
    {
        var query = PFQuery(className: "GameScore")
        
        query.whereKey("username", equalTo: PFUser.currentUser().username)
        
        query.findObjectsInBackgroundWithBlock({
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                // The find succeeded.
                println("Successfully retrieved \(objects!.count) scores.")
 
                // Do something with the found objects
                if let objects = objects as? [PFObject]
                {
                    for object in objects
                    {
                        
                        var score1 = object["score"]// as NSString
                        //self.scoreSummary.text = object.valueForKey("score") as? String
                        println(score1)//object.valueForKey("score") )
                        
                        self.myScores.append(score1)
                        
                        self.scoreSummary.text = self.myScores[0] as? NSString
                    }
                }
            }
            else
            {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        })
    
    
    }
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        retrieveScores()
        
        
/*
        query.findObjectsInBackgroundWithBlock({
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                if let objects = objects as? PFObject
                {
                    for object in objects
                    {
                        if let score = object as? PFObject
                        {
                            self.myScores.append(score.score)
                        }
                    
                    }
                    
                }
            }
            else
            {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        })
*/
        
//        let object = query.getFirstObjectInBackground()
//        println(object.valueForKey("score"))
//        let score = object.valueForKey("score") as? String
        
        
        //scoreSummary.text = object.valueForKey("score") as? String
/*
        query.getFirstObjectInBackgroundWithBlock
        {
            (object: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                // The find succeeded.
                println("Successfully retrieved \(object)")

                if let object = object as? [PFObject]
                {
                    let score = object["score"] as String

                    self.scoreSummary.text = score//object.valueForKey("score") as? NSString
                }

            }
            else
            {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
        }
*/



   /*
        query.getObjectInBackgroundWithId("wVh8uxntZ6", block: { (object: PFObject?, error: NSError?) -> Void in
            if error != nil
            {
                println(error)
                
            }
            else if let user = object
            {
                user["score"] = 3242354
                
                println(user.objectForKey("score"))
                //user.saveInBackground()
            }
        })
  */
        //var gameScore = PFObject(className: "GameScore")
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
