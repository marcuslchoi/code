//
//  MyScoresTVC.swift
//  ear training
//
//  Created by Marcus Choi on 6/29/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class MyScoresTVC: UITableViewController
{

    var myScores = [String]()
    
    func retrieveScores()
    {
        let query = PFQuery(className: "GameScore")
        
        query.whereKey("username", equalTo: PFUser.currentUser()!.username!)
        
        //query.orderByAscending("gameLength")
        query.orderByDescending("createdAt")
        
        
        query.findObjectsInBackgroundWithBlock({
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                // The find succeeded.
                //println("Successfully retrieved \(objects!.count) scores.")
                
                // Do something with the found objects
                if let objects = objects as? [PFObject]
                {
                    for object in objects
                    {
                        
                        let score: Int! = object.valueForKey("score") as! Int
                        
                        
                        let scale = object.valueForKey("scale") as! String
                        
                        let gameLength:Int! = object.valueForKey("gameLength") as! Int
                        
                        let dataFormatter:NSDateFormatter = NSDateFormatter()
                        dataFormatter.dateFormat = "MM-dd-YY"
                        let date = dataFormatter.stringFromDate(object.createdAt!)
                        
                        self.myScores.append("\(gameLength)min: \(score) \(scale) \(date)")
                    }
                }
                
                //ok to reload here if not too many users
                self.tableView.reloadData()
                
            }
            else
            {
                // Log details of the failure
                //println("Error: \(error!) \(error!.userInfo!)")
            }
        })
        
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        retrieveScores()
        
        title = "My Scores"

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return myScores.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellID") as UITableViewCell!
        if(cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellID")
        }
        
        cell!.textLabel!.text = myScores[indexPath.row]
        
        return cell!
        
    }


}
