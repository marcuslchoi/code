//
//  ScoreSummary.swift
//  ear training
//
//  Created by Marcus Choi on 6/28/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class ScoreSummary: PFQueryTableViewController
{
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(className aClassName: String!) {
        super.init(className: aClassName)
        
        self.parseClassName = aClassName
        self.textKey = "YOUR_PARSE_COLOMN_YOU_WANT_TO_SHOW"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
    }

   
}
