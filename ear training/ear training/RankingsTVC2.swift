//
//  RankingsTVC2.swift
//  ear training
//
//  Created by Marcus Choi on 8/1/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class RankingsTVC2: UITableViewController
{
    
    //HighScores is a type of NSObject
    let highScores = HighScores()
    
    var masteryLevelArrays = [[" "],[" "],[" "]]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
            Int64(1 * Double(NSEC_PER_SEC)))
        
        var delayTime2 = dispatch_time(DISPATCH_TIME_NOW,
            Int64(2 * Double(NSEC_PER_SEC)))
        
        var gameLengthArray = [1,3,5]
        
        //initially set as divider between my ranking and all rankings
        var userScoreArrays = [["_________________________"],["_________________________"],["_________________________"]]

        
        var myRanking:Int32! // = Int32()
        
        var queryHighScore0 = PFQuery(className: "HighScore")
        queryHighScore0.whereKey("username", equalTo: PFUser.currentUser()!.username!)
        queryHighScore0.whereKey("gameLength", equalTo: gameLengthArray[0])

        //get current user's high score for specified gameLength to find their ranking
        queryHighScore0.getFirstObjectInBackgroundWithBlock({ (object: AnyObject?, error: NSError?) -> Void in
            
            if error == nil
            {
                if let object = object as? PFObject
                {
                    
                    var currentHighScore = object.valueForKey("score") as! Int
                    var scale = object.valueForKey("scale") as! String
                    var masteryLevel = object.valueForKey("masteryLevel") as! String
                    
                    //finds number of users with higher score and uses it to find ranking
                    var queryRanking = PFQuery(className: "HighScore")
                    queryRanking.whereKey("gameLength", equalTo: gameLengthArray[0])
                    queryRanking.whereKey("score", greaterThan: currentHighScore)
                    
                    //var myRanking:Int32 = 0
                    
                    queryRanking.countObjectsInBackgroundWithBlock(
                    { (numberOfHigherScoreUsers: Int32, error: NSError?) -> Void in
                        
                        myRanking = numberOfHigherScoreUsers + 1
                        
                        
                        userScoreArrays[0].insert("My Rank: \(myRanking). \(currentHighScore) \(scale)", atIndex: 0)
                        
                        self.masteryLevelArrays[0].insert(masteryLevel, atIndex: 0)
                        
                        //println(userScoreArrays[0])
                        
                        //creating the different sections using fetched data
                        self.highScores.addSection("\(gameLengthArray[0])-Minute Game", scoresSection: userScoreArrays[0])
                            
                        self.tableView.reloadData()
                    })
                    
                }
            }
            else
            {
                // Log details of the failure
                //println("Error: \(error!) \(error!.userInfo!)")
            }
            
        })
        

        //finds all user high scores and puts them in rank order for specified gameLength
        var query0 = PFQuery(className: "HighScore")
        query0.whereKey("gameLength", equalTo: gameLengthArray[0])
        query0.orderByDescending("score")
        query0.findObjectsInBackgroundWithBlock({
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                //println("Successfully retrieved \(objects!.count) scores.")
                
                if let objects = objects as? [PFObject]
                {
                    var j = 1
                    
                    for object in objects
                    {
                        
                        var username = object.valueForKey("username") as! String
                        
                        var gameLength:Int = object.valueForKey("gameLength") as! Int
                        
                        var score:Int! = object.valueForKey("score") as! Int
                        
                        var scale = object.valueForKey("scale") as! String
                        
                        userScoreArrays[0].append("\(j). \(username): \(score) \(scale)")
                        
                        var masteryLevel = object.valueForKey("masteryLevel") as! String
                        
                        self.masteryLevelArrays[0].append("\(masteryLevel)")
                        
                        j++
                        
                    }
                    
                }
                
                self.tableView.reloadData()
                
            }
            else
            {
                //println("Error: \(error!) \(error!.userInfo!)")
            }
        })

        dispatch_after(delayTime1, dispatch_get_main_queue()){
            
            var queryHighScore1 = PFQuery(className: "HighScore")
            queryHighScore1.whereKey("username", equalTo: PFUser.currentUser()!.username!)
            queryHighScore1.whereKey("gameLength", equalTo: gameLengthArray[1])
            
            //get the current high score
            queryHighScore1.getFirstObjectInBackgroundWithBlock({ (object: AnyObject?, error: NSError?) -> Void in
                
                if error == nil
                {
                    if let object = object as? PFObject
                    {
                        
                        var currentHighScore = object.valueForKey("score") as! Int
                        var scale = object.valueForKey("scale") as! String
                        var masteryLevel = object.valueForKey("masteryLevel") as! String
                        
                        var queryRanking = PFQuery(className: "HighScore")
                        queryRanking.whereKey("gameLength", equalTo: gameLengthArray[1])
                        queryRanking.whereKey("score", greaterThan: currentHighScore)
                        
                        //var myRanking:Int32 = 0
                        
                        queryRanking.countObjectsInBackgroundWithBlock({ (numberOfHigherScoreUsers: Int32, error: NSError?) -> Void in
                                
                            myRanking = numberOfHigherScoreUsers + 1
                            
                            
                            userScoreArrays[1].insert("My Rank: \(myRanking). \(currentHighScore) \(scale)", atIndex: 0)
                            
                            self.masteryLevelArrays[1].insert(masteryLevel, atIndex: 0)
                            
                            //creating the different sections using fetched data
                            self.highScores.addSection("\(gameLengthArray[1])-Minute Game", scoresSection: userScoreArrays[1])
                            
                            self.tableView.reloadData()
                        })
                        
                    }
                }
                else
                {
                    // Log details of the failure
                    //println("Error: \(error!) \(error!.userInfo!)")
                }
                
            })
            
            var query1 = PFQuery(className: "HighScore")
            query1.whereKey("gameLength", equalTo: gameLengthArray[1])
            query1.orderByDescending("score")
            query1.findObjectsInBackgroundWithBlock({
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil
                {
                    
                    if let objects = objects as? [PFObject]
                    {
                        var j = 1
                        
                        for object in objects
                        {
                            
                            var username = object.valueForKey("username") as! String
                            
                            var gameLength:Int = object.valueForKey("gameLength") as! Int
                            
                            var score:Int! = object.valueForKey("score") as! Int
                            
                            var scale = object.valueForKey("scale") as! String
                            
                            userScoreArrays[1].append("\(j). \(username): \(score) \(scale)")
                            
                            var masteryLevel = object.valueForKey("masteryLevel") as! String
                            
                            self.masteryLevelArrays[1].append("\(masteryLevel)")
                            
                            j++
                            
                        }
                        
                    }
                    
                    self.tableView.reloadData()
                }
                else
                {
                    //println("Error: \(error!) \(error!.userInfo!)")
                }
            })
            
        }
      
        dispatch_after(delayTime2, dispatch_get_main_queue()){
            
            var queryHighScore2 = PFQuery(className: "HighScore")
            queryHighScore2.whereKey("username", equalTo: PFUser.currentUser()!.username!)
            queryHighScore2.whereKey("gameLength", equalTo: gameLengthArray[2])
            
            //get the current high score
            queryHighScore2.getFirstObjectInBackgroundWithBlock({ (object: AnyObject?, error: NSError?) -> Void in
                
                if error == nil
                {
                    if let object = object as? PFObject
                    {
                        
                        var currentHighScore = object.valueForKey("score") as! Int
                        var scale = object.valueForKey("scale") as! String
                        var masteryLevel = object.valueForKey("masteryLevel") as! String
                        
                        var queryRanking = PFQuery(className: "HighScore")
                        queryRanking.whereKey("gameLength", equalTo: gameLengthArray[2])
                        queryRanking.whereKey("score", greaterThan: currentHighScore)
                        
                        //var myRanking:Int32 = 0
                        
                        queryRanking.countObjectsInBackgroundWithBlock(
                        { (numberOfHigherScoreUsers: Int32, error: NSError?) -> Void in
                            
                            myRanking = numberOfHigherScoreUsers + 1
                            
                            
                            userScoreArrays[2].insert("My Rank: \(myRanking). \(currentHighScore) \(scale)", atIndex: 0)
                            
                            self.masteryLevelArrays[2].insert(masteryLevel, atIndex: 0)
                            
                            //creating the different sections using fetched data
                            self.highScores.addSection("\(gameLengthArray[2])-Minute Game", scoresSection: userScoreArrays[2])
                            
                            self.tableView.reloadData()
                        })
                        
                    }
                }
                else
                {
                    // Log details of the failure
                    //println("Error: \(error!) \(error!.userInfo!)")
                }
                
            })

            var query2 = PFQuery(className: "HighScore")
            query2.whereKey("gameLength", equalTo: gameLengthArray[2])
            query2.orderByDescending("score")
            query2.findObjectsInBackgroundWithBlock(
            {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil
                {
                    
                    if let objects = objects as? [PFObject]
                    {
                        var j = 1
                        
                        for object in objects
                        {
                            
                            var username = object.valueForKey("username") as! String
                            
                            var gameLength:Int = object.valueForKey("gameLength") as! Int
                            
                            var score:Int! = object.valueForKey("score") as! Int
                            
                            var scale = object.valueForKey("scale") as! String
                            
                            userScoreArrays[2].append("\(j). \(username): \(score) \(scale)")
                            
                            var masteryLevel = object.valueForKey("masteryLevel") as! String
                            
                            self.masteryLevelArrays[2].append("\(masteryLevel)")
                            
                            j++
                            
                        }
                        
                    }
                    
                    self.tableView.reloadData()
                }
                else
                {
                    //println("Error: \(error!) \(error!.userInfo!)")
                }
            })
            
        }
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        // Return the number of sections.
        return highScores.sections.count
    }
    
    //We count the array of items for the current section
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        // Return the number of rows in the section.
        return highScores.scoresSections[section].count
    }
    
    override func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        //instantiate a cell
        let cell = tableView.dequeueReusableCellWithIdentifier("cell",
            forIndexPath: indexPath) //as! UITableViewCell
        
        cell.textLabel?.text = highScores.scoresSections[indexPath.section][indexPath.row]
        
        cell.detailTextLabel!.text = masteryLevelArrays[indexPath.section][indexPath.row]
        
        return cell
    }
    
    //returns a string which is a title for a header row
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return highScores.sections[section]
    }

}
