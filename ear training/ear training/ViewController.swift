//
//  ViewController.swift
//  ear training
//
//  Created by Marcus Choi on 5/24/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import AVFoundation
import Parse
import iAd


class ViewController: UIViewController,OptionsViewControllerDelegate, UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate
{
    var error: NSError? = nil
    @IBOutlet var solfegeImage: UIImageView!
    
    @IBOutlet var solfegeText: UITextView!
    
    
    var userAnswer = ""
    var userAnswerArray = [String]()
    var userGuesses = 0
    @IBOutlet weak var userGuessesText: UILabel!
    
    
    //number of notes that user must guess for each problem
    var numberOfNotesPlayed = 1
    //total correct user guessed notes, updates after each correct answer
    var totalNumberOfNotesPlayed = 0
    
    //time that passes between notes played in seconds
    var timeBetweenNotes: NSTimeInterval = 1
    
    //plays chord then melody with delay of timeBetweenNotes
    var delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
        Int64(1 * Double(NSEC_PER_SEC)))
    
    @IBOutlet var scoreLabel: UILabel!
    var points = 0
    var problems = 0
    var finalScore = 0.00
    
    //for "Major and minor" scale selection
    var randomMajorminor:UInt32!
    
    //timer stuff
    var timer = NSTimer()
    //hundredths of a second
    var hseconds = 0.00
    var minutes = 0
    @IBOutlet var timeLabel: UILabel!
    
    
    //set the play/pause button toggle
    @IBOutlet var navbar: UINavigationItem!
    
    //initially game is activated so playing and paused are both false
    var isPlaying = false
    var isPaused = false
    
    //tells how many times play/pause, marks game entry
    var counter = 0
    
    @IBOutlet var playPauseOutlet: UIBarButtonItem!
    @IBAction func playPauseToggle(sender: AnyObject)
    {
        counter++
        
        var toggleButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Play, target: self, action: "playPauseToggle:")
        
        //pausing the game
        if isPlaying
        {
            timer.invalidate()
            isPlaying = false
            isPaused = true
            
            generateNewMelodyOutlet.enabled = false
            
        }
        else    //un-pausing creates new melody. If first play, reset points and probs
        {
            //game entry. counter is reset at end of game
            if counter == 1
            {
                resetGameProperties()
            }
            
            gameTime(self)
            
            toggleButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Pause, target: self, action: "playPauseToggle:")
            isPlaying = true
            isPaused = false
        }
        navbar.rightBarButtonItem = toggleButton
        
        print(counter)
    }

    //GAME TIME!!!
    var gameLength = 1
    @IBAction func gameTime(sender: AnyObject)
    {
       
        //moveableRootOutlet.enabled = false
        scaleType.userInteractionEnabled = false
        scaleChosen.userInteractionEnabled = false
        
        
        playMelodyOutlet.enabled = true
        playChordOutlet.enabled = true
        
        //resetGuessesOutlet.enabled = true
        
        generateNewMelody(self)
        generateNewMelodyOutlet.enabled = true
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("result"), userInfo: nil, repeats: true)

    }
    var scoreAlertTitle = String()
    func result()
    {
        var masteryLevels = ["New Beginner","Beginner1","Beginner2","Intermediate1","Intermediate2","Advanced1","Advanced2","Master1","Grand Master"]
        
        hseconds++
        
        //hseconds/100 is 1 second
        if hseconds/100 == 60.00
        {
            minutes++
            hseconds = 0.00
        }
        //proper display of hundredths of a second
        else if hseconds/100 < 10.00
        {
            timeLabel.text = "\(minutes):0\(hseconds/100)"
        }
        else
        {
            timeLabel.text = "\(minutes):\(hseconds/100)"
        }
        
        if minutes == gameLength //hseconds/100 == 5
        {

            timer.invalidate()
            
            timeLabel.text = "\(minutes):00.00"
           
            message.textColor = UIColor.redColor()
            message.text = "Game Over"
            
            
            //if user did not finish a problem
            if (problems - 1) == 0
            {
                finalScore = 0.0
            }
            else
            {
                
                //total number of notes accumulated for each problem * accuracy * 100
                finalScore = Double(totalNumberOfNotesPlayed)*Double(abs(points))/Double(problems - 1) //problems-1 because last problem in game unfinished
                
                //score multipliers
                if movingTonic == true
                {
                    finalScore *= 1.5
                }
                
                if scale == "random" || scale == "chromatic" || scale.rangeOfString("bebop") != nil
                {
                    finalScore *= 1.5
                }
                else if scale == "Major_and_minor"
                {
                    finalScore *= 1.25
                }
                
                finalScore = round(100*finalScore)
            }
            
            //each mastery level represents a score of +1000 pts
            var masteryLevel = masteryLevels[Int(trunc(finalScore / (Double(gameLength) * 1000)))]
            
            scoreAlertTitle = "Final Score: \(finalScore). \(masteryLevel)"
            
            var currentHighScore:Int!
            
            //only save score to database if it is above 0
            if finalScore > 0.0
            {
                var gameScore = PFObject(className: "GameScore")
                gameScore["score"] = finalScore
                gameScore["scale"] = highlightedPickerScale
                if movingTonic == true
                {
                    gameScore["moveableDo"] = 1
                }
                else
                {
                    gameScore["moveableDo"] = 0
                }
                gameScore["username"] = PFUser.currentUser()!.username
                gameScore["gameLength"] = gameLength
                gameScore["numberOfNotes"] = numberOfNotesPlayed
  
                //save the score
                gameScore.saveInBackgroundWithBlock
                {(success: Bool, error: NSError?) -> Void in
                    if (success)
                    {print("\(gameScore.objectId)")}
                    else
                    {print("failed")}
                }
                
                //if scale chosen is not limited to syllables, query for new high score
                if limitToSyllables == false
                {
                    
                    //finding current high score
                    var queryHighScore = PFQuery(className: "HighScore")
                    queryHighScore.whereKey("username", equalTo: PFUser.currentUser()!.username!)
                    queryHighScore.whereKey("gameLength", equalTo: gameLength)
                    
                    //get the current high score and replace if new score is higher
                    //queryHighScore.orderByDescending("score")
                    queryHighScore.getFirstObjectInBackgroundWithBlock({ (object: AnyObject?, error: NSError?) -> Void in
                        
                        if error == nil
                        {
                            if let object = object as? PFObject
                            {
                                
                                currentHighScore = object.valueForKey("score") as! Int
                                
                                //doesn't save if no new high score
                                if Int(self.finalScore) > currentHighScore
                                {
                                    
                                    //THIS DOESN'T SAVE BECAUSE IN BLOCK??
                                    self.scoreAlertTitle = "New High Score: \(self.finalScore)!"
                                    
                                    print("new high score!")
                                    
                                    object["score"] = self.finalScore
                                    
                                    object["scale"] = self.highlightedPickerScale
                                    
                                    object["masteryLevel"] = masteryLevel
                                    
                                    //save the high score object
                                    object.saveInBackgroundWithBlock
                                        {(success: Bool, error: NSError?) -> Void in
                                            if (success)
                                            {print("\(object.objectId)")}
                                            else
                                            {print("failed")}
                                    }
                                    
                                }
                            }
                        }
                        else
                        {
                            // Log details of the failure
                            //print("Error: \(error!) \(error!.userInfo!)")
                        }
                        
                    })
                }
            }
            
            //show the final score and ask to play again
            var alert = UIAlertController(title: scoreAlertTitle, message: "Your score is the total number of correct tones * %accuracy (* difficulty multipliers). Play Again?", preferredStyle: UIAlertControllerStyle.Alert)
/*
            if Int(finalScore) > currentHighScore
            {
                //alert that it is a new high score
                alert = UIAlertController(title: "New High Score: \(self.finalScore)!", message: "Play Again?", preferredStyle: UIAlertControllerStyle.Alert)
            
            }
*/
            alert.addAction(UIAlertAction(title: "Yes!", style: .Default, handler: { (action) -> Void in
                
                self.resetGameProperties()
                self.gameTime(self)
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action) -> Void in
                
                self.resetGameProperties()
                
                //the last unanswered problem from the game
                self.problems = 1
                
                //self.moveableRootOutlet.enabled = true
                self.scaleType.userInteractionEnabled = true
                self.scaleChosen.userInteractionEnabled = true
                
                self.playPauseToggle(self)
                self.isPaused = false    //because play pause toggle is called, make sure this is false
                self.counter = 0
                
                self.generateNewMelodyOutlet.enabled = true
                
                for solfegeOutlet in self.solfegeCollection
                {
                    solfegeOutlet.enabled = true
                }
                
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
            
            points = 0
            problems = 0
            minutes = 0
        }
    }
    func resetGameProperties()
    {
        //reset minutes and seconds
        minutes = 0
        hseconds = 0.0
        
        scoreLabel.text = "0/0"
        points = 0
        problems = 0
        totalNumberOfNotesPlayed = 0
        
        userGuesses = 0
        userAnswer = ""
        userAnswerArray.removeAll(keepCapacity: false)
        userGuessesText.text = "Correct: \(userGuesses) Tones"
    }
    
    @IBOutlet var resetGuessesOutlet: UIButton!
    @IBAction func resetGuesses(sender: AnyObject)
    {
        if userGuesses != 0
        {
            //if points > 0
            //{
            //    points--
            //}
            problems++
            
            userGuesses = 0
            userAnswer = ""
            userAnswerArray.removeAll(keepCapacity: false)
            
            userGuessesText.text = "Correct: \(userGuesses) Tones"
            scoreLabel.text = "\(points)/\(problems)"
        }
    
    }

    var previousUserNote = String?()
    
    //very first guess of entire instance of using the app
    var firstGuessHasHappened = false
    
    let darkGreen = UIColor(hue: 0.3, saturation: 1.0, brightness: 0.5, alpha: 1.0)

    var buttonPressDelayTime: dispatch_time_t!
    
    var playerSolfege: AVAudioPlayer = AVAudioPlayer()
    @IBAction func solfegeButtons(sender: AnyObject)
    {
        buttonPressDelayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        
        sender.layer.shadowOffset = CGSizeMake(0, 0)
        sender.layer.shadowOpacity = 0.5
        
        dispatch_after(buttonPressDelayTime, dispatch_get_main_queue()){
            
            sender.layer.shadowColor = UIColor.blackColor().CGColor
            sender.layer.shadowOffset = CGSizeMake(2, 2)
            sender.layer.shadowOpacity = 1
            
        }
        
        //clear the message whenever a solfege button is pressed
        if answerIsShown == false // && highlightedPickerScale != "chosen_tones"
        {
            message.text = ""
        }
        
        var restID = sender.restorationIdentifier!!
        
        if scaleChosen.editing == true
        {
            scaleChosen.text = scaleChosen.text! + restID + " "
        }
        
        var scaleDegree = String()
        
        //identify image based on restoration identifier property of button pressed
        solfegeImage.image = UIImage(named: restID + ".jpg")
        
        if restID == "do"
        {
            scaleDegree = "1"
        }
        else if restID == "ra"
        {
            scaleDegree = "b2"
        }
        else if restID == "re"
        {
            scaleDegree = "2"
        }
        else if restID == "me"
        {
            scaleDegree = "b3"
        }
        else if restID == "mi"
        {
            scaleDegree = "3"
        }
        else if restID == "fa"
        {
            scaleDegree = "4"
        }
        else if restID == "fi"
        {
            scaleDegree = "#4"
        }
        else if restID == "sol"
        {
            scaleDegree = "5"
        }
        else if restID == "le"
        {
            scaleDegree = "b6"
        }
        else if restID == "la"
        {
            scaleDegree = "6"
        }
        else if restID == "te"
        {
            scaleDegree = "b7"
        }
        else //if restID == "ti"
        {
            scaleDegree = "7"
        }
        
        solfegeText.text = "\(restID)(\(scaleDegree))"
        
        //show number of notes user has played if melody contains more than one note
        if numberOfNotesStepper.value > 1
        {
            userGuessesText.hidden = false
        }
        
        //file location of first (possibly wrong) guess
        var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(sender.restorationIdentifier!!)1"]!, ofType: "mp3")
        
        //FOR HARMONIC QUESTIONS, LOOP THROUGH ANSWERARRAY TO SEE IF CURRENT USERANSWERARRAY VALUE MATCHES ANY ANSWERARRAY SYLLABLE (WITHOUT END NUMBER). IF IT DOES, PLAY THAT ANSWERARRAY SYLLABLE AND ELIMINATE IT FROM ANSWERARRAY. ONCE ANSWERARRAY IS EMPTY, CORRECT ANSWER. IF SYLLABLE PLAYED NOT IN ARRAY, RESET GUESSES
        if harmonicQuestion == true
        {
            
            if answerIsShown == false && answer != "" //&& highlightedPickerScale != "chosen_tones"
            {
                userGuesses++
                
                var newAnswerArrayCount = answerArray.count
                for var i = 0; i < newAnswerArrayCount; i++
                {
                    //check if button pressed is in the answer array
                    if answerArray[i].rangeOfString(sender.restorationIdentifier!!) != nil
                    {
                        //assign the file location to be played as the tone correctly guessed
                        fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary[answerArray[i]], ofType: "mp3")
                        
                        //remove the array value of the correctly guessed tone from answer array so that it can't be correctly replayed
                        answerArray.removeAtIndex(i)
                        
                        if userGuesses == 1
                        {
                            userGuessesText.text = "Correct: \(userGuesses) Tone"
                        }
                        else
                        {
                            userGuessesText.text = "Correct: \(userGuesses) Tones"
                        }
                        
                        print(answerArray)
                        
                        break
                    }
                }
                
                //answer array length should be less than previous answer array length if last user guess was correct
                if answerArray.count == newAnswerArrayCount
                {
                    message.textColor = UIColor.redColor()
                    message.text = "Try Again"
                    
                    //this imcrements problems
                    resetGuesses(self)
                    
                    scoreLabel.text = "\(points)/\(problems)"
                    
                    answerArray = savedAnswerArray
                }
                //answerarray is empty when all correct tones are played
                else if answerArray.count == 0
                {
                    userGuesses = 0
                    
                    delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
                        Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
                    
                    message.textColor = darkGreen
                    message.text = "Correct!"
                    
                    userGuessesText.text = "Correct: \(numberOfNotesPlayed) Tones"
                    
                    for solfegeOutlet in solfegeCollection
                    {
                        solfegeOutlet.enabled = false
                    }
                    
                    //gives points if game is not paused and in non-game mode
                    if isPaused == false
                    {
                        points += 1
                        totalNumberOfNotesPlayed += numberOfNotesPlayed
                        scoreLabel.text = "\(points)/\(problems)"
                        
                        //generates new melody with delay
                        dispatch_after(delayTime1, dispatch_get_main_queue()){
                            //self.userGuesses = 0
                            self.generateNewMelody(self)
                            self.userGuessesText.text = "Correct: \(self.userGuesses) Tones"
                            
                            for solfegeOutlet in self.solfegeCollection
                            {
                                solfegeOutlet.enabled = true
                            }
                        }
                    }
                    
                    
                }
            
            }
            print(problems)
        }
        else //for all melodies (harmonicQuestion is false)
        {
            //add to userAnswer string, userAnswerArray, increment userGuesses as long as answer is not empty (ie there was a question asked)
            if answerIsShown == false && answer != "" && scaleChosen.editing != true
            {
                message.text = ""
                userAnswer += sender.restorationIdentifier!! + " "
                
                //has syllables without ending number
                userAnswerArray.append(sender.restorationIdentifier!!)
                
                //assigning the correct file for the solfege button pressed
                //if value from useranswerarray == value from answer (minus the last character), play the exact syllable (same octave) as answer
                if answerArrayMelodic[userGuesses].rangeOfString(userAnswerArray[userGuesses]) != nil // == answerArrayMelodic[userGuesses].substringToIndex(answerArrayMelodic[userGuesses].endIndex.predecessor())//solfegeArray[solfegeIndices[userGuesses]].substringToIndex(solfegeArray[solfegeIndices[userGuesses]].endIndex.predecessor())
                {
                    //the solfege syllable of the current correctly identified note
                    //var solfegeKeysOfCurrentCorrectAnswer = (solfegeWavDictionary as NSDictionary).allKeysForObject(answerArrayMelodic[userGuesses])
                    //var solfegeKeyOfCurrentCorrectAnswer = solfegeKeysOfCurrentCorrectAnswer[0] as! String
                    
                    fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary[answerArrayMelodic[userGuesses]], ofType: "mp3")
                }
                else //if user answer is wrong, play nearest note for sender's syllable
                {
                    //userAnswerArray.removeAll(keepCapacity: true)
                    
                    if firstGuessHasHappened
                    {
                        //previous user note must exist in current pianowavarray (ie pitch range was not changed)
                        if pianoWavArray.indexOf(previousUserNote!) != nil
                        {
                            //get index of previously played note, subtract index of syllable with end numbers, play the one that is smaller difference
                            //if abs(find(pianoWavArray, previousUserNote!)! - find(pianoWavArray, solfegeWavDictionary["\(userAnswerArray[userGuesses])1"]!)!) > abs(find(pianoWavArray, previousUserNote!)! - find(pianoWavArray, solfegeWavDictionary["\(userAnswerArray[userGuesses])2"]!)!)
                            
                            if abs(pianoWavArray.indexOf(previousUserNote!)! - pianoWavArray.indexOf(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"]!)!) > abs(pianoWavArray.indexOf(previousUserNote!)! - pianoWavArray.indexOf(solfegeWavDictionary["\(userAnswerArray[userGuesses])2"]!)!)
                            {
                                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])2"], ofType: "mp3")
                                
                            }
                            else //if previous user note is not found in pianowavarray, user changed the pitch range
                            {
                                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"], ofType: "mp3")
                            }
                            
                        }
                        else
                        {
                            fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"], ofType: "mp3")
                        }
                    }
                }
                
                //after this is true, can start ensuring notes of guesses are close together
                firstGuessHasHappened = true
                
                //remove extension from filename (last 4 characters) gives just the note
                previousUserNote = String(NSURL(fileURLWithPath: fileLocationSolfege!).URLByDeletingPathExtension)
                
                userGuesses += 1
            }
            
            //resets guesses once a wrong tone is played (before number of tones in melody are played). Ensures there is an answer before checking. Answer is recorded as a string.
            if answer.characters.count > userAnswer.characters.count
            {
                //check if userAnswer string is equal to answer string so far. If not, reset guesses
                if userAnswer != answer.substringToIndex(userAnswer.endIndex) && answer != ""
                {
                    resetGuesses(self)
                    
                    message.textColor = UIColor.redColor()
                    message.text = "Try Again"
                    
                }
            }
            
            //the last note guess
            //TAKE THIS OUT OF ELSE FOR HARMONIC QUESTIONS?
            if userGuesses == numberOfNotesPlayed //|| harmonicQuestion == true
            {
                userGuesses = 0
                
                delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
                    Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
                
                if userAnswer == answer //|| answerArray.count == 0
                {
                    message.textColor = darkGreen
                    
                    message.text = "Correct!"
                    
                    for solfegeOutlet in solfegeCollection
                    {
                        solfegeOutlet.enabled = false
                    }
                    
                    userGuessesText.text = "Correct: \(numberOfNotesPlayed) Tones"
                    
                    //gives points if game is not paused and in non-game mode
                    if isPaused == false
                    {
                        points += 1
                        
                        //reset answer so user can't repeatedly press and get pts
                        answer = ""
                        
                        totalNumberOfNotesPlayed += numberOfNotesPlayed
                        
                        //generates new melody with delay
                        dispatch_after(delayTime1, dispatch_get_main_queue()){
                            //self.userGuesses = 0
                            self.generateNewMelody(self)
                            self.userGuessesText.text = "Correct: \(self.userGuesses) Tones"
                            
                            for solfegeOutlet in self.solfegeCollection
                            {
                                solfegeOutlet.enabled = true
                            }
                        }
                    }
       
                }
                    //If last note played in melody length is wrong. Does not apply to harmonic questions
                else //if userAnswer != answer && harmonicQuestion == false
                {
                    message.textColor = UIColor.redColor()
                    message.text = "Try Again"
                    
                    dispatch_after(delayTime1, dispatch_get_main_queue()){
                        self.message.text = ""
                        self.userGuessesText.text = "Correct: \(self.userGuesses) Tones"
                    }
                    
                    problems++
                }
                scoreLabel.text = "\(points)/\(problems)"
                
                userAnswer = ""
                userAnswerArray.removeAll(keepCapacity: false)
            }
            
            //if answer incorrect/incomplete, displays number of guesses
            if userAnswer != answer
            {
                if userGuesses == 1
                {
                    userGuessesText.text = "Correct: \(userGuesses) Tone"
                }
                else
                {
                    userGuessesText.text = "Correct: \(userGuesses) Tones"
                }
            }
        }
        
        //play the tone from button press
        //playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!), error: &error)
        //CHANGED FOR SWIFT2 ERROR HANDLING
        
        //init(){   do I need this???
        do
        {
            try playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!))
            
        } catch
        {
            //handle error
        }
        //} end of init()
        
        playerSolfege.play()
            
    }//end of func solfegeButtons
    
    var answerIsShown = false
    @IBOutlet var showAnswerOutlet: UIButton!
    @IBAction func showAnswer(sender: AnyObject)
    {
        //make syllables the same octave if answer is played?
        
        //play the melody when showAnswer pressed.
        if harmonicQuestion == true
        {
            //this makes the notes sound melodically instead of harmonically
            harmonicQuestion = false
            playMelody(self)
            harmonicQuestion = true
        }
        else
        {
            playMelody(self)
        }
            
        message.textColor = UIColor.blackColor()
        
        if numberSystem == true
        {
            //turn answer into numbers
            var solfegeNumberDictionary = ["do":"1","ra":"b2","re":"2","me":"b3","mi":"3","fa":"4","fi":"#4","sol":"5","le":"b6","la":"6","te":"b7","ti":"7"]
            
            //CHANGED FOR SWIFT2
            let answerNumSysArray = answer.characters.split{$0 == " "}.map(String.init)
            
            var answerNumSys = ""
            
            for answerNum in answerNumSysArray
            {
                answerNumSys += solfegeNumberDictionary[answerNum]! + " "
            }
            
            message.text = answerNumSys
        }
        else
        {
            message.text = answer
        }
        
        answerIsShown = true

    }

    var player = [AVAudioPlayer]()
    
    var chord1: AVAudioPlayer = AVAudioPlayer()
    var chordm3: AVAudioPlayer = AVAudioPlayer()
    var chord3: AVAudioPlayer = AVAudioPlayer()
    var chord5: AVAudioPlayer = AVAudioPlayer()
    
    var pianoWavArray = ["C3", "Db3", "D3", "Eb3", "E3", "F3", "Gb3", "G3", "Ab3", "A3", "Bb3", "B3", "C4", "Db4", "D4", "Eb4", "E4", "F4", "Gb4", "G4", "Ab4", "A4", "Bb4", "B4"]
    
    var solfegeArray = ["do1","ra1","re1","me1","mi1","fa1","fi1","sol1","le1","la1","te1","ti1","do2","ra2","re2","me2","mi2","fa2","fi2","sol2","le2","la2","te2","ti2"]

    var rootIndex = 0//Int(arc4random_uniform(UInt32(12)))
    
    //the end number of first octave in range
    var pitchRange = 3

    //dictionary do=Ab.wav, ra=A.wav etc
    var solfegeWavDictionary = [String: String]()
    
    var solfegeIndices = [Int]()

    //each notesPlayed index contains a wav filename, ie "Bb", for the melody
    var notesPlayed = [String]()
    
    var answer: String = ""
    
    //these are used for harmonic questions
    var answerArray = [String]()
    var savedAnswerArray = [String]()
    
    var answerArrayMelodic = [String]()
    
    //fixed or moving tonic
    var movingTonic = false
/*
    @IBOutlet var moveableRootOutlet: UISegmentedControl!
    @IBAction func moveableRoot(sender: AnyObject)
    {
        if(moveableRootOutlet.selectedSegmentIndex == 0)
        {
            movingTonic = false;
        }
        else if(moveableRootOutlet.selectedSegmentIndex == 1)
        {
            movingTonic = true;
        }
    }
*/
    //shuffle the values in the ordered array of unique notes to get random order of unique notes
//    func shuffle<C: MutableCollectionType where C.Index == Int>(var list: C) -> C {
//        let countList = list.count
//        for i in 0..<(countList - 1) {
//            let j = Int(arc4random_uniform(UInt32(countList - i))) + i
//            swap(&list[i], &list[j])
//        }
//        return list
//    }
    
    func shuffle(uniqueNumberArray: [Int]) -> [Int]
    {
        var randomNumber = Int()
        var shuffledNumberArray = [Int]()
        
        for var i = 0; i < uniqueNumberArray.count; i++
        {
            randomNumber = Int(arc4random_uniform(UInt32(uniqueNumberArray.count)))
            
            while shuffledNumberArray.contains(randomNumber)
            {
                randomNumber = Int(arc4random_uniform(UInt32(uniqueNumberArray.count)))
            }
        
            shuffledNumberArray.append(randomNumber)
        }
        
        return shuffledNumberArray
    }
    
    func populateSolfegeWavDictionary()
    {
        var i:Int
        for i = rootIndex; i > 0; i--
        {
            //ie, do = "Bb"
            solfegeWavDictionary[solfegeArray[12-i]] = pianoWavArray[rootIndex-i];
        }
        
        //12 is index of beginning of 2nd octave
        for i = rootIndex; i < 12; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
        for i = 12; i < 12 + rootIndex; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex + 12]] = pianoWavArray[i];
        }
        
        for i = 12 + rootIndex; i < solfegeArray.count; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
    }

    //the scale chosen variable
    var scale = "Major"//scaleChosen.text.lowercaseString
    
    var lastNoteOfPreviousMelody = ""//String()
    var firstNoteOfCurrentMelody = String()
    var limitToSyllables = Bool()
    @IBOutlet var generateNewMelodyOutlet: UIButton!
    @IBAction func generateNewMelody(sender: AnyObject)
    {
        buttonPressDelayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        
        generateNewMelodyOutlet.layer.shadowOffset = CGSizeMake(0, 0)
        generateNewMelodyOutlet.layer.shadowOpacity = 0.5
        
        dispatch_after(buttonPressDelayTime, dispatch_get_main_queue()){
            
            self.generateNewMelodyOutlet.layer.shadowColor = UIColor.blackColor().CGColor
            self.generateNewMelodyOutlet.layer.shadowOffset = CGSizeMake(2, 2)
            self.generateNewMelodyOutlet.layer.shadowOpacity = 1
            
        }
        
        if randomPitchRangeOption == true
        {
            pitchRange = Int(arc4random_uniform(5)) + 1
            
            var lowerC = "C" + "\(pitchRange)"
            var lowerDb = "Db" + "\(pitchRange)"
            var lowerD = "D" + "\(pitchRange)"
            var lowerEb = "Eb" + "\(pitchRange)"
            var lowerE = "E" + "\(pitchRange)"
            var lowerF = "F" + "\(pitchRange)"
            var lowerGb = "Gb" + "\(pitchRange)"
            var lowerG = "G" + "\(pitchRange)"
            var lowerAb = "Ab" + "\(pitchRange)"
            var lowerA = "A" + "\(pitchRange)"
            var lowerBb = "Bb" + "\(pitchRange)"
            var lowerB = "B" + "\(pitchRange)"
            
            var higherC = "C" + "\(pitchRange+1)"
            var higherDb = "Db" + "\(pitchRange+1)"
            var higherD = "D" + "\(pitchRange+1)"
            var higherEb = "Eb" + "\(pitchRange+1)"
            var higherE = "E" + "\(pitchRange+1)"
            var higherF = "F" + "\(pitchRange+1)"
            var higherGb = "Gb" + "\(pitchRange+1)"
            var higherG = "G" + "\(pitchRange+1)"
            var higherAb = "Ab" + "\(pitchRange+1)"
            var higherA = "A" + "\(pitchRange+1)"
            var higherBb = "Bb" + "\(pitchRange+1)"
            var higherB = "B" + "\(pitchRange+1)"
            
            pianoWavArray = [lowerC,lowerDb,lowerD,lowerEb,lowerE,lowerF,lowerGb,lowerG,lowerAb,lowerA,lowerBb,lowerB,higherC,higherDb,higherD,higherEb,higherE,higherF,higherGb,higherG,higherAb,higherA,higherBb,higherB]
        }
        
        
        scaleChosen.endEditing(true)
        
        answerArray.removeAll(keepCapacity: true)
        
        answerArrayMelodic.removeAll(keepCapacity: true)
        
        if numberOfNotesStepper.value == 1
        {
            userGuessesText.hidden = true
        }
        
        var random1to100:UInt32
        var percentageOfSmallerIntervals:UInt32 = 90
        
        //difference between indices
        var smallestLargeInterval = 6
        var largestInterval = 16
        
        var indexOfPreviousNote: Int
        //index of 2 notes before
        var indexOfPreviousNote2: Int
        var indexOfCurrentNote: Int
        var currentSyllable = String()
        var random0or1: UInt32
        
        //used to save the tonic while it temporarily changes for "outside_melodies" option
        var savedRootIndex = rootIndex
        
        //in half steps
        var maxIntervalBebop = 7
        var randomIntervalLessThanMaxBebop:Int
        
        resetGuesses(self)
        showAnswerOutlet.enabled = true
        
        for solfegeOutlet in solfegeCollection
        {
            solfegeOutlet.enabled = true
        }
        
        playMelodyOutlet.enabled = true
        playChordOutlet.enabled = true
        
        //resetGuessesOutlet.enabled = true
        
        answerIsShown = false
        
        //IMPLEMENT IN A DIFFERENT WAY: NUMBER OF NOTES TEXT AND RANDOM CASE
        //if number of notes text is an int and <= 100
        if Int(numberOfNotesText.text!) != nil && Int(numberOfNotesText.text!) <= 100
        {
            numberOfNotesPlayed = Int(numberOfNotesText.text!)!
        }
        //random option if "r" put behind number
//        else if numberOfNotesText.text.lowercaseString.rangeOfString("r") != nil
//        {
//            //if just "r", make random number of notes 8 or fewer
//            if numberOfNotesText.text.lowercaseString == "r"
//            {
//                numberOfNotesPlayed = Int(arc4random_uniform(UInt32(8))) + 1
//            }
//            else
//            {
//                for var k = 2; k <= 100; k++
//                {
//                    if numberOfNotesText.text.lowercaseString == "\(k)r" //|| numberOfNotesText.text == "\(k)R"
//                    {
//                        numberOfNotesPlayed = Int(arc4random_uniform(UInt32(k))) + 1
//                    }
//                }
//            
//            }
//            
//        }
        //don't increase number of problems if game is paused and new melody generated
        if isPaused == false
        {
            problems += 1
        }
        answer = ""
        
        //for "Major_and_minor"
        randomMajorminor = arc4random_uniform(2)
        
        if movingTonic == true
        {
            rootIndex = Int(arc4random_uniform(UInt32(12)))
            tonic = pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor())
            newKeyConfiguration()
            
        }
        
        populateSolfegeWavDictionary()
        
        solfegeIndices = [Int](count: numberOfNotesPlayed, repeatedValue: 0)
        notesPlayed = [String](count: numberOfNotesPlayed, repeatedValue: "")
        
        limitToSyllables = false
        var sylChosenArray = [String]()
        
        //SYLLABLES: SHOW THE TEXT FIELD IF SCALE == INPUT SYLLABLES
        //Check if user input syllables. Check for space and check if text does not match anything in scaleData array
        if highlightedPickerScale == "chosen_tones" //scaleChosen.text.rangeOfString(" ") != nil || !contains(scaleData, scaleChosen.text) && highlightedPickerScale == "chosen_tones"
        {
            
            //CHANGED FOR SWIFT2
            sylChosenArray = (scaleChosen.text?.characters.split{$0 == " "}.map(String.init))!
            
            //keeps array count from updating after every loop
            let sylChosenArrayCount = sylChosenArray.count
            
            //make the array of syllables in all octaves
            for var i = 0; i < sylChosenArrayCount; i++
            {
                //change syllable to match solfegeArray syllables if user inputs numbers or other syllable names
                if sylChosenArray[i] == "1"
                {
                    sylChosenArray[i] = "do"
                }
                else if sylChosenArray[i] == "#1" || sylChosenArray[i] == "b2" || sylChosenArray[i] == "di"
                {
                    sylChosenArray[i] = "ra"
                }
                else if sylChosenArray[i] == "2"
                {
                    sylChosenArray[i] = "re"
                }
                else if sylChosenArray[i] == "#2" || sylChosenArray[i] == "b3" || sylChosenArray[i] == "ri"
                {
                    sylChosenArray[i] = "me"
                }
                else if sylChosenArray[i] == "3"
                {
                    sylChosenArray[i] = "mi"
                }
                else if sylChosenArray[i] == "4"
                {
                    sylChosenArray[i] = "fa"
                }
                else if sylChosenArray[i] == "#4" || sylChosenArray[i] == "b5" || sylChosenArray[i] == "se"
                {
                    sylChosenArray[i] = "fi"
                }
                else if sylChosenArray[i] == "5" || sylChosenArray[i] == "so"
                {
                    sylChosenArray[i] = "sol"
                }
                else if sylChosenArray[i] == "#5" || sylChosenArray[i] == "b6" || sylChosenArray[i] == "si"
                {
                    sylChosenArray[i] = "le"
                }
                else if sylChosenArray[i] == "6"
                {
                    sylChosenArray[i] = "la"
                }
                else if sylChosenArray[i] == "#6" || sylChosenArray[i] == "b7" || sylChosenArray[i] == "li"
                {
                    sylChosenArray[i] = "te"
                }
                else if sylChosenArray[i] == "7"
                {
                    sylChosenArray[i] = "ti"
                }
                
                sylChosenArray.append(sylChosenArray[i] + "2")
                sylChosenArray[i] += "1"
            }
            
            print(sylChosenArray)
            
            for sylChosen in sylChosenArray
            {
                //if solfegeArray contains chosen syllable(s) (if what user typed are syllables), melody can be limited to syllables
                if solfegeArray.contains(sylChosen)
                {
                    limitToSyllables = true
                    
                }
            }
        }

        var unshuffledIndices = [Int]()
        for var b = 0; b < solfegeArray.count; b++
        {
            unshuffledIndices.append(b)
        }
        var shuffledIndices = shuffle(unshuffledIndices)
        
        var x = 0
        
        var currentSolfegeScale = [String]()
        
        //non-scale tones
        var passingTones: [String]!
        //resolution points for the end of bebop melodies
        var endingTones: [String]!
        
        if highlightedPickerScale == "random" || highlightedPickerScale == "outside_melodies"
        {
            var randomScales = [String]()
            
            var randomScaleIndex = Int()
            
            //scales for "random" option
            if highlightedPickerScale == "random"
            {
                randomScales = ["Major","minor(relative)","bebop_Major","bebop_minor","minor(blues)","minor(melodic)","minor(harmonic)"]
                
                randomScaleIndex = Int(arc4random_uniform(UInt32(randomScales.count)))
            }
            //outside melodies does not have bebop scales
            else //if highlightedPickerScale == "outside_melodies"
            {
                
                randomScales = ["Major","minor(relative)","minor(blues)","minor(melodic)","minor(harmonic)"]
                
                let coinflip = arc4random_uniform(2)
                
                if coinflip == 0
                {
                    randomScaleIndex = randomScales.indexOf("Major")!
                }
                else //if coinflip == 1
                {
                    randomScaleIndex = Int(arc4random_uniform(UInt32(randomScales.count)))
                    
                    while randomScales[randomScaleIndex] == "Major"
                    {
                        randomScaleIndex = Int(arc4random_uniform(UInt32(randomScales.count)))
                    }
                }
                
                rootIndex = Int(arc4random_uniform(UInt32(12)))
                
                print("temp root \(pianoWavArray[rootIndex])")
                
                populateSolfegeWavDictionary()
            }
            
            scale = randomScales[randomScaleIndex]
            print("scale \(scale)")
            
        }
        else if highlightedPickerScale == "Major_and_minor"
        {
            
            if randomMajorminor == 0
            {
                scale = "Major"
                
            }
            else if randomMajorminor == 1
            {
                scale = "minor(relative)"
                
            }
            
        }
        else
        {
            scale = highlightedPickerScale
        }
        
        
        if scale == "Major"
        {
            currentSolfegeScale = ["do","re","mi","fa","sol","la","ti"]
        }
        else if scale == "minor(relative)"
        {
            currentSolfegeScale = ["do","re","me","fa","sol","le","te"]
        }
        else if scale == "minor(harmonic)"
        {
            currentSolfegeScale = ["do","re","me","fa","sol","le","ti"]
        }
        else if scale == "minor(melodic)"
        {
            currentSolfegeScale = ["do","re","me","fa","sol","la","ti"]
        }
        else if scale == "minor(blues)"
        {
            currentSolfegeScale = ["do","me","fa","fi","sol","te"]
        }
        else if scale == "bebop_Major" || scale == "bebop_minor" || scale == "chromatic"
        {
            currentSolfegeScale = ["do","ra","re","me","mi","fa","fi","sol","le","la","te","ti"]
            
            if scale == "bebop_Major"
            {
                passingTones = ["ra","me","fi","le","te"]
                endingTones = ["do","re","mi","fi","sol","la","ti"]
            }
            else if scale == "bebop_minor"
            {
                passingTones = ["ra","mi","fi"]
                endingTones = ["do","re","me","fa","sol","le","la","te","ti"]
            }
        }
        
        
        //creating notesPlayed array
        for var j = 0; j < numberOfNotesPlayed; j++
        {
            random1to100 = arc4random_uniform(100) + 1
            
            //PUT THIS IN THE NON-HARMONIC SECTION?
            //if moving root and >1 melody note, last note of previous melody is first note of next melody
            if movingTonic == true && numberOfNotesPlayed > 1 && harmonicQuestion == false && j == 0 && highlightedPickerScale != "chosen_tones"
            {
                //first question will start with "do1"
                if lastNoteOfPreviousMelody == ""
                {
                    lastNoteOfPreviousMelody = solfegeWavDictionary["do1"]!
                }
                firstNoteOfCurrentMelody = lastNoteOfPreviousMelody
                
                var solfegeKeys = (solfegeWavDictionary as NSDictionary).allKeysForObject(firstNoteOfCurrentMelody)
                var solfegeKey = solfegeKeys[0] as! String
                
                while !currentSolfegeScale.contains(solfegeKey.substringToIndex(solfegeKey.endIndex.predecessor()))
                {
                    rootIndex = Int(arc4random_uniform(UInt32(12)))
                    tonic = pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor())
                    populateSolfegeWavDictionary()
                    
                    //needed everytime there is a new root index
                    newKeyConfiguration()
                    
                    solfegeKeys = (solfegeWavDictionary as NSDictionary).allKeysForObject(firstNoteOfCurrentMelody)
                    solfegeKey = solfegeKeys[0] as! String
                }
                
                solfegeIndices[j] = solfegeArray.indexOf(solfegeKey)!
               
            }
            else
            {
                //solfegeIndices is an array of solfegeArray indices
                solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
            }
                
            
            
//HARMONIC QUESTIONS-----------------------------------------------------------------------
            
            if harmonicQuestion == true
            {
                
                //harmonic chords: should only work for 3 or 4 notes
                if /*numberOfNotesPlayed == 3 || numberOfNotesPlayed == 4 &&*/ harmonicChordQuestion == true
                {
                    
                    //the root of the chord
                    if j == 0
                    {
                        solfegeIndices[j] = Int(arc4random_uniform(UInt32(currentSolfegeScale.count)))
                    }
                    else //other chordal tones are 2 indices away
                    {
                        //wrap the value around to beginning of solfege scale array if indices exceed its size
                        if solfegeIndices[j-1] + 2 >= currentSolfegeScale.count
                        {
                            solfegeIndices[j] = solfegeIndices[j-1] + 2 - currentSolfegeScale.count
                        }
                        else
                        {
                            solfegeIndices[j] = solfegeIndices[j-1] + 2
                        }
                    }
                    
                    //adding octave number to end of solfege indices
                    var numberOfOctaves = 2
                    
                    var endNumberSolfege = Int()
                    
                    //chords in root position
                    if harmonicChordRootPosQuestion == true
                    {
                        //root of chord is in first octave
                        if j == 0
                        {
                            endNumberSolfege = 1
                            
                        }
                        else    //other tones in root position chord need to be the closest note for given syllable
                        {
                            //calculate the absolute distances between solfege syllables in different octaves, use the closest one
                            let distBetweenRootAndSyl1 = abs(pianoWavArray.indexOf(solfegeWavDictionary[currentSolfegeScale[solfegeIndices[j]] + "1"]!)! - pianoWavArray.indexOf(solfegeWavDictionary[answerArray[j-1]]!)!)
                            
                            let distBetweenRootAndSyl2 = abs(pianoWavArray.indexOf(solfegeWavDictionary[currentSolfegeScale[solfegeIndices[j]] + "2"]!)! - pianoWavArray.indexOf(solfegeWavDictionary[answerArray[j-1]]!)!)
                            
                            if distBetweenRootAndSyl1 > distBetweenRootAndSyl2
                            {
                                endNumberSolfege = 2
                            }
                            else
                            {
                                endNumberSolfege = 1
                            }
                        }
                    }
                    else //chords not in root position (if harmonicChordRootPosQuestion == false)
                    {
                        endNumberSolfege = Int(arc4random_uniform(UInt32(numberOfOctaves))) + 1
                    }
                    
                    //this array only used in harmonic questions
                    answerArray.append(currentSolfegeScale[solfegeIndices[j]] + String(endNumberSolfege))
                    
                    //populating the array that contains the absolute names of the notes played (for harmonic questions only!!)
                    notesPlayed[j] = solfegeWavDictionary[answerArray[j]]!
                    
                    //to display if Show Answer pressed
                    answer += currentSolfegeScale[solfegeIndices[j]] + " "
                    
                }
                else //if harmonicChordQuestion == false (for harmonic questions with random notes)
                {
                    
                    //while the current solfege syllable of shuffledIndices is not in currentSolfegeScale, go to next index of shuffledIndices
                    while !currentSolfegeScale.contains(solfegeArray[shuffledIndices[x]].substringToIndex(solfegeArray[shuffledIndices[x]].endIndex.predecessor()))
                    {
                        //x represents the index of shuffledIndices that matches a syllable in the current scale
                        x++
                    }
                    
                    //assign value to solfegeIndices based on shuffledIndices value that matches the current scale
                    solfegeIndices[j] = shuffledIndices[x]
                    
                    //this array only used in harmonic questions
                    answerArray.append(solfegeArray[solfegeIndices[j]])
                    
                    //iterate x so that it is the next index in next loop
                    x++
                }
                //USE THIS FOR CHROMATIC SCALE
                //solfegeIndices[j] = shuffledIndices[j]
                
            }
//END HARMONIC QUESTIONS----------------------------------------------------------------------------------
                
            else //if harmonicQuestion == false. cases: limit to syllables, bebop scales, all others
            {
                //if user inputs syllables, limit melody to those
                if limitToSyllables == true
                {
                    
                    while !sylChosenArray.contains(solfegeArray[solfegeIndices[j]])
                    {
                        solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                    }
                }
                    
                //bebop scales
                else if scale == "bebop_Major" || scale == "bebop_minor"
                {

                    if j > 0
                    {
                        //previous index was a downbeat
                        if (j-1) % 2 == 0 //random1to100 <= percentageOfSmallerIntervals
                        {
                            //if downbeat is a passing tone
                            if passingTones.contains(solfegeArray[solfegeIndices[j-1]].substringToIndex(solfegeArray[solfegeIndices[j-1]].endIndex.predecessor()))
                            {
                                
                                indexOfPreviousNote = pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)!
                                
                                random0or1 = arc4random_uniform(2)
                                
                                //makes the next note up a step half the time, down a step half the time (prepare for sandwich)
                                if random0or1 == 0
                                {
                                
                                    if indexOfPreviousNote + 2 < pianoWavArray.count
                                    {
                                        indexOfCurrentNote = indexOfPreviousNote + 2
                                    }
                                    else
                                    {
                                        indexOfCurrentNote = indexOfPreviousNote - 2
                                    }
                                }
                                else //if random0or1 == 1
                                {
                                    
                                    if indexOfPreviousNote - 2 >= 0
                                    {
                                        indexOfCurrentNote = indexOfPreviousNote - 2
                                    }
                                    else
                                    {
                                        indexOfCurrentNote = indexOfPreviousNote + 2
                                    }
                                }
                                
                                //get key for value
                                for (syllable, note) in solfegeWavDictionary
                                {
                                    if note == pianoWavArray[indexOfCurrentNote]
                                    {
                                        currentSyllable = syllable
                                    }
                                }
                                //make current index the current syllable
                                solfegeIndices[j] = solfegeArray.indexOf(currentSyllable)!
                            }
                            //if previous note (downbeat) was not passing tone, make interval of 5th or less with current note
                            else
                            {
                                while abs(pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)! - pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!)!) > maxIntervalBebop
                                {
                                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                                }
                            }
                        }   //close downbeats
                            
                        //previous index was an upbeat
                        else //if (j-1) % 2 == 1
                        {
                            //if previous downbeat was passing tone, sandwich
                            if passingTones.contains(solfegeArray[solfegeIndices[j-2]].substringToIndex(solfegeArray[solfegeIndices[j-2]].endIndex.predecessor()))
                            {
                                
                                indexOfPreviousNote = pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)!

                                indexOfPreviousNote2 = pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-2]]]!)!
                                
                                //for bebop_Major only: dont sandwich resolve to fa (last 2 were fi then mi)
                                if scale == "bebop_Major" && solfegeArray[solfegeIndices[j-1]].rangeOfString("mi") != nil
                                {
                                    randomIntervalLessThanMaxBebop = Int(arc4random_uniform(UInt32(maxIntervalBebop))) + 1
                                    random0or1 = arc4random_uniform(2)
                                    
                                    //makes the next note up random interval half the time, down random interval half the time
                                    if random0or1 == 0
                                    {
                                        if indexOfPreviousNote + randomIntervalLessThanMaxBebop < pianoWavArray.count
                                        {
                                            indexOfCurrentNote = indexOfPreviousNote + randomIntervalLessThanMaxBebop
                                        }
                                        else
                                        {
                                            indexOfCurrentNote = indexOfPreviousNote - randomIntervalLessThanMaxBebop
                                        }
                                    }
                                    else //if random0or1 == 1
                                    {
                                        
                                        if indexOfPreviousNote - randomIntervalLessThanMaxBebop >= 0
                                        {
                                            indexOfCurrentNote = indexOfPreviousNote - randomIntervalLessThanMaxBebop
                                        }
                                        else
                                        {
                                            indexOfCurrentNote = indexOfPreviousNote + randomIntervalLessThanMaxBebop
                                        }
                                    }
                                
                                }
                                else    //sandwich
                                {
                                    indexOfCurrentNote = (indexOfPreviousNote + indexOfPreviousNote2) / 2
                                }
                                
                                //ensures the current syllable is what I want it to be based on above
                                for (syllable, note) in solfegeWavDictionary
                                {
                                    if note == pianoWavArray[indexOfCurrentNote]
                                    {
                                        currentSyllable = syllable
                                    }
                                }
                                
                                //make current index the current syllable
                                solfegeIndices[j] = solfegeArray.indexOf(currentSyllable)!
                                
                            }
                            //if downbeat was not passing tone
                            else
                            {
                                //if previous tone (upbeat) was passing tone, then resolve
                                if passingTones.contains(solfegeArray[solfegeIndices[j-1]].substringToIndex(solfegeArray[solfegeIndices[j-1]].endIndex.predecessor()))
                                {
                                    indexOfPreviousNote = pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)!
                                    
                                    indexOfPreviousNote2 = pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-2]]]!)!

                                    //if previous 2 were 1 whole step apart and previous was passing tone, sandwich
                                    if abs(indexOfPreviousNote - indexOfPreviousNote2) == 2
                                    {
                                        //bebop_Major only: don't resolve to fa if last 2 syllables were mi and fi
                                        if scale == "bebop_Major" && solfegeArray[solfegeIndices[j-1]].rangeOfString("fi") != nil
                                        {
                                            randomIntervalLessThanMaxBebop = Int(arc4random_uniform(UInt32(maxIntervalBebop))) + 1
                                            random0or1 = arc4random_uniform(2)
                                            
                                            //makes the next note up random interval half the time, down random interval half the time
                                            if random0or1 == 0
                                            {
                                                if indexOfPreviousNote + randomIntervalLessThanMaxBebop < pianoWavArray.count
                                                {
                                                    indexOfCurrentNote = indexOfPreviousNote + randomIntervalLessThanMaxBebop
                                                }
                                                else
                                                {
                                                    indexOfCurrentNote = indexOfPreviousNote - randomIntervalLessThanMaxBebop
                                                }
                                            }
                                            else //if random0or1 == 1
                                            {
                                                
                                                if indexOfPreviousNote - randomIntervalLessThanMaxBebop >= 0
                                                {
                                                    indexOfCurrentNote = indexOfPreviousNote - randomIntervalLessThanMaxBebop
                                                }
                                                else
                                                {
                                                    indexOfCurrentNote = indexOfPreviousNote + randomIntervalLessThanMaxBebop
                                                }
                                            }

                                        }
                                        else    //sandwich
                                        {
                                            indexOfCurrentNote = (indexOfPreviousNote + indexOfPreviousNote2) / 2
                                        }
                                    }
                                    //resolve to scaletone from passing tone, not sandwich
                                    else    //previous 2 notes were not whole step apart
                                    {
                                        random0or1 = arc4random_uniform(2)
                                        
                                        //makes the next note up a half step half the time, down a half step half the time
                                        if random0or1 == 0
                                        {
                                            
                                            if indexOfPreviousNote + 1 < pianoWavArray.count
                                            {
                                                indexOfCurrentNote = indexOfPreviousNote + 1
                                            }
                                            else
                                            {
                                                indexOfCurrentNote = indexOfPreviousNote - 1
                                            }
                                        }
                                        else //if random0or1 == 1
                                        {
                                            
                                            if indexOfPreviousNote - 1 >= 0
                                            {
                                                indexOfCurrentNote = indexOfPreviousNote - 1
                                            }
                                            else
                                            {
                                                indexOfCurrentNote = indexOfPreviousNote + 1
                                            }
                                        }
                                    }
                                    
                                    //This generates the correct index for current syllable
                                    for (syllable, note) in solfegeWavDictionary
                                    {
                                        if note == pianoWavArray[indexOfCurrentNote]
                                        {
                                            currentSyllable = syllable
                                        }
                                    }
                                    //make current index the current syllable
                                    solfegeIndices[j] = solfegeArray.indexOf(currentSyllable)!
                                    
                                }
                                //if previous tone (upbeat) was not passing tone, make interval of 5th or less with current tone
                                else
                                {
                                    
                                    while abs(pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)! - pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!)!) > maxIntervalBebop
                                    {
                                        solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                                    }
                                    
                                }
                            
                            }
                            

                        }   //close upbeats

                    }
                    
                    //if last note is not a scaletone or fi (for bebop_Major), make it one
                    if j == numberOfNotesPlayed - 1 && numberOfNotesPlayed > 1
                    {
                        indexOfCurrentNote = pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!)!
                        
                        //if current syllable is not an endingTone, make it one
                        if !endingTones.contains(solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()))
                        {
                            random0or1 = arc4random_uniform(2)
                            
                            //makes the last note an endingTone (resolution point)
                            if random0or1 == 0
                            {
                                
                                if indexOfCurrentNote + 1 < pianoWavArray.count
                                {
                                    indexOfCurrentNote++
                                }
                                else
                                {
                                    indexOfCurrentNote--
                                }
                            }
                            else //if random0or1 == 1
                            {
                            
                                if indexOfCurrentNote - 1 >= 0
                                {
                                    indexOfCurrentNote--
                                }
                                else
                                {
                                    indexOfCurrentNote++
                                }
                            }
                        }
                        
                        //This generates the correct index for current syllable
                        for (syllable, note) in solfegeWavDictionary
                        {
                            if note == pianoWavArray[indexOfCurrentNote]
                            {
                                currentSyllable = syllable
                            }
                        }
                        //make current index the current syllable
                        solfegeIndices[j] = solfegeArray.indexOf(currentSyllable)!
                    }
                    
                    
                }
                else if scale == "Major" || scale == "minor(relative)" || scale == "minor(melodic)" || scale == "minor(harmonic)" || scale == "minor(blues)" || scale == "chromatic"
                {
                    
                    if j == 0
                    {
                        
                        while !currentSolfegeScale.contains(solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()))
                        {
                            solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                        }
                        
                    }
                    else //if j > 0
                    {
                        //after first random note generated, ensure next notes are mostly closer together
                        if random1to100 <= percentageOfSmallerIntervals
                        {
                        
                            //indices of solfegewavdictionary[solfegeArray[solfegeIndices[j]]] in pianowavarray can't be far apart
                            while abs(pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)! - pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!)!) >= smallestLargeInterval || !currentSolfegeScale.contains(solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()))
                            {
                                solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                            }
                        }

                        else //if random1to100 > 60 && random1to100 <= 90
                        {
                            while abs(pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)! - pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!)!) < smallestLargeInterval || abs(pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j-1]]]!)! - pianoWavArray.indexOf(solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!)!) > largestInterval || !currentSolfegeScale.contains(solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()))
                            {
                                solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                            }
                    
                        }

                    }
                    
                }
                
                
            } //CLOSE OF NON-HARMONIC QUESTIONS

            //this is done independently in harmonicChordQuestion because it generates questions using currentSolfegeScale instead of solfegeArray.
            //works with all questions except harmonicChordQuestion
            if harmonicChordQuestion == false || harmonicQuestion == false
            {
                
                //contains the notes ie ["Db3", "A4", "C3"] of the melody
                notesPlayed[j] = solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!
                
                //the solfege syllable of the current note
                var solfegeKeysOfCurrentNote = (solfegeWavDictionary as NSDictionary).allKeysForObject(notesPlayed[j])
                var solfegeKeyOfCurrentNote = solfegeKeysOfCurrentNote[0] as! String
                
                answer += solfegeKeyOfCurrentNote.substringToIndex(solfegeKeyOfCurrentNote.endIndex.predecessor()) + " "
                
                //answer array for melodic questions
                if harmonicQuestion == false
                {
                    answerArrayMelodic.append(solfegeKeyOfCurrentNote)
                    
                }
                //answer += solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()) + " "
            }
            
            //FOR MOVING TONIC, 2+ NOTE MELODY. MAKES LAST NOTE SAME AS NEW FIRST NOTE
            if j == numberOfNotesPlayed - 1 && movingTonic == true && numberOfNotesPlayed > 1 && harmonicQuestion == false
            {
                lastNoteOfPreviousMelody = solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!
            }
            
        }//CLOSE FOR LOOP THAT GENERATES SOLFEGE INDICES ARRAY
        
        //change the tonic back to original one for outside melodies
        if highlightedPickerScale == "outside_melodies"
        {
            rootIndex = savedRootIndex
            populateSolfegeWavDictionary()
            
            answerArrayMelodic.removeAll(keepCapacity: true)
            answer = ""
            
            for var j1 = 0; j1 < notesPlayed.count; j1++
            {
                var solfegeKeysOfCurrentOutsideNote = (solfegeWavDictionary as NSDictionary).allKeysForObject(notesPlayed[j1])
                var solfegeKeyOfCurrentOutsideNote = solfegeKeysOfCurrentOutsideNote[0] as! String
                
                
                answerArrayMelodic.append(solfegeKeyOfCurrentOutsideNote)
                
                answer += solfegeKeyOfCurrentOutsideNote.substringToIndex(solfegeKeyOfCurrentOutsideNote.endIndex.predecessor()) + " "
            }
        }
        
        print("answerarrayMelodic \(answerArrayMelodic)")

        print(notesPlayed)
        print(answer)
        
        //plays chord(s) then melody with delay
        if playCadence == true
        {
            delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
            Int64((shortStartDelays[shortStartDelays.count - 1] + timeBetweenNotes) * Double(NSEC_PER_SEC)))
        }
        else
        {
            delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
                Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
        }
        
        //play chord and melody if moveable do
        if movingTonic == true
        {

            self.playChord(self)
            message.text = "Tonic: \(pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor()))"

            dispatch_after(delayTime1, dispatch_get_main_queue())
            {
                self.playMelody(self)
            }
        }
        //fixed tonic
        else if movingTonic == false
        {
            //if first problem, play chord
            if problems == 1
            {
                self.playChord(self)
                dispatch_after(delayTime1, dispatch_get_main_queue())
                {
                    self.playMelody(self)
                }
            }
            else
            {
                self.playMelody(self)
                message.text = ""
            }
        }
        
        scoreLabel.text = "\(points)/\(problems)"
        
        savedAnswerArray = answerArray
        
    }//CLOSE GENERATE NEW MELODY

    //SCALETYPE is the picker for different scales
    @IBOutlet var scaleType: UIPickerView!
    
    //the picker values of current game type (non-harmonic or harmonic)
    var scaleData = [String]()
    
    var scaleDataNonHarmonic = ["Major","minor(relative)","chosen_tones","Major_and_minor","bebop_Major","bebop_minor","minor(blues)","minor(melodic)","minor(harmonic)","outside_melodies","chromatic","random"]

    var scaleDataHarmonic = ["Major","minor(relative)","Major_and_minor","minor(blues)","minor(melodic)","minor(harmonic)","chromatic","random"]
    
    @IBOutlet var scaleChosen: UITextField!
    
    var playerChords = [AVAudioPlayer]()
    //time between cadence chords
    var shortStartDelays:[NSTimeInterval] = [0,0.5,1,1.5,2]//[0,timeBetweenNotes,2*timeBetweenNotes,3*timeBetweenNotes,4*timeBetweenNotes]
    var playCadence = false
    
    @IBOutlet var playChordOutlet: UIButton!
    @IBAction func playChord(sender: AnyObject)
    {
        playerChords.removeAll(keepCapacity: true)
        
        var indexOfLowerTi = rootIndex - 1
        
        var endNumberOfTonicChosen = Int(String(pianoWavArray[rootIndex][pianoWavArray[rootIndex].endIndex.predecessor()]))
        
        //endnumberoftonic + x = 3 (endnumberofTonicSounded)
        var endNumberOfTonicSounded = 3
        
        var x = endNumberOfTonicSounded - endNumberOfTonicChosen!
        
        print("x is \(x) tonic: \(tonic)")
        
        //populated with all possible tones in cadences (all notes from lower ti to higher ti)
        var fileLocationsOfNotes = [NSObject]()
        
        var shortStartDelay: NSTimeInterval = 0
        
        //populating fileLocationsOfNotes array with filenames of notes from ti to higher ti
        var numberOfUniqueTones = 12
        for var j = 0; j < numberOfUniqueTones; j++
        {
            //quick fix for key of C
            if tonic == "C"
            {
                if j == 0
                {
                    fileLocationsOfNotes.append(NSBundle.mainBundle().pathForResource("B" + "\(endNumberOfTonicSounded - 1)", ofType: "mp3")!)
                }
                else
                {
                    fileLocationsOfNotes.append(NSBundle.mainBundle().pathForResource(pianoWavArray[indexOfLowerTi+j].substringToIndex(pianoWavArray[indexOfLowerTi+j].endIndex.predecessor()) + "\(endNumberOfTonicSounded)", ofType: "mp3")!)
                }
            }
            else
            {
                var endNumberOfCurrentNote = Int(String(pianoWavArray[indexOfLowerTi+j][pianoWavArray[indexOfLowerTi+j].endIndex.predecessor()]))
                
                //TAKE THE END NUMBER OF PIANOWAVARRAY[ROOTINDEX] AND ADD X TO IT (X CHANGES BASED ON THE END NUMBER)
                fileLocationsOfNotes.append(NSBundle.mainBundle().pathForResource(pianoWavArray[indexOfLowerTi+j].substringToIndex(pianoWavArray[indexOfLowerTi+j].endIndex.predecessor()) + "\(endNumberOfCurrentNote! + x)", ofType: "mp3")!)
            }
        }
        
        var numberOfNotesInChord = 3
        
        var numberOfChordsToPlay = Int()
        
        if playCadence == true
        {
            numberOfChordsToPlay = 5
        }
        else //play just tonic chord
        {
            numberOfChordsToPlay = 1
        }
        
        //each value is an index of fileLocationsOfNotes used to play the chords in order
        var indicesOfCadenceNotes = [Int]()
        
        //1,5,8 is do, mi, sol, etc
        var indicesOfMajorCadenceNotes = [1,5,8,1,6,10,1,5,8,0,3,8,1,5,8]
        var indicesOfMinorCadenceNotes = [1,4,8,1,6,9,1,4,8,0,3,8,1,4,8]
        
        //if scale chosen has "minor" in string, play minor chord
        if scale.rangeOfString("minor") != nil
        {
            if scale == "Major_and_minor"
            {
                //Major
                if randomMajorminor == 0
                {
                    indicesOfCadenceNotes = indicesOfMajorCadenceNotes
                }
                    //minor
                else if randomMajorminor == 1
                {
                    indicesOfCadenceNotes = indicesOfMinorCadenceNotes
                }
                
            }
            else if scale == "minor(blues)"
            {
                indicesOfCadenceNotes = indicesOfMajorCadenceNotes
            }
            else
            {
                indicesOfCadenceNotes = indicesOfMinorCadenceNotes
            }
        }
        else
        {
            indicesOfCadenceNotes = indicesOfMajorCadenceNotes
        }
        
        
        for var k = 0; k < numberOfNotesInChord * numberOfChordsToPlay; k++
        {
            do
            {
                try playerChords.append(AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationsOfNotes[indicesOfCadenceNotes[k]] as! String)))
            }
            catch
            {
            
            }
        }
        
        let now: NSTimeInterval = playerChords[1].deviceCurrentTime;
        
        //relates cadence play speed to tempo of notes
        //shortStartDelays = [0,0.5*timeBetweenNotes,1*timeBetweenNotes,1.5*timeBetweenNotes,2*timeBetweenNotes]
        
        //plays each chord with a delay in shortStartDelays[]
        for var i = 0; i < numberOfChordsToPlay; i++
        {
            //plays the notes in each chord
            for var h = 0; h < numberOfNotesInChord; h++
            {
                playerChords[h + i*numberOfNotesInChord].playAtTime(now + shortStartDelays[i])
            }
        
        }
        
        if answerIsShown == false
        {
            message.textColor = UIColor.blackColor()
            message.text = "Tonic: \(pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor()))"
        }
        
    }
    
    var harmonicQuestion = false//true
    var harmonicChordQuestion = true
    var harmonicChordRootPosQuestion = true
    
    @IBOutlet var playMelodyOutlet: UIButton!
    @IBAction func playMelody(sender: AnyObject)
    {
        player.removeAll(keepCapacity: true)
        
        var error: NSError? = nil
        
        var fileLocation = [NSObject]()
        
        var shortStartDelay: NSTimeInterval = 0
 
        for var i = 0; i < numberOfNotesPlayed; i++
        {
            
            //file name of notesPlayed array sound file is appended to fileLocation array
            fileLocation.append(NSBundle.mainBundle().pathForResource(notesPlayed[i], ofType: "mp3" as String)!)
            
            //file location of random sound file is appended to audio player array
            do
            {
                try player.append(AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocation[i] as! String)))
            }
            catch
            {
            
            }
            //current time associated with output device "player[0], the first random sound file played"
            let now: NSTimeInterval = player[0].deviceCurrentTime;
            
            player[i].playAtTime(now + shortStartDelay)
            
            //puts time between notes of non-harmonic questions. If answer is shown, play notes individually for harmonic questions
            if harmonicQuestion == false
            {
                shortStartDelay += timeBetweenNotes;
            }
        }
    }
    @IBOutlet var message: UITextView!
    
    @IBOutlet var numberOfNotesStepper: UIStepper!
    
    @IBAction func numberOfNotesStepperAction(sender: AnyObject)
    {
        numberOfNotesText.text = "\(Int(numberOfNotesStepper.value))"
        
        if numberOfNotesStepper.value == 1
        {
            melodyLengthText.text = "Length: \(Int(numberOfNotesStepper.value)) Tone"
        }
        else
        {
            melodyLengthText.text = "Length: \(Int(numberOfNotesStepper.value)) Tones"
        }

    }
    
    @IBOutlet var numberOfNotesText: UITextField!
    
    @IBOutlet var melodyLengthText: UILabel!
    
    @IBOutlet var iAdOutlet: ADBannerView!
    
    @IBOutlet var dismissAdOutlet: UIButton!
    
    @IBAction func dismissAdAction(sender: AnyObject) {
        
//        iAdOutlet.hidden = true//removeFromSuperview()
//        dismissAdOutlet.hidden = true//removeFromSuperview()
        
    }
    
    var appHasOpenedBefore = NSUserDefaults.standardUserDefaults().valueForKey("appHasOpenedBefore") as? Bool
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var launchCount = NSUserDefaults.standardUserDefaults().valueForKey("launchCount") as! Int
        
        if launchCount % 5 == 0 && launchCount != 0
        {
            let alertRate = UIAlertController(title: "Like the App?", message: "Please take a moment to review it in the App Store, or send me feedback on what you would like to see next!", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertRate.addAction(UIAlertAction(title: "Rate", style: .Default, handler: { (action) -> Void in
                
                //send to app store
                UIApplication.sharedApplication().openURL(NSURL(string: "https://itunes.apple.com/app/id1033412236")!)
                
                launchCount = 0
                NSUserDefaults.standardUserDefaults().setValue(launchCount, forKey: "launchCount")
                
            }))
            
            alertRate.addAction(UIAlertAction(title: "Send Feedback", style: .Default, handler: { (action) -> Void in
                
                //email me
                let email = "marcuslchoi@gmail.com?&subject=Melodic%20Instinct%20App%20Feedback"
                let url = NSURL(string: "mailto:\(email)")
                UIApplication.sharedApplication().openURL(url!)
                
                
                launchCount = 0
                NSUserDefaults.standardUserDefaults().setValue(launchCount, forKey: "launchCount")
            }))
            
            alertRate.addAction(UIAlertAction(title: "Not Now", style: .Default, handler: { (action) -> Void in
                //ignore
            }))
            
            self.presentViewController(alertRate, animated: true, completion: nil)
        }
            
        //make sure sound plays when muted
        do{
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }catch
        {}
        
        do{
            try AVAudioSession.sharedInstance().setActive(true)
        }catch
        {}
        
        
        iAdOutlet.hidden = true
        dismissAdOutlet.hidden = true
        
        //self.canDisplayBannerAds = true
        
        if appHasOpenedBefore != true
        {
        
            var alertHowToPlay = UIAlertController(title: "Welcome!", message: "Press Play (game mode) or New Question (practice mode). You will hear the tonic chord followed by a tone. Use the buttons below to play the tone. A correct answer will automatically generate a new question.", preferredStyle: UIAlertControllerStyle.Alert)
            
            alertHowToPlay.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action) -> Void in
                
                
            }))
            
            self.presentViewController(alertHowToPlay, animated: true, completion: nil)
        
        }
        
        appHasOpenedBefore = true
        NSUserDefaults.standardUserDefaults().setObject(appHasOpenedBefore, forKey: "appHasOpenedBefore")
        
        generateNewMelodyOutlet.layer.borderWidth = 1
        generateNewMelodyOutlet.layer.borderColor = UIColor.blackColor().CGColor
        

        scaleData = scaleDataNonHarmonic

        scaleChosen.hidden = true
        
        userGuessesText.hidden = true
        
        //keep this?
        resetGuessesOutlet.hidden = true
        numberOfNotesText.hidden = true
        
        populateSolfegeWavDictionary()
        
        let bgImage = UIImage(named: "bluebgBig.jpg")!//"music_bg1.jpg")!
        
        //let scaledImage = UIImage(CGImage: bgImage.CGImage, scale: UIScreen.mainScreen().scale, orientation: bgImage.imageOrientation)

        view.backgroundColor = UIColor(patternImage: bgImage)
        
        solfegeText.text = "do(1)"
        
        generateNewMelodyOutlet.layer.shadowColor = UIColor.blackColor().CGColor
        generateNewMelodyOutlet.layer.shadowOffset = CGSizeMake(2, 2)
        //generateNewMelodyOutlet.layer.shadowRadius = 5
        generateNewMelodyOutlet.layer.shadowOpacity = 1
        
        for solfegeOutlet in solfegeCollection
        {
            solfegeOutlet.enabled = false
            
            solfegeOutlet.layer.shadowColor = UIColor.blackColor().CGColor
            solfegeOutlet.layer.shadowOffset = CGSizeMake(2, 2)
            //solfegeOutlet.layer.shadowRadius = 5
            solfegeOutlet.layer.shadowOpacity = 1
        
        }
        
        playChordOutlet.enabled = false
        playMelodyOutlet.enabled = false
        
        showAnswerOutlet.enabled = false
       
        scaleType.dataSource = self
        scaleType.delegate = self
        
        
    }
    
    var highlightedPickerScale = "Major"
    //PICKER FUNCTIONS
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return scaleData.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return scaleData[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //is always equal to what is highlighted in picker
        highlightedPickerScale = scaleData[row]
        
        if scaleData[row] == "chosen_tones"
        {
            scaleChosen.hidden = false
            
            var dummyView = UIView(frame: CGRectMake(0, 0, 1, 1))
            
            scaleChosen.inputView = dummyView
            
            //puts textfield into editing mode
            scaleChosen.becomeFirstResponder()
            
            
            for solfegeOutlet in solfegeCollection
            {
                solfegeOutlet.enabled = true
            }
            
        }
        else
        {
            scaleChosen.hidden = true
        }
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //@IBOutlet var BPM: UITextView!
    
//DELEGATE AND SEGUE PASSING VALUES TO/FROM OPTIONS-------------------------------------------
    
    //use delegate to pass variables back to viewController
    func myVCDidFinish(controller: OptionsViewController, bpmPassed: Int, gameLengthIndexPassed: Int, numbSolfPassed: Int, playCadencePassed: Bool, rootIndexPassed: Int, pitchRangePassed: Int, movingTonicPassed: Bool, fixKeyConfigPassed: Bool, harmonicQuestionPassed: Bool, harmonicTypePassed: Int)
    {
        
        rootIndex = rootIndexPassed
        
        pitchRange = pitchRangePassed
        
        movingTonic = movingTonicPassed
        
        fixKeyConfigToC = fixKeyConfigPassed
        
        //changes time between notes based on bpm passed back to self
        timeBetweenNotes = NSTimeInterval(60.0/Double(bpmPassed))

        //changes gameLength
        if gameLengthIndexPassed == 0
        {
            gameLength = 1
        }
        else if gameLengthIndexPassed == 1
        {
            gameLength = 3
        }
        else if gameLengthIndexPassed == 2
        {
            gameLength = 5
        }
        
        //changes number system/solfege system
        if numbSolfPassed == 1
        {
            numberSystem = true
        }
        else //if numbSolfPassed == 0
        {
            numberSystem = false
        }
        
        playCadence = playCadencePassed
        
        harmonicQuestion = harmonicQuestionPassed
        if harmonicTypePassed == 0
        {
            harmonicChordRootPosQuestion = true
            harmonicChordQuestion = true
        }
        else if harmonicTypePassed == 1
        {
            harmonicChordRootPosQuestion = false
            harmonicChordQuestion = true
        }
        else if harmonicTypePassed == 2
        {
            harmonicChordRootPosQuestion = false
            harmonicChordQuestion = false
        }

        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    //use segue to pass value from this page to Options
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "optionsSegue")
        {
            if isPlaying == true
            {
                playPauseToggle(self)
            }
            
            if randomPitchRangeOption == true
            {
                pitchRange = 6
            }
            
            var ovc = segue.destinationViewController as! OptionsViewController;
            
            // 60/timebetweennotes gives BPM
            ovc.toPassBPM = Int(60/Double(timeBetweenNotes))
            
            ovc.toPassGameLength = gameLength
            
            //bool
            ovc.toPassNumbSolf = numberSystem
            
            ovc.toPassPlayCadence = playCadence
            
            ovc.toPassRootIndex = rootIndex
            
            ovc.toPassPitchRange = pitchRange
            
            ovc.toPassMovingTonic = movingTonic
            
            ovc.toPassFixKeyConfig = fixKeyConfigToC
            
            ovc.toPassHarmonicQuestion = harmonicQuestion
            
            if harmonicChordQuestion == true && harmonicChordRootPosQuestion == true
            {
                ovc.toPassHarmonicType = 0
            }
            else if harmonicChordQuestion == true && harmonicChordRootPosQuestion == false
            {
                ovc.toPassHarmonicType = 1
            }
            else if harmonicChordQuestion == false && harmonicChordRootPosQuestion == false
            {
                ovc.toPassHarmonicType = 2
            }
            
            ovc.delegate = self
            
        }
    }
    
//END: DELEGATE AND SEGUE PASSING VALUES TO/FROM OPTIONS--------------------------------------
    
    @IBOutlet var doOutlet: UIButton!
    @IBOutlet var reOutlet: UIButton!
    @IBOutlet var miOutlet: UIButton!
    @IBOutlet var faOutlet: UIButton!
    @IBOutlet var solOutlet: UIButton!
    @IBOutlet var laOutlet: UIButton!
    @IBOutlet var tiOutlet: UIButton!
    @IBOutlet var raOutlet: UIButton!
    @IBOutlet var meOutlet: UIButton!
    @IBOutlet var fiOutlet: UIButton!
    @IBOutlet var leOutlet: UIButton!
    @IBOutlet var teOutlet: UIButton!
    
    //half keys shown for keyboard reference, non-functional
    @IBOutlet var leftHalfKey: UIButton!
    @IBOutlet var rightHalfKey: UIButton!
    
    //solfege button collection in array
    @IBOutlet var solfegeCollection: [UIButton]!
    
  
    //these must be defined outside the func so that the var exists between func calls and is updated each time func is called
    var verticalConstraints = [NSLayoutConstraint]()
    var horizontalConstraints = [NSLayoutConstraint]()
    
    //re-draw the constraints when view changes orientation
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        //calls new key config when transition between orientations is completed. Animation is nil
        coordinator.animateAlongsideTransition({context in
            self.newKeyConfiguration()
            
            }, completion: nil)
    }
    
    var fixKeyConfigToC = false
    
    var tonic: String! //pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor())
    
    func newKeyConfiguration()
    {
        
        leftHalfKey.hidden = false
        rightHalfKey.hidden = false
        
        //message.textColor = UIColor.blackColor()
        //message.text = "Tonic: \(pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor()))"
        
        var halfKeyDisplacement = self.view.frame.size.width/15
        
        //initial distance (do) is at left side of screen
        var horizontalDist:CGFloat = -halfKeyDisplacement//0
        var verticalDist:CGFloat = 0
        
        var black = UIColor.blackColor()
        var white = UIColor.whiteColor()
        var blue = UIColor.blueColor()
        
        var bgColor = UIColor()
        var tintColor = blue
        var i:Int
        
        var indexOfLeftHalf:Int = 0
        var indexOfDb:Int
        var indexOfEb:Int
        var indexOfGb:Int
        var indexOfAb:Int
        var indexOfBb:Int
        var indexOfRightHalf:Int = 13
        
        var indexOfC:Int
        var indexOfF:Int
            
        //Width of keys: calculates width of each note based on width of screen. Same for all tonic keys
        // 1/7.5 width: 7 notes + .5 for black key gives 2/15 (2/16 makes smaller for space between)
        self.view.addConstraint(NSLayoutConstraint(item: doOutlet, attribute: .Width, relatedBy: .Equal, toItem: self.view, attribute: .Width, multiplier: 2/16, constant: 0))

        
        
        if tonic == "C" || fixKeyConfigToC == true
        {
            indexOfDb = 2
            indexOfEb = 4
            indexOfGb = 7
            indexOfAb = 9
            indexOfBb = 11
            
            halfKeyDisplacement = self.view.frame.size.width/14
            
            //offset from C as 2nd white key in a row doesn't apply if key is C
            indexOfC = -1
            indexOfF = 6

        }
        else if tonic == "F"
        {
            indexOfDb = 9
            indexOfEb = 11
            indexOfGb = 2
            indexOfAb = 4
            indexOfBb = 6
            
            indexOfC = 8
            indexOfF = -1
            
            halfKeyDisplacement = self.view.frame.size.width/14

        }

        else if tonic == "Db"
        {
            
            indexOfDb = 1//0
            indexOfEb = 3//2
            indexOfGb = 6//5
            indexOfAb = 8//7
            indexOfBb = 10//9
            
            indexOfC = 12//11
            indexOfF = 5//4
            
        }
        else if tonic == "D"
        {
            indexOfDb = 12//11
            indexOfEb = 2//1
            indexOfGb = 5//4
            indexOfAb = 7//6
            indexOfBb = 9//8
            
            indexOfC = 11//10
            indexOfF = 4//3

        }
        else if tonic == "Eb"
        {
            indexOfDb = 11
            indexOfEb = 1
            indexOfGb = 4
            indexOfAb = 6
            indexOfBb = 8
            
            indexOfC = 10
            indexOfF = 3

        }
        else if tonic == "E"
        {
            indexOfDb = 10
            indexOfEb = 12
            indexOfGb = 3
            indexOfAb = 5
            indexOfBb = 7
            
            indexOfC = 9
            indexOfF = 2

        }
        else if tonic == "Gb"
        {
            indexOfDb = 8
            indexOfEb = 10
            indexOfGb = 1
            indexOfAb = 3
            indexOfBb = 5
            
            indexOfC = 7
            indexOfF = 12

        }
        else if tonic == "G"
        {
            indexOfDb = 7
            indexOfEb = 9
            indexOfGb = 12
            indexOfAb = 2
            indexOfBb = 4
            
            indexOfC = 6
            indexOfF = 11

        }
        else if tonic == "Ab"
        {
            indexOfDb = 6
            indexOfEb = 8
            indexOfGb = 11
            indexOfAb = 1
            indexOfBb = 3
            
            indexOfC = 5
            indexOfF = 10

        }
        else if tonic == "A"
        {
            indexOfDb = 5
            indexOfEb = 7
            indexOfGb = 10
            indexOfAb = 12
            indexOfBb = 2
            
            indexOfC = 4
            indexOfF = 9

        }
        else if tonic == "Bb"
        {
            indexOfDb = 4
            indexOfEb = 6
            indexOfGb = 9
            indexOfAb = 11
            indexOfBb = 1
            
            indexOfC = 3
            indexOfF = 8

        }
        else //if tonic == "B"
        {
            indexOfDb = 3
            indexOfEb = 5
            indexOfGb = 8
            indexOfAb = 10
            indexOfBb = 12
            
            indexOfC = 2
            indexOfF = 7

        }
        
        
        for i = 0; i < solfegeCollection.count; i++
        {
            
            //black keys vertical displacement
            if i == indexOfDb || i == indexOfEb || i == indexOfGb || i == indexOfAb || i == indexOfBb
            {
                bgColor = black
                tintColor = white
                verticalDist = -doOutlet.frame.size.height - 2//-42
            }
            //white keys
            else if i != indexOfLeftHalf && i != indexOfRightHalf
            {
                bgColor = white
                tintColor = blue
                verticalDist = 0
            }
            //half keys on left and right
            else
            {
                if fixKeyConfigToC == true
                {
                    //just so that bgcolor has a value and doesn't cause an error
                    bgColor = white
                    
                    leftHalfKey.hidden = true
                    rightHalfKey.hidden = true
                }
                else if tonic == "Db" || tonic == "Eb" || tonic == "Gb" || tonic == "Ab" || tonic == "Bb"
                {
                    if i == indexOfLeftHalf
                    {
                        bgColor = white
                        verticalDist = 0
                    }
                    else //if i == indexOfRightHalf
                    {
                        bgColor = black
                        verticalDist = -doOutlet.frame.size.height - 2
                    }
                }
                else if tonic == "D" || tonic == "E" || tonic == "G" || tonic == "A" || tonic == "B"
                {
                    if i == indexOfLeftHalf
                    {
                        bgColor = black
                        verticalDist = -doOutlet.frame.size.height - 2
                    }
                    else //if i == indexOfRightHalf
                    {
                        bgColor = white
                        verticalDist = 0
                    }
                }
                else
                {
                    //just so that bgcolor has a value and doesn't cause an error
                    bgColor = white
                    
                    leftHalfKey.hidden = true
                    rightHalfKey.hidden = true
                }
                    
            }
            
            //index of 2nd white key in a row (F and/or C) is twice distance away from previous key than if black key were between them
            if i == indexOfC || i == indexOfF
            {
                horizontalDist += halfKeyDisplacement
            }
            
            //put constraints into array
            verticalConstraints.append(NSLayoutConstraint(item: solfegeCollection[i], attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: verticalDist))
            
            //makes sure the constant of each constraint is updated everytime function called
            verticalConstraints[i].constant = verticalDist
            
            //applying vertical position of keys
            self.view.addConstraint(verticalConstraints[i])
            
            solfegeCollection[i].backgroundColor = bgColor
            solfegeCollection[i].tintColor = tintColor
            solfegeCollection[i].layer.borderWidth = 1.5
            solfegeCollection[i].layer.borderColor = UIColor.greenColor().CGColor //black.CGColor
            
            //horizontal positions of keys
            horizontalConstraints.append(NSLayoutConstraint(item: solfegeCollection[i], attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: horizontalDist))
            
            horizontalConstraints[i].constant = horizontalDist
            self.view.addConstraint(horizontalConstraints[i])
            
            horizontalDist += halfKeyDisplacement
            
        }

    }
    
    //number system stuff
    var numberSystem = false
    
    var randomPitchRangeOption = false
    
    override func viewDidAppear(animated: Bool)
    {
//        iAdOutlet.hidden = false
//        dismissAdOutlet.hidden = false
        
        //pitchRange is the end number of the lower scale
        if pitchRange == 6
        {
            randomPitchRangeOption = true
            //pitchRange = Int(arc4random_uniform(5)) + 1
        }
        else
        {
            randomPitchRangeOption = false
            
            var lowerC = "C" + "\(pitchRange)"
            var lowerDb = "Db" + "\(pitchRange)"
            var lowerD = "D" + "\(pitchRange)"
            var lowerEb = "Eb" + "\(pitchRange)"
            var lowerE = "E" + "\(pitchRange)"
            var lowerF = "F" + "\(pitchRange)"
            var lowerGb = "Gb" + "\(pitchRange)"
            var lowerG = "G" + "\(pitchRange)"
            var lowerAb = "Ab" + "\(pitchRange)"
            var lowerA = "A" + "\(pitchRange)"
            var lowerBb = "Bb" + "\(pitchRange)"
            var lowerB = "B" + "\(pitchRange)"
            
            var higherC = "C" + "\(pitchRange+1)"
            var higherDb = "Db" + "\(pitchRange+1)"
            var higherD = "D" + "\(pitchRange+1)"
            var higherEb = "Eb" + "\(pitchRange+1)"
            var higherE = "E" + "\(pitchRange+1)"
            var higherF = "F" + "\(pitchRange+1)"
            var higherGb = "Gb" + "\(pitchRange+1)"
            var higherG = "G" + "\(pitchRange+1)"
            var higherAb = "Ab" + "\(pitchRange+1)"
            var higherA = "A" + "\(pitchRange+1)"
            var higherBb = "Bb" + "\(pitchRange+1)"
            var higherB = "B" + "\(pitchRange+1)"
            
            pianoWavArray = [lowerC,lowerDb,lowerD,lowerEb,lowerE,lowerF,lowerGb,lowerG,lowerAb,lowerA,lowerBb,lowerB,higherC,higherDb,higherD,higherEb,higherE,higherF,higherGb,higherG,higherAb,higherA,higherBb,higherB]
        }
        
        tonic = pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor())

        if harmonicQuestion == false
        {
            scaleData = scaleDataNonHarmonic
            
            numberOfNotesStepper.maximumValue = 16
        }
        else if harmonicQuestion == true
        {
            scaleChosen.hidden = true
            scaleData = scaleDataHarmonic
            
            numberOfNotesStepper.maximumValue = 8
            
            //if number of notes played is higher than max for harmonic questions, set it to 2
            if numberOfNotesPlayed > Int(numberOfNotesStepper.maximumValue)
            {
                numberOfNotesStepper.value = 2
                
                //NEED TO GET RID OF THIS TEXTFIELD
                numberOfNotesText.text = "\(Int(numberOfNotesStepper.value))"
                
                numberOfNotesPlayed = Int(numberOfNotesText.text!)!
                
                //numberOfNotesPlayed = Int(numberOfNotesStepper.value)
                
                melodyLengthText.text = "Length: \(Int(numberOfNotesStepper.value)) Tones"
            }
        }
        
        scaleType.reloadAllComponents()
        
        highlightedPickerScale = scaleData[scaleType.selectedRowInComponent(0)]
        
        message.text = "Tonic: \(pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor()))" //Press Play or New Question to begin."
        //"Welcome, \(PFUser.currentUser()!.username!)!"
        
        message.font = UIFont(name: message.font!.fontName, size: 22)
        message.textColor = UIColor.blackColor()
        message.layer.opacity = 0.80
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        newKeyConfiguration()
        if numberSystem == true
        {
            doOutlet.setTitle("1", forState: .Normal)
            reOutlet.setTitle("2", forState: .Normal)
            miOutlet.setTitle("3", forState: .Normal)
            faOutlet.setTitle("4", forState: .Normal)
            solOutlet.setTitle("5", forState: .Normal)
            laOutlet.setTitle("6", forState: .Normal)
            tiOutlet.setTitle("7", forState: .Normal)
            raOutlet.setTitle("#1/b2", forState: .Normal)
            meOutlet.setTitle("#2/b3", forState: .Normal)
            fiOutlet.setTitle("#4/b5", forState: .Normal)
            leOutlet.setTitle("#5/b6", forState: .Normal)
            teOutlet.setTitle("#6/b7", forState: .Normal)
        }
        else if numberSystem == false
        {
            doOutlet.setTitle("do", forState: .Normal)
            reOutlet.setTitle("re", forState: .Normal)
            miOutlet.setTitle("mi", forState: .Normal)
            faOutlet.setTitle("fa", forState: .Normal)
            solOutlet.setTitle("sol", forState: .Normal)
            laOutlet.setTitle("la", forState: .Normal)
            tiOutlet.setTitle("ti", forState: .Normal)
            raOutlet.setTitle("di/ra", forState: .Normal)
            meOutlet.setTitle("ri/me", forState: .Normal)
            fiOutlet.setTitle("fi/se", forState: .Normal)
            leOutlet.setTitle("si/le", forState: .Normal)
            teOutlet.setTitle("li/te", forState: .Normal)
        }
    }
    
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
    
//    //prevents keyboard from showing when editing a text field
//    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
//        return false
//    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
}

