//
//  CadenceTableViewCell.swift
//  ear training
//
//  Created by Marcus Choi on 10/31/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class CadenceTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBOutlet var cadenceOutlet: UISwitch!
    @IBAction func cadenceValueChanged(sender: AnyObject)
    {
        var playCadence = Bool()
        if cadenceOutlet.on == true
        {
            playCadence = true
        }
        else
        {
            playCadence = false
        }
    }

}
