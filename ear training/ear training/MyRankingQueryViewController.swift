//
//  MyRankingQueryViewController.swift
//  ear training
//
//  Created by Marcus Choi on 8/3/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class MyRankingQueryViewController: UIViewController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var currentHighScore:Int! //= 0
        
        //finding current high score
        var queryHighScore = PFQuery(className: "HighScore")
        queryHighScore.whereKey("username", equalTo: PFUser.currentUser().username)
        queryHighScore.whereKey("gameLength", equalTo: 1)

        //get the current high score and replace if new score is higher
        //queryHighScore.orderByDescending("score")
        queryHighScore.getFirstObjectInBackgroundWithBlock({ (object: AnyObject?, error: NSError?) -> Void in
            
            if error == nil
            {
                if let object = object as? PFObject
                {
                    
                    currentHighScore = object.valueForKey("score") as Int
                    println("score query worked \(currentHighScore)")
                    
                    var queryRanking = PFQuery(className: "HighScore")
                    queryRanking.whereKey("gameLength", equalTo: 1)
                    queryRanking.whereKey("score", greaterThan: currentHighScore)
                    
                    var myRanking:Int32 = 0
                
                    queryRanking.countObjectsInBackgroundWithBlock(
                    { (numberOfHigherScoreUsers: Int32, error: NSError?) -> Void in
                        
                        myRanking = numberOfHigherScoreUsers + 1
                        
                        println(myRanking)
                        
                        
                    })
                    
 
                }
            }
            else
            {
                // Log details of the failure
                println("Error: \(error!) \(error!.userInfo!)")
            }
            
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
