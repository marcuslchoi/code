//
//  HarmonicTableViewCell.swift
//  ear training
//
//  Created by Marcus Choi on 10/31/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class HarmonicTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    @IBOutlet var harmonicOutlet: UISwitch!
    @IBAction func harmonicValueChanged(sender: AnyObject)
    {
        var harmonicGame = Bool()
        if harmonicOutlet.on == true
        {
            harmonicGame = true
        }
        else
        {
            harmonicGame = false
        }
    }
}
