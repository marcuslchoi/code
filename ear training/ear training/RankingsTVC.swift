//
//  RankingsTVC.swift
//  ear training
//
//  Created by Marcus Choi on 6/30/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class RankingsTVC: UITableViewController {

    
    //var userScoreDictionary = NSMutableDictionary()
    
    var gameLengthArray = [1,3,5]
    
    var userScoreArray = [String]()
    
    func retrieveScores()
    {
        
        var query = PFQuery(className: "HighScore")
        query.whereKey("gameLength", equalTo: gameLengthArray[0])
        query.orderByDescending("score1")
   
        query.findObjectsInBackgroundWithBlock(
        {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            
            if error == nil
            {
                println("Successfully retrieved \(objects!.count) scores.")
                
                if let objects = objects as? [PFObject]
                {
                    var i = 1
                    
                    for object in objects
                    {
                        
                        var username = object.valueForKey("username") as String
                        
                        var score:Int! = object.valueForKey("score1") as Int
                        
                        var scale = object.valueForKey("scale") as String
                        
                        self.userScoreArray.append("\(i). \(username): \(score) \(scale)")
                        
                        i++

                    }
                }

                
                println(self.userScoreArray)
                
                //ok to reload here if not too many users
                self.tableView.reloadData()

            }
            else
            {
                println("Error: \(error!) \(error!.userInfo!)")
            }
        })
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        retrieveScores()
        
        title = "Rankings"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return userScoreArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("cellID") as? UITableViewCell
        if(cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cellID")
        }
        
        cell!.textLabel!.text = userScoreArray[indexPath.row]
        return cell!
        
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as? UITableViewCell
    
    cell?.textLabel?.text = "Test"
    
    return cell!
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
}
