//
//  MyScores.swift
//  ear training
//
//  Created by Marcus Choi on 8/4/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit

class MyScores: NSObject {
   
    //string array containing the names of the sections.
    var sections:[String] = []
    
    //an array of string arrays, ie [["300. Bebop","250. Major","23. Minor"],["300. Bebop","2450. Major","235. Minor"],["30440. Bebop","20. Major","2333. Minor"]] one array for each section (date)
    var scoresSections:[[String]] = []
    
    
    //We add a section of the menu table by the method addSection, which adds the section and the item array to the existing property
    func addSection(section: String, scoresSection:[String])
    {
        sections = sections + [section]
        //scoresSection is an array of scores for 1 section
        scoresSections = scoresSections + [scoresSection]
    }
}
