//
//  LearnViewController.swift
//  ear training
//
//  Created by Marcus Choi on 9/8/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import AVFoundation
import Parse

class LearnViewController: UIViewController, UITextFieldDelegate {

    var error: NSError? = nil
    @IBOutlet var solfegeImage: UIImageView!
    
    var userAnswer = ""
    var userAnswerArray = [String]()
    var userGuesses = 0
    //@IBOutlet var userGuessesText: UITextView!
    
    //number of notes that user must guess for each problem
    var numberOfNotesPlayed = 1
    //total correct user guessed notes, updates after each correct answer
    var totalNumberOfNotesPlayed = 0
    
    //time that passes between notes played in seconds
    var timeBetweenNotes: NSTimeInterval = 1/3
    
    //plays chord then melody with delay of timeBetweenNotes
    var delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
        Int64(1/3 * Double(NSEC_PER_SEC)))
    
    @IBOutlet var scoreLabel: UILabel!
    var points = 0
    var problems = 0
    var finalScore = 0.00
    
    //timer stuff
    var timer = NSTimer()
    //hundredths of a second
    var hseconds = 0.00
    var minutes = 0
    @IBOutlet var timeLabel: UILabel!
    
    
    //set the play/pause button toggle
    @IBOutlet var navbar: UINavigationItem!
    
    //initially game is activated so playing and paused are both false
    var isPlaying = false
    var isPaused = false
    
    //tells how many times play/pause, marks game entry
    var counter = 0
    
    @IBOutlet var playPauseOutlet: UIBarButtonItem!
    @IBAction func playPauseToggle(sender: AnyObject)
    {
        counter++
        
        var toggleButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Play, target: self, action: "playPauseToggle:")
        
        //pausing the game
        if isPlaying
        {
            timer.invalidate()
            isPlaying = false
            isPaused = true
            
            generateNewMelodyOutlet.enabled = false
            
        }
        else    //un-pausing creates new melody. If first play, reset points and probs
        {
            //game entry. counter is reset at end of game
            if counter == 1
            {
                resetGameProperties()
            }
            
            gameTime(self)
            
            toggleButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Pause, target: self, action: "playPauseToggle:")
            isPlaying = true
            isPaused = false
        }
        navbar.rightBarButtonItem = toggleButton
        
        println(counter)
    }
    
    //GAME TIME!!!
    var gameLength = 1
    @IBAction func gameTime(sender: AnyObject)
    {
        
        
        playMelodyOutlet.enabled = true
        playChordOutlet.enabled = true
        
        generateNewMelody(self)
        generateNewMelodyOutlet.enabled = true
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("result"), userInfo: nil, repeats: true)
        
    }
    func result()
    {
        hseconds++
        
        //hseconds/100 is 1 second
        if hseconds/100 == 60.00
        {
            minutes++
            hseconds = 0.00
        }
            //proper display of hundredths of a second
        else if hseconds/100 < 10.00
        {
            timeLabel.text = "\(minutes):0\(hseconds/100)"
        }
        else
        {
            timeLabel.text = "\(minutes):\(hseconds/100)"
        }
        
        if minutes == gameLength //hseconds/100 == 5
        {
            
            timer.invalidate()
            
            timeLabel.text = "\(minutes):00.00"
            
            message.text = "Game Over"
            
            //if user did not finish a problem
            if (problems - 1) == 0
            {
                finalScore = 0.0
            }
            else
            {
                
                //total number of notes accumulated for each problem * accuracy * 100
                finalScore = Double(totalNumberOfNotesPlayed)*Double(abs(points))/Double(problems - 1) //problems-1 because last problem in game unfinished
                
                finalScore = round(100*finalScore)
            }
            
            //show the final score and ask to play again
            var alert = UIAlertController(title: "Final Score: \(finalScore)", message: "Play Again?", preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "Yes!", style: .Default, handler: { (action) -> Void in
                
                self.resetGameProperties()
                self.gameTime(self)
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action) -> Void in
                
                self.resetGameProperties()
                
                //the last unanswered problem from the game
                self.problems = 1
                
                self.playPauseToggle(self)
                self.isPaused = false    //because play pause toggle is called, make sure this is false
                self.counter = 0
                
                self.generateNewMelodyOutlet.enabled = true
                
                for solfegeOutlet in self.solfegeCollection
                {
                    solfegeOutlet.enabled = true
                }
                
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
            
            //only save score to database if it is above 0
            if finalScore > 0.0
            {
                var gameScore = PFObject(className: "GameScore")
                gameScore["score"] = finalScore
                gameScore["scale"] = scaleChosen.text
                gameScore["username"] = PFUser.currentUser()!.username
                gameScore["gameLength"] = gameLength
                
                //save the score
                gameScore.saveInBackgroundWithBlock
                    {(success: Bool, error: NSError?) -> Void in
                        if (success)
                        {println("\(gameScore.objectId)")}
                        else
                        {println("failed")}
                }
                
            }
            
            points = 0
            problems = 0
            minutes = 0
        }
    }
    func resetGameProperties()
    {
        //reset minutes and seconds
        minutes = 0
        hseconds = 0.0
        
        scoreLabel.text = "0/0"
        points = 0
        problems = 0
        totalNumberOfNotesPlayed = 0
        
        userGuesses = 0
        userAnswer = ""
        userAnswerArray.removeAll(keepCapacity: false)
    }

//    @IBOutlet var resetGuessesOutlet: UIButton!
    @IBAction func resetGuesses(sender: AnyObject)
    {
        if userGuesses != 0
        {
            problems++
            
            userGuesses = 0
            userAnswer = ""
            userAnswerArray.removeAll(keepCapacity: false)
            
            //userGuessesText.text = "Guesses: \(userGuesses)"
            scoreLabel.text = "\(points)/\(problems)"
        }
        
    }

    
    var previousUserNote = String?()
    
    //very first guess of entire instance of using the app
    var firstGuessHasHappened = false
    
    var playerSolfege: AVAudioPlayer = AVAudioPlayer()
    @IBAction func solfegeButtons(sender: AnyObject)
    {
        
        //identify image based on restoration identifier property of button pressed
        solfegeImage.image = UIImage(named: sender.restorationIdentifier!! + ".jpg")
        
        
        //file location of first (possibly wrong) guess
        var fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(sender.restorationIdentifier!!)1"]!, ofType: "wav")
        
        if answerIsShown == false
        {
            message.text = ""
            userAnswer += sender.restorationIdentifier!! + " "
            
            //has syllables without ending number
            userAnswerArray.append(sender.restorationIdentifier!!)
            
            //if value from useranswerarray == value from answer (minus the last character), play the exact syllable as answer
            if userAnswerArray[userGuesses] == solfegeArray[solfegeIndices[userGuesses]].substringToIndex(solfegeArray[solfegeIndices[userGuesses]].endIndex.predecessor())
            {
                fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary[solfegeArray[solfegeIndices[userGuesses]]], ofType: "wav")
            }
            else //if user answer is wrong, play nearest note for sender's syllable
            {
                if firstGuessHasHappened
                {
                    //get index of previously played note, subtract index of syllable with end numbers, play the one that is smaller difference
                    if abs(find(pianoWavArray, previousUserNote!)! - find(pianoWavArray, solfegeWavDictionary["\(userAnswerArray[userGuesses])1"]!)!) > abs(find(pianoWavArray, previousUserNote!)! - find(pianoWavArray, solfegeWavDictionary["\(userAnswerArray[userGuesses])2"]!)!)
                    {
                        fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])2"], ofType: "wav")
                        println("higher octave")
                    }
                    else
                    {
                        fileLocationSolfege = NSBundle.mainBundle().pathForResource(solfegeWavDictionary["\(userAnswerArray[userGuesses])1"], ofType: "wav")
                    }
                }
            }
            
            //after this is true, can start ensuring notes of guesses are close together
            firstGuessHasHappened = true
            
            //remove extension from filename (last 4 characters) gives just the note
            previousUserNote = fileLocationSolfege?.lastPathComponent.stringByDeletingPathExtension
            
            userGuesses += 1
        }
        
        playerSolfege = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationSolfege!), error: &error)
        playerSolfege.play()
        
        //userGuessesText.text = "Guesses: \(userGuesses)"
        if userGuesses == numberOfNotesPlayed
        {
            userGuesses = 0
            
            delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
                Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
            
            if userAnswer == answer
            {
                message.text = "Correct!"
                
                //gives points if game is not paused and in non-game mode
                if isPaused == false
                {
                    points += 1
                    totalNumberOfNotesPlayed += numberOfNotesPlayed
                }
                
                //generates new melody with delay
                dispatch_after(delayTime1, dispatch_get_main_queue())
                    {
                        self.generateNewMelody(self)
                        //self.userGuessesText.text = "Guesses: \(self.userGuesses)"
                }
                
            }
            else
            {
                message.text = "Try Again"
                dispatch_after(delayTime1, dispatch_get_main_queue())
                    {
                        self.message.text = ""
                        //self.userGuessesText.text = "Guesses: \(self.userGuesses)"
                }

                problems++
            }
            scoreLabel.text = "\(points)/\(problems)"
            
            userAnswer = ""
            userAnswerArray.removeAll(keepCapacity: false)
        }
    }
    
    var answerIsShown = false
    @IBOutlet var showAnswerOutlet: UIButton!
    @IBAction func showAnswer(sender: AnyObject)
    {
        //make syllables the same octave if answer is played?
        
        
        //NUMBER SYSTEM
        if numberSystem == true
        {
            //turn answer into numbers
            var solfegeNumberDictionary = ["do":"1","ra":"b2","re":"2","me":"b3","mi":"3","fa":"4","fi":"#4","sol":"5","le":"b6","la":"6","te":"b7","ti":"7"]
            
            var answerNumSysArray = split(answer) {$0 == " "}
            
            var answerNumSys = ""
            
            for answerNum in answerNumSysArray
            {
                answerNumSys += solfegeNumberDictionary[answerNum]! + " "
            }
            
            message.text = answerNumSys
        }
        else
        {
            message.text = answer
        }
        
        answerIsShown = true
        
    }
    
    var player = [AVAudioPlayer]()
    
    var chord1: AVAudioPlayer = AVAudioPlayer()
    var chordm3: AVAudioPlayer = AVAudioPlayer()
    var chord3: AVAudioPlayer = AVAudioPlayer()
    var chord5: AVAudioPlayer = AVAudioPlayer()
    
    var pianoWavArray = ["C3", "Db3", "D3", "Eb3", "E3", "F3", "Gb3", "G3", "Ab3", "A3", "Bb3", "B3", "C4", "Db4", "D4", "Eb4", "E4", "F4", "Gb4", "G4", "Ab4", "A4", "Bb4", "B4"]
    
    //MAKE MELODIES GO BEYOND ONE OCTAVE
    var solfegeArray = ["do1","ra1","re1","me1","mi1","fa1","fi1","sol1","le1","la1","te1","ti1","do2","ra2","re2","me2","mi2","fa2","fi2","sol2","le2","la2","te2","ti2"]
    
    var rootIndex = 0//Int(arc4random_uniform(UInt32(12)))
    
    //dictionary do=Ab.wav, ra=A.wav etc
    var solfegeWavDictionary = [String: String]()
    
    var solfegeIndices = [Int]()
    
    //each notesPlayed index contains a wav filename, ie "Bb", for the melody
    var notesPlayed = [String]()
    
    var answer: String = ""
    
    
    @IBOutlet var generateNewMelodyOutlet: UIButton!
    @IBAction func generateNewMelody(sender: AnyObject)
    {
/*
        var random1to100:UInt32
        var percentageOfSmallerIntervals:UInt32 = 90
        
        //difference between indices
        var smallestLargeInterval = 6
        var largestInterval = 16
        
        var indexOfPreviousNote: Int
        //index of 2 notes before
        var indexOfPreviousNote2: Int
        var indexOfCurrentNote: Int
        var currentSyllable = String()
        var random0or1: UInt32
        
        //in half steps
        var maxIntervalBebop = 7
        var randomIntervalLessThanMaxBebop:Int
*/
        //the scale chosen variable
        var scale = scaleChosen.text.lowercaseString
        
        resetGuesses(self)
        showAnswerOutlet.enabled = true
        
        for solfegeOutlet in solfegeCollection
        {
            solfegeOutlet.enabled = true
        }
        
        playMelodyOutlet.enabled = true
        playChordOutlet.enabled = true
        
//        resetGuessesOutlet.enabled = true
        
        answerIsShown = false
        
        
        //don't increase number of problems if game is paused and new melody generated
        if isPaused == false
        {
            problems += 1
        }
        answer = ""
        
        solfegeIndices = [Int](count: numberOfNotesPlayed, repeatedValue: 0)
        notesPlayed = [String](count: numberOfNotesPlayed, repeatedValue: "")
        
        //matching solfege syllables to piano notes in dictionary based on root index
        var i:Int
        for i = rootIndex; i > 0; i--
        {
            //ie, do = "Bb"
            solfegeWavDictionary[solfegeArray[12-i]] = pianoWavArray[rootIndex-i];
        }
        
        //12 is index of beginning of 2nd octave
        for i = rootIndex; i < 12; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
        for i = 12; i < 12 + rootIndex; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex + 12]] = pianoWavArray[i];
        }
        
        for i = 12 + rootIndex; i < solfegeArray.count; i++
        {
            
            solfegeWavDictionary[solfegeArray[i - rootIndex]] = pianoWavArray[i];
        }
        
        var limitToSyllables = false
        var sylChosenArray = [String]()
        
        //Check if user input syllables. Check for space and check if text does not match anything in scaleData array
        if scale.rangeOfString(" ") != nil
        {
            sylChosenArray = split(scaleChosen.text.lowercaseString) {$0 == " "}
            
            //keeps array count from updating after every loop
            let sylChosenArrayCount = sylChosenArray.count
            
            //make the array of syllables in all octaves
            for var i = 0; i < sylChosenArrayCount; i++
            {
                //change syllable to match solfegeArray syllables if user inputs numbers or other syllable names
                if sylChosenArray[i] == "1"
                {
                    sylChosenArray[i] = "do"
                }
                else if sylChosenArray[i] == "#1" || sylChosenArray[i] == "b2" || sylChosenArray[i] == "di"
                {
                    sylChosenArray[i] = "ra"
                }
                else if sylChosenArray[i] == "2"
                {
                    sylChosenArray[i] = "re"
                }
                else if sylChosenArray[i] == "#2" || sylChosenArray[i] == "b3" || sylChosenArray[i] == "ri"
                {
                    sylChosenArray[i] = "me"
                }
                else if sylChosenArray[i] == "3"
                {
                    sylChosenArray[i] = "mi"
                }
                else if sylChosenArray[i] == "4"
                {
                    sylChosenArray[i] = "fa"
                }
                else if sylChosenArray[i] == "#4" || sylChosenArray[i] == "b5" || sylChosenArray[i] == "se"
                {
                    sylChosenArray[i] = "fi"
                }
                else if sylChosenArray[i] == "5" || sylChosenArray[i] == "so"
                {
                    sylChosenArray[i] = "sol"
                }
                else if sylChosenArray[i] == "#5" || sylChosenArray[i] == "b6" || sylChosenArray[i] == "si"
                {
                    sylChosenArray[i] = "le"
                }
                else if sylChosenArray[i] == "6"
                {
                    sylChosenArray[i] = "la"
                }
                else if sylChosenArray[i] == "#6" || sylChosenArray[i] == "b7" || sylChosenArray[i] == "li"
                {
                    sylChosenArray[i] = "te"
                }
                else if sylChosenArray[i] == "7"
                {
                    sylChosenArray[i] = "ti"
                }
                
                sylChosenArray.append(sylChosenArray[i] + "2")
                sylChosenArray[i] += "1"
            }
            
            for sylChosen in sylChosenArray
            {
                //if solfegeArray contains chosen syllable(s) (if what user typed are syllables), melody can be limited to syllables
                if contains(solfegeArray, sylChosen)
                {
                    limitToSyllables = true
                    
                }
            }
        }
        
        //creating notesPlayed array
        for var j = 0; j < numberOfNotesPlayed; j++
        {
            
            //solfegeIndices is an array of solfegeArray indices from 0-11
            solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
            
            //if user inputs syllables, limit melody to those
            if limitToSyllables == true
            {
                
                while !contains(sylChosenArray, solfegeArray[solfegeIndices[j]])
                {
                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                }
            }
                
                //Major scale
            else if scale == "major"
            {
 
                while solfegeArray[solfegeIndices[j]].rangeOfString("ra") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("me") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("fi") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("le") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("te") != nil
                {
                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                }
                    
                
            }
                //minor scale
            else if scale == "minor(relative)"
            {
                    
                while solfegeArray[solfegeIndices[j]].rangeOfString("ra") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("mi") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("fi") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("la") != nil || solfegeArray[solfegeIndices[j]].rangeOfString("ti") != nil
                {
                    solfegeIndices[j] = Int(arc4random_uniform(UInt32(solfegeArray.count)))
                }
        
            }
            
            //contains the notes ie ["Db3", "A4", "C3"] of the melody
            notesPlayed[j] = solfegeWavDictionary[solfegeArray[solfegeIndices[j]]]!
            
            
            answer += solfegeArray[solfegeIndices[j]].substringToIndex(solfegeArray[solfegeIndices[j]].endIndex.predecessor()) + " "
            
        }
        
        println(answer)
        
        //plays chord then melody with delay of timeBetweenNotes
        delayTime1 = dispatch_time(DISPATCH_TIME_NOW,
            Int64(timeBetweenNotes * Double(NSEC_PER_SEC)))
        
        
        scoreLabel.text = "\(points)/\(problems)"
        
    }//CLOSE GENERATE NEW MELODY
    
    
    //@IBOutlet var scaleChosen: UITextView!
    @IBOutlet var scaleChosen: UITextField!
    
    
    @IBOutlet var playChordOutlet: UIButton!
    @IBAction func playChord(sender: AnyObject)
    {
        var rootIndexm3rd = rootIndex + 3
        var rootIndex3rd = rootIndex + 4
        var rootIndex5th = rootIndex + 7
        
        var fileLocationRoot = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndex], ofType: "wav")
        var fileLocationRootm3rd = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndexm3rd], ofType: "wav")
        var fileLocationRoot3rd = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndex3rd], ofType: "wav")
        var fileLocationRoot5th = NSBundle.mainBundle().pathForResource(pianoWavArray[rootIndex5th], ofType: "wav")
        //var error: NSError? = nil
        
        chord1 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRoot!), error: &error)
        chordm3 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRootm3rd!), error: &error)
        chord3 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRoot3rd!), error: &error)
        chord5 = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocationRoot5th!), error: &error)
        
        chord1.play()
        chord5.play()
        
        //if scale chosen has "minor" in string, play minor chord
        if scaleChosen.text.rangeOfString("minor") != nil
        {
            
            chordm3.play()
      
        }
        else
        {
            chord3.play()

        }
        message.text = "Tonic: \(pianoWavArray[rootIndex].substringToIndex(pianoWavArray[rootIndex].endIndex.predecessor()))"
    }
    
    @IBOutlet var playMelodyOutlet: UIButton!
    @IBAction func playMelody(sender: AnyObject)
    {
        player.removeAll(keepCapacity: true)
        
        var error: NSError? = nil
        
        var fileLocation = [NSObject]()
        
        var shortStartDelay: NSTimeInterval = 0
        
        for var i = 0; i < numberOfNotesPlayed; i++
        {
            
            //file name of notesPlayed array sound file is appended to fileLocation array
            fileLocation.append(NSBundle.mainBundle().pathForResource(notesPlayed[i], ofType: "wav" as String)!)
            
            //file location of random sound file is appended to audio player array
            player.append(AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: fileLocation[i] as! String), error: &error))
            
            //current time associated with output device "player[0], the first random sound file played"
            let now: NSTimeInterval = player[0].deviceCurrentTime;
            
            player[i].playAtTime(now + shortStartDelay)
            
            shortStartDelay += timeBetweenNotes;
        }
    }
    @IBOutlet var message: UITextView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(patternImage: UIImage(named: "music_bg1.jpg")!)
        
        for solfegeOutlet in solfegeCollection
        {
            solfegeOutlet.enabled = false
        }
        
        playChordOutlet.enabled = false
        playMelodyOutlet.enabled = false
        
        //resetGuessesOutlet.enabled = false
        
        showAnswerOutlet.enabled = false
        
        message.text = "Welcome \(PFUser.currentUser()!.username)!"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //@IBOutlet var BPM: UITextView!
    
    @IBOutlet var doOutlet: UIButton!
    @IBOutlet var reOutlet: UIButton!
    @IBOutlet var miOutlet: UIButton!
    @IBOutlet var faOutlet: UIButton!
    @IBOutlet var solOutlet: UIButton!
    @IBOutlet var laOutlet: UIButton!
    @IBOutlet var tiOutlet: UIButton!
    @IBOutlet var raOutlet: UIButton!
    @IBOutlet var meOutlet: UIButton!
    @IBOutlet var fiOutlet: UIButton!
    @IBOutlet var leOutlet: UIButton!
    @IBOutlet var teOutlet: UIButton!

    //solfege button collection in array
    @IBOutlet var solfegeCollection: [UIButton]!
    
    //these must be defined outside the func so that the var exists between func calls and is updated each time func is called
    var verticalConstraints = [NSLayoutConstraint]()
    var horizontalConstraints = [NSLayoutConstraint]()
    
    //re-draw the constraints when view changes orientation
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        //calls new key config when transition between orientations is completed. Animation is nil
        coordinator.animateAlongsideTransition(nil, completion: {context in
            
            self.newKeyConfiguration()
            
        })
    }
    
    var fixKeyConfigToC = true //false
    
    func newKeyConfiguration()
    {
        //initial distance (do) is at left side of screen
        var horizontalDist:CGFloat = 0
        
        var black = UIColor.blackColor()
        var white = UIColor.whiteColor()
        var verticalDist:CGFloat = 0
        var bgColor:UIColor
        var i:Int
        
        var indexOfDb:Int = 1
        var indexOfEb:Int = 3
        var indexOfGb:Int = 6
        var indexOfAb:Int = 8
        var indexOfBb:Int = 10
        var indexOfC:Int = -1
        var indexOfF:Int = 5
        
        var halfKeyDisplacement = self.view.frame.size.width/14
        
        self.view.addConstraint(NSLayoutConstraint(item: doOutlet, attribute: .Width, relatedBy: .Equal, toItem: self.view, attribute: .Width, multiplier: 2/16, constant: 0))
        
        for i = 0; i < solfegeCollection.count; i++
        {
            
            //black keys vertical displacement
            if i == indexOfDb || i == indexOfEb || i == indexOfGb || i == indexOfAb || i == indexOfBb
            {
                bgColor = black
                verticalDist = -42
            }
                //white keys
            else
            {
                bgColor = white
                verticalDist = 0
            }
            
            //index of 2nd white key in a row (F and/or C) is twice distance away from previous key than if black key were between them
            if i == indexOfC || i == indexOfF
            {
                horizontalDist += halfKeyDisplacement
            }
            
            //put constraints into array
            verticalConstraints.append(NSLayoutConstraint(item: solfegeCollection[i], attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: verticalDist))
            
            //makes sure the constant of each constraint is updated everytime function called
            verticalConstraints[i].constant = verticalDist
            
            //applying vertical position of keys
            self.view.addConstraint(verticalConstraints[i])
            
            solfegeCollection[i].backgroundColor = bgColor
            
            //horizontal positions of keys
            horizontalConstraints.append(NSLayoutConstraint(item: solfegeCollection[i], attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: horizontalDist))
            
            horizontalConstraints[i].constant = horizontalDist
            self.view.addConstraint(horizontalConstraints[i])
            
            horizontalDist += halfKeyDisplacement
            
        }
        
    }
    
    //number system stuff
    var numberSystem = false
    override func viewDidAppear(animated: Bool)
    {
        newKeyConfiguration()
        if numberSystem == true
        {
            doOutlet.setTitle("1", forState: .Normal)
            reOutlet.setTitle("2", forState: .Normal)
            miOutlet.setTitle("3", forState: .Normal)
            faOutlet.setTitle("4", forState: .Normal)
            solOutlet.setTitle("5", forState: .Normal)
            laOutlet.setTitle("6", forState: .Normal)
            tiOutlet.setTitle("7", forState: .Normal)
            raOutlet.setTitle("#1/b2", forState: .Normal)
            meOutlet.setTitle("#2/b3", forState: .Normal)
            fiOutlet.setTitle("#4/b5", forState: .Normal)
            leOutlet.setTitle("#5/b6", forState: .Normal)
            teOutlet.setTitle("#6/b7", forState: .Normal)
        }
        else if numberSystem == false
        {
            doOutlet.setTitle("do", forState: .Normal)
            reOutlet.setTitle("re", forState: .Normal)
            miOutlet.setTitle("mi", forState: .Normal)
            faOutlet.setTitle("fa", forState: .Normal)
            solOutlet.setTitle("sol", forState: .Normal)
            laOutlet.setTitle("la", forState: .Normal)
            tiOutlet.setTitle("ti", forState: .Normal)
            raOutlet.setTitle("di/ra", forState: .Normal)
            meOutlet.setTitle("ri/me", forState: .Normal)
            fiOutlet.setTitle("fi/se", forState: .Normal)
            leOutlet.setTitle("si/le", forState: .Normal)
            teOutlet.setTitle("li/te", forState: .Normal)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        //end editing when screen tapped
        self.view.endEditing(true)
    }
}