//
//  LoginViewController.swift
//  ear training
//
//  Created by Marcus Choi on 6/7/15.
//  Copyright (c) 2015 choi. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController, UITextFieldDelegate
{
    var signUpActive = Bool() //true
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showAlert(title:String, error:String)
    {
        var alert = UIAlertController(title: title, message: error, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { action in
            
            //self.dismissViewControllerAnimated(true, completion: nil)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    @IBOutlet var alreadyRegisteredLabel: UILabel!
    
    @IBOutlet var message: UILabel!
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    
    @IBOutlet var signUpButton: UIButton!
    
    @IBOutlet var logInOutlet: UIButton!
    
    @IBAction func logInButton(sender: AnyObject)
    {
        
        if signUpActive == true
        {
            message.text = "Use the Form Below to Log In"
            alreadyRegisteredLabel.text = "Not registered yet?"
            
            signUpActive = false
            
            signUpButton.setTitle("Log In", forState: UIControlState.Normal)
            
            logInOutlet.setTitle("Sign Up", forState: UIControlState.Normal)
        }
        else
        {
            message.text = "Use the Form Below to Sign Up"
            alreadyRegisteredLabel.text = "Already registered?"
            
            signUpActive = true
            
            signUpButton.setTitle("Sign Up", forState: UIControlState.Normal)
            
            logInOutlet.setTitle("Log In", forState: UIControlState.Normal)
        }
        
    }
    
    //for checking scores
    var usernames:[NSString] = [NSString]()
    var currentUser = String()
    
    @IBAction func signUp(sender: AnyObject)
    {
        var error = ""
        
        if username.text == "" || password.text == ""
        {
            error = "please enter a username and password"
        }
        
        if error != ""
        {
            showAlert("Error", error: error)
        }
        else
        {
            
            //activity indicator when user has signed up
            activityIndicator = UIActivityIndicatorView(frame: CGRectMake(0, 0, 50, 50))
            activityIndicator.center = self.view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
            view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            UIApplication.sharedApplication().beginIgnoringInteractionEvents()
            
            var errorMessage = "Please try again later"
            
            //if user is signing up and creating a username
            if signUpActive == true
            {
                var user = PFUser()
                user.username = username.text!.lowercaseString
                user.password = password.text
                
                user.signUpInBackgroundWithBlock
                {
                    (succeeded: Bool, signUpError: NSError?) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    
                    if signUpError == nil
                    {
                        var gameLengthArray = [1,3,5]
                        
                        //creates new high score objects for each game length for a newly registered player
                        for var i = 0; i < gameLengthArray.count; i++
                        {
                            var highScore = PFObject(className: "HighScore")
                            highScore["gameLength"] = gameLengthArray[i]
                            highScore["score"] = 0
                            highScore["username"] = user.username
                            highScore["scale"] = "N/A"
                            highScore["masteryLevel"] = "New Beginner"
                            
                            //save the high score object
                            highScore.saveInBackgroundWithBlock
                            {(success: Bool, error: NSError?) -> Void in
                                if (success)
                                {print("\(highScore.objectId)")}
                                else
                                {print("failed")}
                            }
                        }
                        

                        //add own name to usernames for checking scores
                        //check if "usernames" key has been created yet in nsuserdefaults
                        if NSUserDefaults.standardUserDefaults().valueForKey("usernames") == nil
                        {
                            self.usernames.append(PFUser.currentUser()!.username!)
                            
                            NSUserDefaults.standardUserDefaults().setObject(self.usernames, forKey: "usernames")
                            NSUserDefaults.standardUserDefaults().synchronize()
                        }
                        
                        self.performSegueWithIdentifier("login", sender: self)
                        
                    }
                    else
                    {
                        if let errorString = signUpError!.userInfo["error"] as? NSString
                        {
                            error = errorString as String
                        }
                        else
                        {
                            error = "something is wrong, try again later"
                        }
                        
                        self.showAlert("could not sign up", error: error)
                        
                    }
                }
            }
            else //log the user in with their inputed username and password
            {
                
                
                PFUser.logInWithUsernameInBackground(username.text!.lowercaseString, password: password.text!, block: { (user, error) -> Void in
                    
                    self.activityIndicator.stopAnimating()
                    UIApplication.sharedApplication().endIgnoringInteractionEvents()
                    
                    if error == nil
                    {
                        // Do stuff after successful login.
                        print("logged in")
                        print(NSUserDefaults.standardUserDefaults().valueForKey("usernames"))
                        
                        
                        //for logging into new device with previously created username
                        if NSUserDefaults.standardUserDefaults().valueForKey("usernames") == nil
                        {
                            self.usernames.append(PFUser.currentUser()!.username!)
                            
                            NSUserDefaults.standardUserDefaults().setObject(self.usernames, forKey: "usernames")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            
                        }
                        
                        self.performSegueWithIdentifier("login", sender: self)
                       
                        
                    }
                    else
                    {
                        // The login failed. Check error to see why.
                        if let errorString = error!.userInfo["error"] as? NSString    //?s indicate that it might not exist
                        {
                            errorMessage = errorString as String
                        }
                        else
                        {
                            errorMessage = "Login failed, try again later"
                        }
                        
                        self.showAlert("Could Not Log In", error: errorMessage)
                    }
                })
            }
            
        }
        
    }   //close func signUp
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        signUpActive = true
        
    }
    
    override func viewDidAppear(animated: Bool)
    {
        print(PFUser.currentUser())
        if PFUser.currentUser() != nil
        {
        
            self.performSegueWithIdentifier("login", sender: self)
            
        }
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}